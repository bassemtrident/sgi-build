(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["common"], {
    /***/
    "0C2C":
    /*!******************************************!*\
      !*** ./src/app/services/loan.service.ts ***!
      \******************************************/

    /*! exports provided: LoanService */

    /***/
    function C2C(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoanService", function () {
        return LoanService;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ../../environments/environment */
      "AytR");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");

      var LoanService = /*#__PURE__*/function () {
        function LoanService(http) {
          _classCallCheck(this, LoanService);

          this.http = http;
          this.url = _environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].SERVER_API_URL;
        }

        _createClass(LoanService, [{
          key: "createLoan",
          value: function createLoan(data) {
            return this.http.post("".concat(this.url, "/loan/create/"), data, {
              observe: 'response'
            });
          }
        }, {
          key: "allLoan",
          value: function allLoan() {
            return this.http.get("".concat(this.url, "/loan/all/"), {
              observe: 'response'
            });
          }
        }, {
          key: "getLoan",
          value: function getLoan(id) {
            return this.http.get("".concat(this.url, "/loan/").concat(id), {
              observe: 'response'
            });
          }
        }, {
          key: "countLoan",
          value: function countLoan() {
            return this.http.get("".concat(this.url, "/loan/count"), {
              observe: 'response'
            });
          }
        }, {
          key: "getLoanByCompany",
          value: function getLoanByCompany(username) {
            return this.http.get("".concat(this.url, "/loan/company/").concat(username), {
              observe: 'response'
            });
          }
        }]);

        return LoanService;
      }();

      LoanService.ɵfac = function LoanService_Factory(t) {
        return new (t || LoanService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]));
      };

      LoanService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
        token: LoanService,
        factory: LoanService.ɵfac,
        providedIn: 'root'
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](LoanService, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
          args: [{
            providedIn: 'root'
          }]
        }], function () {
          return [{
            type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "ZtWP":
    /*!*********************************************!*\
      !*** ./src/app/services/company.service.ts ***!
      \*********************************************/

    /*! exports provided: CompanyService */

    /***/
    function ZtWP(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CompanyService", function () {
        return CompanyService;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ../../environments/environment */
      "AytR");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");

      var CompanyService = /*#__PURE__*/function () {
        function CompanyService(http) {
          _classCallCheck(this, CompanyService);

          this.http = http;
          this.url = _environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].SERVER_API_URL;
        }

        _createClass(CompanyService, [{
          key: "allCompany",
          value: function allCompany() {
            return this.http.get("".concat(this.url, "/company/all"), {
              observe: 'response'
            });
          }
        }, {
          key: "getCompany",
          value: function getCompany(id) {
            return this.http.get("".concat(this.url, "/company/").concat(id), {
              observe: 'response'
            });
          }
        }, {
          key: "countCompany",
          value: function countCompany() {
            return this.http.get("".concat(this.url, "/company/count"), {
              observe: 'response'
            });
          }
        }]);

        return CompanyService;
      }();

      CompanyService.ɵfac = function CompanyService_Factory(t) {
        return new (t || CompanyService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]));
      };

      CompanyService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
        token: CompanyService,
        factory: CompanyService.ɵfac,
        providedIn: 'root'
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](CompanyService, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
          args: [{
            providedIn: 'root'
          }]
        }], function () {
          return [{
            type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "xf/2":
    /*!**********************************************!*\
      !*** ./src/app/services/investor.service.ts ***!
      \**********************************************/

    /*! exports provided: InvestorService */

    /***/
    function xf2(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "InvestorService", function () {
        return InvestorService;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ../../environments/environment */
      "AytR");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common/http */
      "tk/3");

      var InvestorService = /*#__PURE__*/function () {
        function InvestorService(http) {
          _classCallCheck(this, InvestorService);

          this.http = http;
          this.url = _environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].SERVER_API_URL;
        }

        _createClass(InvestorService, [{
          key: "getAccounts",
          value: function getAccounts(username) {
            return this.http.get("".concat(this.url, "/accounts/investor/").concat(username), {
              observe: 'response'
            });
          }
        }, {
          key: "countInvestor",
          value: function countInvestor() {
            return this.http.get("".concat(this.url, "/investor/count"), {
              observe: 'response'
            });
          }
        }]);

        return InvestorService;
      }();

      InvestorService.ɵfac = function InvestorService_Factory(t) {
        return new (t || InvestorService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]));
      };

      InvestorService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
        token: InvestorService,
        factory: InvestorService.ɵfac,
        providedIn: 'root'
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](InvestorService, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
          args: [{
            providedIn: 'root'
          }]
        }], function () {
          return [{
            type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
          }];
        }, null);
      })();
      /***/

    }
  }]);
})();
//# sourceMappingURL=common-es5.js.map