export const loan = [
  {
    "id":"1",
    "company_name": "company1",
    "comapny_email": "comapny@company.com",
    "target": 50000,
    "raised": 10000,
    "company_sector": "IT",
    "credit_scoring": "D",
    "stage": "start up",
    "description": "This loan for investment in IT ",
  },

  {
    "id":"2",
    "company_name": "company2",
    "comapny_email": "comapny2@company.com",
    "target": 100000,
    "raised": 20000,
    "company_sector": "Gaming",
    "credit_scoring": "A",
    "stage": "Idea",
    "description": "This loan for investment in start up ",
  },

  {
    "id":"3",
    "company_name": "company3",
    "comapny_email": "comapny3@company.com",
    "target": 150000,
    "raised": 30000,
    "company_sector": "E-commerce",
    "credit_scoring": "E",
    "stage": "Prototype",
    "description": "This loan for investment in green energy",
  },
  {
    "id":"4",
    "company_name": "company4",
    "comapny_email": "comapny4@company.com",
    "target": 150000,
    "raised": 30000,
    "company_sector": "Legal",
    "credit_scoring": "B",
    "stage": "start up",
    "description": "This loan for investment in solar energy",
  } ,
  {
    "id":"5",
    "company_name": "company5",
    "comapny_email": "comapny5@company.com",
    "target": 150000,
    "raised": 30000,
    "company_sector": "Finance",
    "credit_scoring": "D",
    "stage": "start up",
    "description": "This loan for investment in solar energy",
  }
]
