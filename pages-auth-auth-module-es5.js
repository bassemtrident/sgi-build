(function () {
  function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

  function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

  function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

  function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

  function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

  function _possibleConstructorReturn(self, call) { if (call && (typeof call === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

  function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

  function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

  function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

  function _createForOfIteratorHelper(o, allowArrayLike) { var it; if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

  function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

  function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-auth-auth-module"], {
    /***/
    "9rsm":
    /*!**********************************************************************!*\
      !*** ./src/app/pages/auth/auth-signin/auth-signin-routing.module.ts ***!
      \**********************************************************************/

    /*! exports provided: AuthSigninRoutingModule */

    /***/
    function rsm(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AuthSigninRoutingModule", function () {
        return AuthSigninRoutingModule;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _auth_signin_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./auth-signin.component */
      "Zeh+");

      var routes = [{
        path: '',
        component: _auth_signin_component__WEBPACK_IMPORTED_MODULE_2__["AuthSigninComponent"]
      }];

      var AuthSigninRoutingModule = function AuthSigninRoutingModule() {
        _classCallCheck(this, AuthSigninRoutingModule);
      };

      AuthSigninRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
        type: AuthSigninRoutingModule
      });
      AuthSigninRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
        factory: function AuthSigninRoutingModule_Factory(t) {
          return new (t || AuthSigninRoutingModule)();
        },
        imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](AuthSigninRoutingModule, {
          imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
          exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        });
      })();
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AuthSigninRoutingModule, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
          args: [{
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
          }]
        }], null, null);
      })();
      /***/

    },

    /***/
    "9vPV":
    /*!****************************************************************************!*\
      !*** ./src/app/pages/auth/company-signup/company-signup-routing.module.ts ***!
      \****************************************************************************/

    /*! exports provided: CompanySignupRoutingModule */

    /***/
    function vPV(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CompanySignupRoutingModule", function () {
        return CompanySignupRoutingModule;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _company_signup_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./company-signup.component */
      "S3YR");

      var routes = [{
        path: '',
        component: _company_signup_component__WEBPACK_IMPORTED_MODULE_2__["CompanySignupComponent"]
      }];

      var CompanySignupRoutingModule = function CompanySignupRoutingModule() {
        _classCallCheck(this, CompanySignupRoutingModule);
      };

      CompanySignupRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
        type: CompanySignupRoutingModule
      });
      CompanySignupRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
        factory: function CompanySignupRoutingModule_Factory(t) {
          return new (t || CompanySignupRoutingModule)();
        },
        imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](CompanySignupRoutingModule, {
          imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
          exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        });
      })();
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](CompanySignupRoutingModule, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
          args: [{
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
          }]
        }], null, null);
      })();
      /***/

    },

    /***/
    "B0WW":
    /*!******************************************************************************!*\
      !*** ./src/app/pages/auth/auth-reset-password/auth-reset-password.module.ts ***!
      \******************************************************************************/

    /*! exports provided: AuthResetPasswordModule */

    /***/
    function B0WW(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AuthResetPasswordModule", function () {
        return AuthResetPasswordModule;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _auth_reset_password_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./auth-reset-password-routing.module */
      "N8Np");
      /* harmony import */


      var _auth_reset_password_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./auth-reset-password.component */
      "e8PA");

      var AuthResetPasswordModule = function AuthResetPasswordModule() {
        _classCallCheck(this, AuthResetPasswordModule);
      };

      AuthResetPasswordModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
        type: AuthResetPasswordModule
      });
      AuthResetPasswordModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
        factory: function AuthResetPasswordModule_Factory(t) {
          return new (t || AuthResetPasswordModule)();
        },
        imports: [[_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _auth_reset_password_routing_module__WEBPACK_IMPORTED_MODULE_2__["AuthResetPasswordRoutingModule"]]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](AuthResetPasswordModule, {
          declarations: [_auth_reset_password_component__WEBPACK_IMPORTED_MODULE_3__["AuthResetPasswordComponent"]],
          imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _auth_reset_password_routing_module__WEBPACK_IMPORTED_MODULE_2__["AuthResetPasswordRoutingModule"]]
        });
      })();
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AuthResetPasswordModule, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
          args: [{
            declarations: [_auth_reset_password_component__WEBPACK_IMPORTED_MODULE_3__["AuthResetPasswordComponent"]],
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _auth_reset_password_routing_module__WEBPACK_IMPORTED_MODULE_2__["AuthResetPasswordRoutingModule"]]
          }]
        }], null, null);
      })();
      /***/

    },

    /***/
    "BUwF":
    /*!***************************************************!*\
      !*** ./src/app/pages/auth/auth-routing.module.ts ***!
      \***************************************************/

    /*! exports provided: AuthRoutingModule */

    /***/
    function BUwF(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AuthRoutingModule", function () {
        return AuthRoutingModule;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");

      var routes = [{
        path: 'signin',
        loadChildren: function loadChildren() {
          return Promise.resolve().then(__webpack_require__.bind(null,
          /*! ../auth/auth-signin/auth-signin.module */
          "Mk9t")).then(function (module) {
            return module.AuthSigninModule;
          });
        }
      }, {
        path: 'investor-signup',
        loadChildren: function loadChildren() {
          return Promise.resolve().then(__webpack_require__.bind(null,
          /*! ../auth/auth-investor-signup/auth-investor-signup.module */
          "UItr")).then(function (module) {
            return module.AuthInvestorSignupModule;
          });
        }
      }, {
        path: 'reset-password',
        loadChildren: function loadChildren() {
          return Promise.resolve().then(__webpack_require__.bind(null,
          /*! ../auth/auth-reset-password/auth-reset-password.module */
          "B0WW")).then(function (module) {
            return module.AuthResetPasswordModule;
          });
        }
      }, {
        path: 'company-signup',
        loadChildren: function loadChildren() {
          return Promise.resolve().then(__webpack_require__.bind(null,
          /*! ../auth/company-signup/company-signup.module */
          "h5nQ")).then(function (module) {
            return module.CompanySignupModule;
          });
        }
      }];

      var AuthRoutingModule = function AuthRoutingModule() {
        _classCallCheck(this, AuthRoutingModule);
      };

      AuthRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
        type: AuthRoutingModule
      });
      AuthRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
        factory: function AuthRoutingModule_Factory(t) {
          return new (t || AuthRoutingModule)();
        },
        imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](AuthRoutingModule, {
          imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
          exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        });
      })();
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AuthRoutingModule, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
          args: [{
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
          }]
        }], null, null);
      })();
      /***/

    },

    /***/
    "Mk9t":
    /*!**************************************************************!*\
      !*** ./src/app/pages/auth/auth-signin/auth-signin.module.ts ***!
      \**************************************************************/

    /*! exports provided: AuthSigninModule */

    /***/
    function Mk9t(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AuthSigninModule", function () {
        return AuthSigninModule;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _auth_signin_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./auth-signin-routing.module */
      "9rsm");
      /* harmony import */


      var _auth_signin_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./auth-signin.component */
      "Zeh+");
      /* harmony import */


      var _theme_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../../../theme/shared/shared.module */
      "ebz3");

      var AuthSigninModule = function AuthSigninModule() {
        _classCallCheck(this, AuthSigninModule);
      };

      AuthSigninModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
        type: AuthSigninModule
      });
      AuthSigninModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
        factory: function AuthSigninModule_Factory(t) {
          return new (t || AuthSigninModule)();
        },
        imports: [[_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _auth_signin_routing_module__WEBPACK_IMPORTED_MODULE_2__["AuthSigninRoutingModule"], _theme_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"]]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](AuthSigninModule, {
          declarations: [_auth_signin_component__WEBPACK_IMPORTED_MODULE_3__["AuthSigninComponent"]],
          imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _auth_signin_routing_module__WEBPACK_IMPORTED_MODULE_2__["AuthSigninRoutingModule"], _theme_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"]]
        });
      })();
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AuthSigninModule, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
          args: [{
            declarations: [_auth_signin_component__WEBPACK_IMPORTED_MODULE_3__["AuthSigninComponent"]],
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _auth_signin_routing_module__WEBPACK_IMPORTED_MODULE_2__["AuthSigninRoutingModule"], _theme_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"]]
          }]
        }], null, null);
      })();
      /***/

    },

    /***/
    "N8Np":
    /*!**************************************************************************************!*\
      !*** ./src/app/pages/auth/auth-reset-password/auth-reset-password-routing.module.ts ***!
      \**************************************************************************************/

    /*! exports provided: AuthResetPasswordRoutingModule */

    /***/
    function N8Np(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AuthResetPasswordRoutingModule", function () {
        return AuthResetPasswordRoutingModule;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _auth_reset_password_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./auth-reset-password.component */
      "e8PA");

      var routes = [{
        path: '',
        component: _auth_reset_password_component__WEBPACK_IMPORTED_MODULE_2__["AuthResetPasswordComponent"]
      }];

      var AuthResetPasswordRoutingModule = function AuthResetPasswordRoutingModule() {
        _classCallCheck(this, AuthResetPasswordRoutingModule);
      };

      AuthResetPasswordRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
        type: AuthResetPasswordRoutingModule
      });
      AuthResetPasswordRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
        factory: function AuthResetPasswordRoutingModule_Factory(t) {
          return new (t || AuthResetPasswordRoutingModule)();
        },
        imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](AuthResetPasswordRoutingModule, {
          imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
          exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        });
      })();
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AuthResetPasswordRoutingModule, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
          args: [{
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
          }]
        }], null, null);
      })();
      /***/

    },

    /***/
    "S3YR":
    /*!***********************************************************************!*\
      !*** ./src/app/pages/auth/company-signup/company-signup.component.ts ***!
      \***********************************************************************/

    /*! exports provided: CompanySignupComponent */

    /***/
    function S3YR(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CompanySignupComponent", function () {
        return CompanySignupComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../../../services/auth.service */
      "lGQG");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _theme_shared_components_card_card_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../../../theme/shared/components/card/card.component */
      "/n7v");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var angular_archwizard__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! angular-archwizard */
      "mkVx");
      /* harmony import */


      var ng_select__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ng-select */
      "lTK2");

      function CompanySignupComponent_div_4_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 51);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "strong");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "Ooops! ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx_r0.error_message, " ");
        }
      }

      var CompanySignupComponent = /*#__PURE__*/function () {
        function CompanySignupComponent(fb, authService, route) {
          _classCallCheck(this, CompanySignupComponent);

          this.fb = fb;
          this.authService = authService;
          this.route = route;
          this.haserror = false;
          this.simpleOption = [{
            value: 'E-Commerce',
            label: 'E-Commerce'
          }, {
            value: 'Market Network',
            label: 'Market Network'
          }, {
            value: 'Social Network',
            label: 'Social Network'
          }];
        }

        _createClass(CompanySignupComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.createSignupForm();
          }
        }, {
          key: "createSignupForm",
          value: function createSignupForm() {
            this.formSignup = this.fb.group({
              username: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
              password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
              email: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
              name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
              website: [''],
              city: [''],
              country: [''],
              sector: [''],
              stage: [''],
              type_of_business: [''],
              revenue: [''],
              currency: [''],
              description: [''],
              logo: [''],
              link_video: ['']
            });
          }
        }, {
          key: "onLogin",
          value: function onLogin() {
            var _this = this;

            console.log(this.formSignup.value);
            this.authService.comapnySignup(this.formSignup.value).subscribe(function (value) {
              console.log(value.body);

              _this.route.navigate(['/auth/signin']);
            }, function (error) {
              _this.haserror = true;
              _this.error_message = error.error.message;
              console.log(error.error.message);
            });
          }
        }]);

        return CompanySignupComponent;
      }();

      CompanySignupComponent.ɵfac = function CompanySignupComponent_Factory(t) {
        return new (t || CompanySignupComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]));
      };

      CompanySignupComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: CompanySignupComponent,
        selectors: [["app-company-signup"]],
        decls: 156,
        vars: 6,
        consts: [[1, "auth-wrapper"], [1, "col-sm-12"], [3, "formGroup", "ngSubmit"], ["cardTitle", "Basic Wizard", 3, "options"], ["class", "alert alert-danger alert-dismissible fade show", "role", "alert", 4, "ngIf"], [1, "arc-wizard"], ["wizard", ""], ["stepTitle", "credentials"], [1, "row"], [1, "col-md-6"], [1, "form-group"], ["for", "exampleInputEmail1"], ["type", "email", "id", "exampleInputEmail1", "aria-describedby", "emailHelp", "placeholder", "Enter email", "name", "email", "formControlName", "email", 1, "form-control"], ["for", "password"], ["type", "password", "id", "password", "placeholder", "Password", "name", "password", "formControlName", "password", 1, "form-control"], ["type", "text", "placeholder", "Username", "name", "username", "formControlName", "username", 1, "form-control"], ["for", "password2"], ["type", "password", "id", "password2", "placeholder", "Re-password", 1, "form-control"], [1, "col-sm-12", "centered-content"], [1, "btn-group", "mt-10"], ["type", "button", "awNextStep", "", 1, "btn", "btn-primary"], ["stepTitle", "Basic Information"], ["for", "name"], [1, "text-danger"], ["type", "text", "id", "name", "placeholder", "Enter the name of your company", "name", "name", "formControlName", "name", 1, "form-control"], ["for", "city"], ["type", "text", "id", "city", "placeholder", "In which city are your headquarters based?", "name", "city", "formControlName", "city", 1, "form-control"], ["for", "sector"], ["id", "sector", "name", "sector", "formControlName", "sector", 1, "form-control"], ["selected", ""], ["for", "website"], ["type", "text", "placeholder", "https://", "id", "website", "name", "website", "formControlName", "website", 1, "form-control"], ["for", "country"], ["id", "country", "name", "country", "formControlName", "country", 1, "form-control"], ["for", "type_of_business"], ["id", "type_of_business", "name", "type_of_business", "formControlName", "type_of_business", 3, "ngClass", "options", "multiple"], ["stepTitle", "Business"], ["for", "stage"], ["id", "stage", "name", "stage", "formControlName", "stage", 1, "form-control"], ["for", "currency"], ["id", "currency", "name", "currency", "formControlName", "currency", 1, "form-control"], ["for", "revenue"], ["type", "number", "placeholder", "How much revenue did you make in the past 6 months?", "id", "revenue", "name", "revenue", "formControlName", "revenue", 1, "form-control"], ["stepTitle", "Finished"], ["for", "logo"], [1, "custom-file"], ["type", "file", "id", "logo", "name", "logo", "formControlName", "logo", 1, "custom-file-input"], ["for", "logo", 1, "custom-file-label"], ["for", "description"], ["id", "description", "rows", "3", "name", "description", "formControlName", "description", 1, "form-control"], ["type", "submit", "awNextStep", "", 1, "btn", "btn-primary"], ["role", "alert", 1, "alert", "alert-danger", "alert-dismissible", "fade", "show"]],
        template: function CompanySignupComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "form", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngSubmit", function CompanySignupComponent_Template_form_ngSubmit_2_listener() {
              return ctx.onLogin();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "app-card", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, CompanySignupComponent_div_4_Template, 4, 1, "div", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "aw-wizard", 5, 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "aw-wizard-step", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "label", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "Email address");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](15, "input", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "label", 13);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "Password");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](19, "input", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "label");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, "Username");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](24, "input", 15);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "label", 16);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](27, "Re-password");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](28, "input", 17);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "div", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "div", 19);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "button", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](32, "Continue");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "aw-wizard-step", 21);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "div", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "div", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "div", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "label", 22);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](40, "Company name: ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "span", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](42, "*");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](43, "input", 24);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "label", 25);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](46, "City of Incorporation:");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "span", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](48, "*");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](49, "input", 26);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](51, "label", 27);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](52, "What sector are you in?:");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "span", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](54, "*");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](55, "select", 28);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](56, "option", 29);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](57, "Select A Sector");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](58, "option");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](59, "Software (Web Marketplace Saas..)");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "option");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](61, "IT (& TMT)");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](62, "option");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](63, "Media");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](64, "div", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](65, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "label", 30);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](67, "Company Website");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](68, "input", 31);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](69, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](70, "label", 32);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](71, "Country of Incorporation:");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](72, "span", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](73, "*");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](74, "select", 33);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](75, "option", 29);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](76, "Select A Country");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](77, "option");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](78, "U.S.A");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](79, "option");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](80, "U.K");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](81, "option");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](82, "Canada");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](83, "option");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](84, "France");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](85, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](86, "label", 34);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](87, "What type of business is your company? ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](88, "ng-select", 35);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](89, "div", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](90, "div", 19);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](91, "button", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](92, "Continue");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](93, "aw-wizard-step", 36);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](94, "div", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](95, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](96, "div", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](97, "div", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](98, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](99, "label", 37);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](100, "Stage :");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](101, "span", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](102, "*");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](103, "select", 38);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](104, "option", 29);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](105, "What stage is your company at?");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](106, "option");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](107, "Idea");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](108, "option");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](109, "Revenues");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](110, "option");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](111, "Start up");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](112, "option");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](113, "Other");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](114, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](115, "label", 39);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](116, "Currency");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](117, "span", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](118, "*");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](119, "select", 40);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](120, "option", 29);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](121, "$");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](122, "option");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](123, "\u20AC");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](124, "div", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](125, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](126, "label", 41);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](127, "How much revenue did you make?");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](128, "span", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](129, "*");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](130, "input", 42);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](131, "div", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](132, "div", 19);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](133, "button", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](134, "Continue");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](135, "aw-wizard-step", 43);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](136, "div", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](137, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](138, "div", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](139, "div", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](140, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](141, "label", 44);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](142, "Upload Your Logo:");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](143, "div", 45);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](144, "input", 46);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](145, "label", 47);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](146, "Choose file");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](147, "div", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](148, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](149, "label", 48);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](150, "Describe what your company does");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](151, "textarea", 49);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](152, "div", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](153, "div", 19);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](154, "button", 50);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](155, "Continue");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.formSignup);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("options", false);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.haserror);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](84);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", "ng-select")("options", ctx.simpleOption)("multiple", true);
          }
        },
        directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"], _theme_shared_components_card_card_component__WEBPACK_IMPORTED_MODULE_4__["CardComponent"], _angular_common__WEBPACK_IMPORTED_MODULE_5__["NgIf"], angular_archwizard__WEBPACK_IMPORTED_MODULE_6__["WizardComponent"], angular_archwizard__WEBPACK_IMPORTED_MODULE_6__["WizardStepComponent"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControlName"], angular_archwizard__WEBPACK_IMPORTED_MODULE_6__["NextStepDirective"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["SelectControlValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgSelectOption"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_x"], ng_select__WEBPACK_IMPORTED_MODULE_7__["SelectComponent"], _angular_common__WEBPACK_IMPORTED_MODULE_5__["NgClass"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NumberValueAccessor"]],
        styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJjb21wYW55LXNpZ251cC5jb21wb25lbnQuc2NzcyJ9 */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](CompanySignupComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-company-signup',
            templateUrl: './company-signup.component.html',
            styleUrls: ['./company-signup.component.scss']
          }]
        }], function () {
          return [{
            type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]
          }, {
            type: _services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]
          }, {
            type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "SEHK":
    /*!***********************************************************************************!*\
      !*** ./src/app/pages/auth/auth-investor-signup/auth-investor-signup.component.ts ***!
      \***********************************************************************************/

    /*! exports provided: AuthInvestorSignupComponent */

    /***/
    function SEHK(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AuthInvestorSignupComponent", function () {
        return AuthInvestorSignupComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../../../services/auth.service */
      "lGQG");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _theme_shared_components_card_card_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../../../theme/shared/components/card/card.component */
      "/n7v");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var angular_archwizard__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! angular-archwizard */
      "mkVx");

      function AuthInvestorSignupComponent_div_3_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 25);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "strong");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "Ooops! ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx_r0.error_message, " ");
        }
      }

      var _c0 = function _c0() {
        return {
          symbol: "1"
        };
      };

      var _c1 = function _c1() {
        return {
          symbol: "2"
        };
      };

      var _c2 = function _c2() {
        return {
          symbol: "3"
        };
      };

      var _c3 = function _c3() {
        return {
          symbol: "4"
        };
      };

      var AuthInvestorSignupComponent = /*#__PURE__*/function () {
        function AuthInvestorSignupComponent(authService, route, elementRef, fb) {
          _classCallCheck(this, AuthInvestorSignupComponent);

          this.authService = authService;
          this.route = route;
          this.elementRef = elementRef;
          this.fb = fb;
          this.haserror = false;
          this.step1 = true;
          this.step2 = true;
          this.step3 = true;
          this.isSubmit = false;
        }

        _createClass(AuthInvestorSignupComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.createSignupForm();
          }
        }, {
          key: "onLogin",
          value: function onLogin() {
            var _this2 = this;

            console.log(this.formSignup.value);
            this.authService.signup(this.formSignup.value).subscribe(function (value) {
              console.log(value.body);

              _this2.route.navigate(['/auth/signin']);
            }, function (error) {
              _this2.haserror = true;
              _this2.error_message = error.error.message;
              console.log(error.error.message);
            });
          }
          /*
              private createSignupForm() {
            this.formSignup = this.fb.group({
              username: ['', Validators.required],
              password: ['', Validators.required],
              email: ['', Validators.required],
              name: ['', Validators.required],
              website: [''],
              city: [''],
              country: [''],
              sector: [''],
              stage: [''],
              revenue: [''],
              currency: [''],
              description: [''],
              logo: [''],
              link_video: [''],
            });
          }
           */

        }, {
          key: "createSignupForm",
          value: function createSignupForm() {
            this.formSignup = this.fb.group({
              username: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
              password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
              email: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
              first_name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
              last_name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
              adress: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
              city: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
              phone: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]
            });
          }
        }, {
          key: "enterSecondStep",
          value: function enterSecondStep($event) {
            console.log($event);
          }
        }, {
          key: "checkStep1",
          value: function checkStep1() {
            if (!(this.formSignup.controls.username.value == '') && !(this.formSignup.controls.password.value == '') && !(this.formSignup.controls.email.value == '')) {
              this.step1 = false;
            } else if (!(this.formSignup.controls.username.value == '') || !(this.formSignup.controls.password.value == '') || !(this.formSignup.controls.email.value == '')) {
              this.step1 = true;
            }
          }
        }, {
          key: "checkStep2",
          value: function checkStep2() {
            if (!(this.formSignup.controls.first_name.value == '') && !(this.formSignup.controls.last_name.value == '')) {
              this.step2 = false;
            } else if (!(this.formSignup.controls.first_name.value == '') || !(this.formSignup.controls.last_name.value == '')) {
              this.step2 = true;
            }
          }
        }, {
          key: "checkStep3",
          value: function checkStep3() {
            if (!(this.formSignup.controls.adress.value == '') && !(this.formSignup.controls.city.value == '') && !(this.formSignup.controls.phone.value == '')) {
              this.step3 = false;
            } else if (!(this.formSignup.controls.adress.value == '') || !(this.formSignup.controls.city.value == '') || !(this.formSignup.controls.phone.value == '')) {
              this.step3 = true;
            }
          }
        }]);

        return AuthInvestorSignupComponent;
      }();

      AuthInvestorSignupComponent.ɵfac = function AuthInvestorSignupComponent_Factory(t) {
        return new (t || AuthInvestorSignupComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]));
      };

      AuthInvestorSignupComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: AuthInvestorSignupComponent,
        selectors: [["app-auth-investor-signup"]],
        decls: 65,
        vars: 15,
        consts: [[1, "auth-wrapper"], [1, "col-sm-12", "col-md-6"], ["cardTitle", "Create your account by filling the form below", 3, "options"], ["class", "alert alert-danger alert-dismissible fade show", "role", "alert", 4, "ngIf"], [3, "formGroup", "ngSubmit"], [1, "arc-wizard", 3, "navBarLayout"], ["wizard", ""], ["stepTitle", "", 3, "navigationSymbol"], [1, "row"], [1, "col-sm-12"], [1, "input-group", "mb-3"], ["type", "text", "placeholder", "Username", "name", "username", "formControlName", "username", 1, "form-control", 3, "input"], ["type", "text", "placeholder", "Email", "name", "email", "formControlName", "email", 1, "form-control", 3, "input"], [1, "input-group", "mb-4"], ["type", "password", "placeholder", "Password", "name", "password", "formControlName", "password", 1, "form-control", 3, "input"], [1, "col-sm-12", "centered-content"], [1, "btn-group", "mt-10"], ["type", "button", "awNextStep", "", 1, "btn", "btn-primary", "btn-sm", 3, "hidden"], ["type", "text", "placeholder", "First name", "name", "first_name", "formControlName", "first_name", 1, "form-control", 3, "input"], ["type", "text", "placeholder", "Last name", "name", "last_name", "formControlName", "last_name", 1, "form-control", 3, "input"], ["type", "button", "awPreviousStep", "", 1, "btn", "btn-secondary", "btn-sm"], ["type", "text", "placeholder", "Phone", "name", "phone", "formControlName", "phone", 1, "form-control", 3, "input"], ["type", "text", "placeholder", "adress", "name", "adress", "formControlName", "adress", 1, "form-control", 3, "input"], ["type", "text", "placeholder", "City", "name", "city", "formControlName", "city", 1, "form-control", 3, "input"], ["type", "submit", "awResetWizard", "", 1, "btn", "btn-success", "btn-sm"], ["role", "alert", 1, "alert", "alert-danger", "alert-dismissible", "fade", "show"]],
        template: function AuthInvestorSignupComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "app-card", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, AuthInvestorSignupComponent_div_3_Template, 4, 1, "div", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "form", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngSubmit", function AuthInvestorSignupComponent_Template_form_ngSubmit_4_listener() {
              return ctx.onLogin();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "aw-wizard", 5, 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "aw-wizard-step", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "h5");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "Credentials");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](12, "hr");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "input", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("input", function AuthInvestorSignupComponent_Template_input_input_14_listener() {
              return ctx.checkStep1();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "input", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("input", function AuthInvestorSignupComponent_Template_input_input_16_listener() {
              return ctx.checkStep1();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 13);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "input", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("input", function AuthInvestorSignupComponent_Template_input_input_18_listener() {
              return ctx.checkStep1();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 15);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 16);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "button", 17);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22, "Continue");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "aw-wizard-step", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "div", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "h5");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](27, "Profile");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](28, "hr");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "input", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("input", function AuthInvestorSignupComponent_Template_input_input_30_listener() {
              return ctx.checkStep2();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "input", 19);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("input", function AuthInvestorSignupComponent_Template_input_input_32_listener() {
              return ctx.checkStep2();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "div", 15);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "div", 16);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "button", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](36, "Back");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "button", 17);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](38, "Continue");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "aw-wizard-step", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "div", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "div", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "h5");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](43, "Contact");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](44, "hr");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "input", 21);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("input", function AuthInvestorSignupComponent_Template_input_input_46_listener() {
              return ctx.checkStep3();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "input", 22);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("input", function AuthInvestorSignupComponent_Template_input_input_48_listener() {
              return ctx.checkStep3();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "input", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("input", function AuthInvestorSignupComponent_Template_input_input_50_listener() {
              return ctx.checkStep3();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](51, "div", 15);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "div", 16);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "button", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](54, "Back");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](55, "button", 17);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](56, "Continue");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "aw-wizard-step", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](58, "div", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](59, "div", 15);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "div", 16);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](61, "button", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](62, "Back");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](63, "button", 24);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](64, "Finished");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("options", false);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.haserror);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.formSignup);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("navBarLayout", "large-filled");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("navigationSymbol", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](11, _c0));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("hidden", ctx.step1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("navigationSymbol", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](12, _c1));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("hidden", ctx.step2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("navigationSymbol", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](13, _c2));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](16);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("hidden", ctx.step3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("navigationSymbol", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](14, _c3));
          }
        },
        directives: [_theme_shared_components_card_card_component__WEBPACK_IMPORTED_MODULE_4__["CardComponent"], _angular_common__WEBPACK_IMPORTED_MODULE_5__["NgIf"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"], angular_archwizard__WEBPACK_IMPORTED_MODULE_6__["WizardComponent"], angular_archwizard__WEBPACK_IMPORTED_MODULE_6__["WizardStepComponent"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControlName"], angular_archwizard__WEBPACK_IMPORTED_MODULE_6__["NextStepDirective"], angular_archwizard__WEBPACK_IMPORTED_MODULE_6__["PreviousStepDirective"], angular_archwizard__WEBPACK_IMPORTED_MODULE_6__["ResetWizardDirective"]],
        styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhdXRoLWludmVzdG9yLXNpZ251cC5jb21wb25lbnQuc2NzcyJ9 */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AuthInvestorSignupComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-auth-investor-signup',
            templateUrl: './auth-investor-signup.component.html',
            styleUrls: ['./auth-investor-signup.component.scss']
          }]
        }], function () {
          return [{
            type: _services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]
          }, {
            type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
          }, {
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]
          }, {
            type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "UItr":
    /*!********************************************************************************!*\
      !*** ./src/app/pages/auth/auth-investor-signup/auth-investor-signup.module.ts ***!
      \********************************************************************************/

    /*! exports provided: AuthInvestorSignupModule */

    /***/
    function UItr(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AuthInvestorSignupModule", function () {
        return AuthInvestorSignupModule;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _auth_investor_signup_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./auth-investor-signup-routing.module */
      "zdKD");
      /* harmony import */


      var _auth_investor_signup_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./auth-investor-signup.component */
      "SEHK");
      /* harmony import */


      var _theme_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../../../theme/shared/shared.module */
      "ebz3");
      /* harmony import */


      var angular_archwizard__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! angular-archwizard */
      "mkVx");

      var AuthInvestorSignupModule = function AuthInvestorSignupModule() {
        _classCallCheck(this, AuthInvestorSignupModule);
      };

      AuthInvestorSignupModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
        type: AuthInvestorSignupModule
      });
      AuthInvestorSignupModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
        factory: function AuthInvestorSignupModule_Factory(t) {
          return new (t || AuthInvestorSignupModule)();
        },
        imports: [[_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _auth_investor_signup_routing_module__WEBPACK_IMPORTED_MODULE_2__["AuthInvestorSignupRoutingModule"], _theme_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"], angular_archwizard__WEBPACK_IMPORTED_MODULE_5__["ArchwizardModule"]]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](AuthInvestorSignupModule, {
          declarations: [_auth_investor_signup_component__WEBPACK_IMPORTED_MODULE_3__["AuthInvestorSignupComponent"]],
          imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _auth_investor_signup_routing_module__WEBPACK_IMPORTED_MODULE_2__["AuthInvestorSignupRoutingModule"], _theme_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"], angular_archwizard__WEBPACK_IMPORTED_MODULE_5__["ArchwizardModule"]]
        });
      })();
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AuthInvestorSignupModule, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
          args: [{
            declarations: [_auth_investor_signup_component__WEBPACK_IMPORTED_MODULE_3__["AuthInvestorSignupComponent"]],
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _auth_investor_signup_routing_module__WEBPACK_IMPORTED_MODULE_2__["AuthInvestorSignupRoutingModule"], _theme_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"], angular_archwizard__WEBPACK_IMPORTED_MODULE_5__["ArchwizardModule"]]
          }]
        }], null, null);
      })();
      /***/

    },

    /***/
    "Zeh+":
    /*!*****************************************************************!*\
      !*** ./src/app/pages/auth/auth-signin/auth-signin.component.ts ***!
      \*****************************************************************/

    /*! exports provided: AuthSigninComponent */

    /***/
    function Zeh(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AuthSigninComponent", function () {
        return AuthSigninComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /* harmony import */


      var _services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../../../services/auth.service */
      "lGQG");
      /* harmony import */


      var _services_storage_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ../../../services/storage.service */
      "n90K");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");

      function AuthSigninComponent_div_10_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 27);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "strong");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "Ooops! ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx_r0.error, " ");
        }
      }

      var _c0 = function _c0() {
        return ["/auth/reset-password"];
      };

      var _c1 = function _c1() {
        return ["/auth/investor-signup"];
      };

      var _c2 = function _c2() {
        return ["/auth/company-signup"];
      };

      var AuthSigninComponent = /*#__PURE__*/function () {
        function AuthSigninComponent(fb, authService, storage, route) {
          _classCallCheck(this, AuthSigninComponent);

          this.fb = fb;
          this.authService = authService;
          this.storage = storage;
          this.route = route;
        }

        _createClass(AuthSigninComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.createSinginForm();
            this.haserror = false;
          }
        }, {
          key: "onLogin",
          value: function onLogin() {
            var _this3 = this;

            console.log(this.form.value);
            this.authService.login(this.form.value).subscribe(function (value) {
              console.log(value.body);

              if (_this3.storage.getRoleUser() === "ROLE_INVESTOR") {
                _this3.route.navigate(['/dashboard/investor']);
              } else if (_this3.storage.getRoleUser() === "ROLE_COMPANY") {
                _this3.route.navigate(['/dashboard/company']);
              }
            }, function (err) {
              _this3.haserror = true;
              _this3.error = err.error.message;
              console.log('gggggg ' + JSON.stringify(err.error.message));
            });
          }
        }, {
          key: "createSinginForm",
          value: function createSinginForm() {
            this.form = this.fb.group({
              username: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
              password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]
            });
          }
        }]);

        return AuthSigninComponent;
      }();

      AuthSigninComponent.ɵfac = function AuthSigninComponent_Factory(t) {
        return new (t || AuthSigninComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_storage_service__WEBPACK_IMPORTED_MODULE_3__["StorageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]));
      };

      AuthSigninComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: AuthSigninComponent,
        selectors: [["app-auth-signin"]],
        decls: 42,
        vars: 8,
        consts: [[1, "auth-wrapper", "align-items-stretch", "aut-bg-img"], [1, "flex-grow-1"], [1, "h-100", "d-md-flex", "align-items-center", "auth-side-img"], [1, "col-sm-10", "auth-content", "w-auto"], [1, "text-white", "my-4"], [1, "text-white", "font-weight-normal"], [1, "auth-side-form"], [1, "auth-content"], ["class", "alert alert-warning alert-dismissible fade show", "role", "alert", 4, "ngIf"], [3, "formGroup", "ngSubmit"], [1, "mb-4", "f-w-400"], [1, "input-group", "mb-3"], ["type", "text", "placeholder", "Username", "name", "username", "formControlName", "username", 1, "form-control"], [1, "input-group", "mb-4"], ["type", "password", "placeholder", "Password", "name", "password", "formControlName", "password", 1, "form-control"], ["type", "submit", 1, "btn", "btn-block", "btn-primary", "mb-0"], [1, "text-center"], [1, "saprator", "my-4"], [1, "btn", "text-white", "bg-facebook", "mb-2", "mr-2", "wid-40", "px-0", "hei-40", "rounded-circle"], [1, "fab", "fa-facebook-f"], [1, "btn", "text-white", "bg-googleplus", "mb-2", "mr-2", "wid-40", "px-0", "hei-40", "rounded-circle"], [1, "fab", "fa-google-plus-g"], [1, "btn", "text-white", "bg-twitter", "mb-2", "wid-40", "px-0", "hei-40", "rounded-circle"], [1, "fab", "fa-twitter"], [1, "mb-2", "text-muted"], [1, "f-w-400", 3, "routerLink"], [1, "mb-0", "text-muted"], ["role", "alert", 1, "alert", "alert-warning", "alert-dismissible", "fade", "show"]],
        template: function AuthSigninComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Welcome Back!");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "h4", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "Signin to your account and get explore our leopard solutions.");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](10, AuthSigninComponent_div_10_Template, 4, 1, "div", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "form", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngSubmit", function AuthSigninComponent_Template_form_ngSubmit_11_listener() {
              return ctx.onLogin();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "h3", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "Signin");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](15, "input", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "div", 13);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](17, "input", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "button", 15);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "Signin");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 16);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "div", 17);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "span");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, "OR");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "button", 18);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](25, "i", 19);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "button", 20);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](27, "i", 21);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "button", 22);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](29, "i", 23);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "p", 24);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](31, "Forgot password? ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "a", 25);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](33, "Reset");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "p", 26);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](35, "Don\u2019t have an Investor account? ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "a", 25);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](37, "Signup");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "p", 26);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](39, "Don\u2019t have an Entrepreneur account? ");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "a", 25);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](41, "Signup");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.haserror);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.form);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](21);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](5, _c0));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](6, _c1));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](7, _c2));
          }
        },
        directives: [_angular_common__WEBPACK_IMPORTED_MODULE_5__["NgIf"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControlName"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterLinkWithHref"]],
        styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhdXRoLXNpZ25pbi5jb21wb25lbnQuc2NzcyJ9 */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AuthSigninComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-auth-signin',
            templateUrl: './auth-signin.component.html',
            styleUrls: ['./auth-signin.component.scss']
          }]
        }], function () {
          return [{
            type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]
          }, {
            type: _services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]
          }, {
            type: _services_storage_service__WEBPACK_IMPORTED_MODULE_3__["StorageService"]
          }, {
            type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
          }];
        }, null);
      })();
      /***/

    },

    /***/
    "dpFU":
    /*!*************************************************************************!*\
      !*** ./node_modules/angular-archwizard/node_modules/tslib/tslib.es6.js ***!
      \*************************************************************************/

    /*! exports provided: __extends, __assign, __rest, __decorate, __param, __metadata, __awaiter, __generator, __createBinding, __exportStar, __values, __read, __spread, __spreadArrays, __await, __asyncGenerator, __asyncDelegator, __asyncValues, __makeTemplateObject, __importStar, __importDefault, __classPrivateFieldGet, __classPrivateFieldSet */

    /***/
    function dpFU(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "__extends", function () {
        return __extends;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "__assign", function () {
        return _assign;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "__rest", function () {
        return __rest;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "__decorate", function () {
        return __decorate;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "__param", function () {
        return __param;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "__metadata", function () {
        return __metadata;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "__awaiter", function () {
        return __awaiter;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "__generator", function () {
        return __generator;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "__createBinding", function () {
        return __createBinding;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "__exportStar", function () {
        return __exportStar;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "__values", function () {
        return __values;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "__read", function () {
        return __read;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "__spread", function () {
        return __spread;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "__spreadArrays", function () {
        return __spreadArrays;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "__await", function () {
        return __await;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "__asyncGenerator", function () {
        return __asyncGenerator;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "__asyncDelegator", function () {
        return __asyncDelegator;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "__asyncValues", function () {
        return __asyncValues;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "__makeTemplateObject", function () {
        return __makeTemplateObject;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "__importStar", function () {
        return __importStar;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "__importDefault", function () {
        return __importDefault;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "__classPrivateFieldGet", function () {
        return __classPrivateFieldGet;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "__classPrivateFieldSet", function () {
        return __classPrivateFieldSet;
      });
      /*! *****************************************************************************
      Copyright (c) Microsoft Corporation.
      
      Permission to use, copy, modify, and/or distribute this software for any
      purpose with or without fee is hereby granted.
      
      THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
      REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
      AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
      INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
      LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
      OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
      PERFORMANCE OF THIS SOFTWARE.
      ***************************************************************************** */

      /* global Reflect, Promise */


      var _extendStatics = function extendStatics(d, b) {
        _extendStatics = Object.setPrototypeOf || {
          __proto__: []
        } instanceof Array && function (d, b) {
          d.__proto__ = b;
        } || function (d, b) {
          for (var p in b) {
            if (b.hasOwnProperty(p)) d[p] = b[p];
          }
        };

        return _extendStatics(d, b);
      };

      function __extends(d, b) {
        _extendStatics(d, b);

        function __() {
          this.constructor = d;
        }

        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
      }

      var _assign = function __assign() {
        _assign = Object.assign || function __assign(t) {
          for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];

            for (var p in s) {
              if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
            }
          }

          return t;
        };

        return _assign.apply(this, arguments);
      };

      function __rest(s, e) {
        var t = {};

        for (var p in s) {
          if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
        }

        if (s != null && typeof Object.getOwnPropertySymbols === "function") for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
          if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i])) t[p[i]] = s[p[i]];
        }
        return t;
      }

      function __decorate(decorators, target, key, desc) {
        var c = arguments.length,
            r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
            d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
          if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        }
        return c > 3 && r && Object.defineProperty(target, key, r), r;
      }

      function __param(paramIndex, decorator) {
        return function (target, key) {
          decorator(target, key, paramIndex);
        };
      }

      function __metadata(metadataKey, metadataValue) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
      }

      function __awaiter(thisArg, _arguments, P, generator) {
        function adopt(value) {
          return value instanceof P ? value : new P(function (resolve) {
            resolve(value);
          });
        }

        return new (P || (P = Promise))(function (resolve, reject) {
          function fulfilled(value) {
            try {
              step(generator.next(value));
            } catch (e) {
              reject(e);
            }
          }

          function rejected(value) {
            try {
              step(generator["throw"](value));
            } catch (e) {
              reject(e);
            }
          }

          function step(result) {
            result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
          }

          step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
      }

      function __generator(thisArg, body) {
        var _ = {
          label: 0,
          sent: function sent() {
            if (t[0] & 1) throw t[1];
            return t[1];
          },
          trys: [],
          ops: []
        },
            f,
            y,
            t,
            g;
        return g = {
          next: verb(0),
          "throw": verb(1),
          "return": verb(2)
        }, typeof Symbol === "function" && (g[Symbol.iterator] = function () {
          return this;
        }), g;

        function verb(n) {
          return function (v) {
            return step([n, v]);
          };
        }

        function step(op) {
          if (f) throw new TypeError("Generator is already executing.");

          while (_) {
            try {
              if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
              if (y = 0, t) op = [op[0] & 2, t.value];

              switch (op[0]) {
                case 0:
                case 1:
                  t = op;
                  break;

                case 4:
                  _.label++;
                  return {
                    value: op[1],
                    done: false
                  };

                case 5:
                  _.label++;
                  y = op[1];
                  op = [0];
                  continue;

                case 7:
                  op = _.ops.pop();

                  _.trys.pop();

                  continue;

                default:
                  if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) {
                    _ = 0;
                    continue;
                  }

                  if (op[0] === 3 && (!t || op[1] > t[0] && op[1] < t[3])) {
                    _.label = op[1];
                    break;
                  }

                  if (op[0] === 6 && _.label < t[1]) {
                    _.label = t[1];
                    t = op;
                    break;
                  }

                  if (t && _.label < t[2]) {
                    _.label = t[2];

                    _.ops.push(op);

                    break;
                  }

                  if (t[2]) _.ops.pop();

                  _.trys.pop();

                  continue;
              }

              op = body.call(thisArg, _);
            } catch (e) {
              op = [6, e];
              y = 0;
            } finally {
              f = t = 0;
            }
          }

          if (op[0] & 5) throw op[1];
          return {
            value: op[0] ? op[1] : void 0,
            done: true
          };
        }
      }

      function __createBinding(o, m, k, k2) {
        if (k2 === undefined) k2 = k;
        o[k2] = m[k];
      }

      function __exportStar(m, exports) {
        for (var p in m) {
          if (p !== "default" && !exports.hasOwnProperty(p)) exports[p] = m[p];
        }
      }

      function __values(o) {
        var s = typeof Symbol === "function" && Symbol.iterator,
            m = s && o[s],
            i = 0;
        if (m) return m.call(o);
        if (o && typeof o.length === "number") return {
          next: function next() {
            if (o && i >= o.length) o = void 0;
            return {
              value: o && o[i++],
              done: !o
            };
          }
        };
        throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
      }

      function __read(o, n) {
        var m = typeof Symbol === "function" && o[Symbol.iterator];
        if (!m) return o;
        var i = m.call(o),
            r,
            ar = [],
            e;

        try {
          while ((n === void 0 || n-- > 0) && !(r = i.next()).done) {
            ar.push(r.value);
          }
        } catch (error) {
          e = {
            error: error
          };
        } finally {
          try {
            if (r && !r.done && (m = i["return"])) m.call(i);
          } finally {
            if (e) throw e.error;
          }
        }

        return ar;
      }

      function __spread() {
        for (var ar = [], i = 0; i < arguments.length; i++) {
          ar = ar.concat(__read(arguments[i]));
        }

        return ar;
      }

      function __spreadArrays() {
        for (var s = 0, i = 0, il = arguments.length; i < il; i++) {
          s += arguments[i].length;
        }

        for (var r = Array(s), k = 0, i = 0; i < il; i++) {
          for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++) {
            r[k] = a[j];
          }
        }

        return r;
      }

      ;

      function __await(v) {
        return this instanceof __await ? (this.v = v, this) : new __await(v);
      }

      function __asyncGenerator(thisArg, _arguments, generator) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var g = generator.apply(thisArg, _arguments || []),
            i,
            q = [];
        return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () {
          return this;
        }, i;

        function verb(n) {
          if (g[n]) i[n] = function (v) {
            return new Promise(function (a, b) {
              q.push([n, v, a, b]) > 1 || resume(n, v);
            });
          };
        }

        function resume(n, v) {
          try {
            step(g[n](v));
          } catch (e) {
            settle(q[0][3], e);
          }
        }

        function step(r) {
          r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r);
        }

        function fulfill(value) {
          resume("next", value);
        }

        function reject(value) {
          resume("throw", value);
        }

        function settle(f, v) {
          if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]);
        }
      }

      function __asyncDelegator(o) {
        var i, p;
        return i = {}, verb("next"), verb("throw", function (e) {
          throw e;
        }), verb("return"), i[Symbol.iterator] = function () {
          return this;
        }, i;

        function verb(n, f) {
          i[n] = o[n] ? function (v) {
            return (p = !p) ? {
              value: __await(o[n](v)),
              done: n === "return"
            } : f ? f(v) : v;
          } : f;
        }
      }

      function __asyncValues(o) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var m = o[Symbol.asyncIterator],
            i;
        return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () {
          return this;
        }, i);

        function verb(n) {
          i[n] = o[n] && function (v) {
            return new Promise(function (resolve, reject) {
              v = o[n](v), settle(resolve, reject, v.done, v.value);
            });
          };
        }

        function settle(resolve, reject, d, v) {
          Promise.resolve(v).then(function (v) {
            resolve({
              value: v,
              done: d
            });
          }, reject);
        }
      }

      function __makeTemplateObject(cooked, raw) {
        if (Object.defineProperty) {
          Object.defineProperty(cooked, "raw", {
            value: raw
          });
        } else {
          cooked.raw = raw;
        }

        return cooked;
      }

      ;

      function __importStar(mod) {
        if (mod && mod.__esModule) return mod;
        var result = {};
        if (mod != null) for (var k in mod) {
          if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
        }
        result["default"] = mod;
        return result;
      }

      function __importDefault(mod) {
        return mod && mod.__esModule ? mod : {
          "default": mod
        };
      }

      function __classPrivateFieldGet(receiver, privateMap) {
        if (!privateMap.has(receiver)) {
          throw new TypeError("attempted to get private field on non-instance");
        }

        return privateMap.get(receiver);
      }

      function __classPrivateFieldSet(receiver, privateMap, value) {
        if (!privateMap.has(receiver)) {
          throw new TypeError("attempted to set private field on non-instance");
        }

        privateMap.set(receiver, value);
        return value;
      }
      /***/

    },

    /***/
    "e8PA":
    /*!*********************************************************************************!*\
      !*** ./src/app/pages/auth/auth-reset-password/auth-reset-password.component.ts ***!
      \*********************************************************************************/

    /*! exports provided: AuthResetPasswordComponent */

    /***/
    function e8PA(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AuthResetPasswordComponent", function () {
        return AuthResetPasswordComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var AuthResetPasswordComponent = /*#__PURE__*/function () {
        function AuthResetPasswordComponent() {
          _classCallCheck(this, AuthResetPasswordComponent);
        }

        _createClass(AuthResetPasswordComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }]);

        return AuthResetPasswordComponent;
      }();

      AuthResetPasswordComponent.ɵfac = function AuthResetPasswordComponent_Factory(t) {
        return new (t || AuthResetPasswordComponent)();
      };

      AuthResetPasswordComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: AuthResetPasswordComponent,
        selectors: [["app-auth-reset-password"]],
        decls: 13,
        vars: 0,
        consts: [[1, "auth-wrapper"], [1, "auth-content"], [1, "card"], [1, "row", "align-items-center", "text-center"], [1, "col-md-12"], [1, "card-body"], ["src", "assets/images/logo-dark.png", "alt", "", 1, "img-fluid", "mb-4"], [1, "mb-3", "f-w-400"], [1, "input-group", "mb-4"], ["type", "email", "placeholder", "Email address", 1, "form-control"], [1, "btn", "btn-block", "btn-primary", "mb-4"]],
        template: function AuthResetPasswordComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "img", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "h4", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Reset your password");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "input", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "button", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Reset password");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }
        },
        styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhdXRoLXJlc2V0LXBhc3N3b3JkLmNvbXBvbmVudC5zY3NzIn0= */"]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AuthResetPasswordComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'app-auth-reset-password',
            templateUrl: './auth-reset-password.component.html',
            styleUrls: ['./auth-reset-password.component.scss']
          }]
        }], function () {
          return [];
        }, null);
      })();
      /***/

    },

    /***/
    "h5nQ":
    /*!********************************************************************!*\
      !*** ./src/app/pages/auth/company-signup/company-signup.module.ts ***!
      \********************************************************************/

    /*! exports provided: CompanySignupModule */

    /***/
    function h5nQ(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CompanySignupModule", function () {
        return CompanySignupModule;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _company_signup_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./company-signup-routing.module */
      "9vPV");
      /* harmony import */


      var _company_signup_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./company-signup.component */
      "S3YR");
      /* harmony import */


      var _theme_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../../../theme/shared/shared.module */
      "ebz3");
      /* harmony import */


      var angular_archwizard__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! angular-archwizard */
      "mkVx");
      /* harmony import */


      var ng_select__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ng-select */
      "lTK2");

      var CompanySignupModule = function CompanySignupModule() {
        _classCallCheck(this, CompanySignupModule);
      };

      CompanySignupModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
        type: CompanySignupModule
      });
      CompanySignupModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
        factory: function CompanySignupModule_Factory(t) {
          return new (t || CompanySignupModule)();
        },
        imports: [[_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _company_signup_routing_module__WEBPACK_IMPORTED_MODULE_2__["CompanySignupRoutingModule"], _theme_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"], angular_archwizard__WEBPACK_IMPORTED_MODULE_5__["ArchwizardModule"], ng_select__WEBPACK_IMPORTED_MODULE_6__["SelectModule"]]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](CompanySignupModule, {
          declarations: [_company_signup_component__WEBPACK_IMPORTED_MODULE_3__["CompanySignupComponent"]],
          imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _company_signup_routing_module__WEBPACK_IMPORTED_MODULE_2__["CompanySignupRoutingModule"], _theme_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"], angular_archwizard__WEBPACK_IMPORTED_MODULE_5__["ArchwizardModule"], ng_select__WEBPACK_IMPORTED_MODULE_6__["SelectModule"]]
        });
      })();
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](CompanySignupModule, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
          args: [{
            declarations: [_company_signup_component__WEBPACK_IMPORTED_MODULE_3__["CompanySignupComponent"]],
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _company_signup_routing_module__WEBPACK_IMPORTED_MODULE_2__["CompanySignupRoutingModule"], _theme_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"], angular_archwizard__WEBPACK_IMPORTED_MODULE_5__["ArchwizardModule"], ng_select__WEBPACK_IMPORTED_MODULE_6__["SelectModule"]]
          }]
        }], null, null);
      })();
      /***/

    },

    /***/
    "lBUW":
    /*!*******************************************!*\
      !*** ./src/app/pages/auth/auth.module.ts ***!
      \*******************************************/

    /*! exports provided: AuthModule */

    /***/
    function lBUW(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AuthModule", function () {
        return AuthModule;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _auth_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./auth-routing.module */
      "BUwF");
      /* harmony import */


      var _auth_signin_auth_signin_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./auth-signin/auth-signin.module */
      "Mk9t");
      /* harmony import */


      var _auth_investor_signup_auth_investor_signup_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ./auth-investor-signup/auth-investor-signup.module */
      "UItr");
      /* harmony import */


      var _auth_reset_password_auth_reset_password_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./auth-reset-password/auth-reset-password.module */
      "B0WW");
      /* harmony import */


      var _company_signup_company_signup_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./company-signup/company-signup.module */
      "h5nQ");

      var AuthModule = function AuthModule() {
        _classCallCheck(this, AuthModule);
      };

      AuthModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
        type: AuthModule
      });
      AuthModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
        factory: function AuthModule_Factory(t) {
          return new (t || AuthModule)();
        },
        imports: [[_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _auth_routing_module__WEBPACK_IMPORTED_MODULE_2__["AuthRoutingModule"], _auth_signin_auth_signin_module__WEBPACK_IMPORTED_MODULE_3__["AuthSigninModule"], _auth_investor_signup_auth_investor_signup_module__WEBPACK_IMPORTED_MODULE_4__["AuthInvestorSignupModule"], _auth_reset_password_auth_reset_password_module__WEBPACK_IMPORTED_MODULE_5__["AuthResetPasswordModule"], _company_signup_company_signup_module__WEBPACK_IMPORTED_MODULE_6__["CompanySignupModule"]]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](AuthModule, {
          imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _auth_routing_module__WEBPACK_IMPORTED_MODULE_2__["AuthRoutingModule"], _auth_signin_auth_signin_module__WEBPACK_IMPORTED_MODULE_3__["AuthSigninModule"], _auth_investor_signup_auth_investor_signup_module__WEBPACK_IMPORTED_MODULE_4__["AuthInvestorSignupModule"], _auth_reset_password_auth_reset_password_module__WEBPACK_IMPORTED_MODULE_5__["AuthResetPasswordModule"], _company_signup_company_signup_module__WEBPACK_IMPORTED_MODULE_6__["CompanySignupModule"]]
        });
      })();
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AuthModule, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
          args: [{
            declarations: [],
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _auth_routing_module__WEBPACK_IMPORTED_MODULE_2__["AuthRoutingModule"], _auth_signin_auth_signin_module__WEBPACK_IMPORTED_MODULE_3__["AuthSigninModule"], _auth_investor_signup_auth_investor_signup_module__WEBPACK_IMPORTED_MODULE_4__["AuthInvestorSignupModule"], _auth_reset_password_auth_reset_password_module__WEBPACK_IMPORTED_MODULE_5__["AuthResetPasswordModule"], _company_signup_company_signup_module__WEBPACK_IMPORTED_MODULE_6__["CompanySignupModule"]]
          }]
        }], null, null);
      })();
      /***/

    },

    /***/
    "lTK2":
    /*!*******************************************************************!*\
      !*** ./node_modules/ng-select/__ivy_ngcc__/fesm2015/ng-select.js ***!
      \*******************************************************************/

    /*! exports provided: SELECT_VALUE_ACCESSOR, SelectComponent, SelectModule, ɵa */

    /***/
    function lTK2(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SELECT_VALUE_ACCESSOR", function () {
        return SELECT_VALUE_ACCESSOR;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SelectComponent", function () {
        return SelectComponent;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SelectModule", function () {
        return SelectModule;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ɵa", function () {
        return SelectDropdownComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/forms */
      "3Pt+");
      /**
       * @fileoverview added by tsickle
       * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
       */


      var _c0 = ["filterInput"];
      var _c1 = ["optionsList"];

      function SelectDropdownComponent_div_1_Template(rf, ctx) {
        if (rf & 1) {
          var _r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "input", 8, 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SelectDropdownComponent_div_1_Template_input_click_1_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r6);

            var ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r5.onSingleFilterClick();
          })("input", function SelectDropdownComponent_div_1_Template_input_input_1_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r6);

            var ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r7.onSingleFilterInput($event);
          })("keydown", function SelectDropdownComponent_div_1_Template_input_keydown_1_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r6);

            var ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r8.onSingleFilterKeydown($event);
          })("focus", function SelectDropdownComponent_div_1_Template_input_focus_1_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r6);

            var ctx_r9 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r9.onSingleFilterFocus();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("placeholder", ctx_r0.placeholder);
        }
      }

      function SelectDropdownComponent_li_5_ng_container_1_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainer"](0);
        }
      }

      function SelectDropdownComponent_li_5_span_2_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "span");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var option_r10 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](option_r10.label);
        }
      }

      var _c2 = function _c2(a0, a1, a2) {
        return {
          "highlighted": a0,
          "selected": a1,
          "disabled": a2
        };
      };

      var _c3 = function _c3(a0) {
        return {
          option: a0
        };
      };

      function SelectDropdownComponent_li_5_Template(rf, ctx) {
        if (rf & 1) {
          var _r15 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "li", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SelectDropdownComponent_li_5_Template_li_click_0_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r15);

            var option_r10 = ctx.$implicit;

            var ctx_r14 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r14.onOptionClick(option_r10);
          })("mouseover", function SelectDropdownComponent_li_5_Template_li_mouseover_0_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r15);

            var option_r10 = ctx.$implicit;

            var ctx_r16 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r16.onOptionMouseover(option_r10);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, SelectDropdownComponent_li_5_ng_container_1_Template, 1, 0, "ng-container", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, SelectDropdownComponent_li_5_span_2_Template, 2, 1, "span", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var option_r10 = ctx.$implicit;

          var ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction3"](5, _c2, option_r10.highlighted, option_r10.selected, option_r10.disabled))("ngStyle", ctx_r2.getOptionStyle(option_r10));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngTemplateOutlet", ctx_r2.optionTemplate)("ngTemplateOutletContext", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](9, _c3, option_r10.wrappedOption));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx_r2.optionTemplate);
        }
      }

      function SelectDropdownComponent_li_6_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "li", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx_r3.notFoundMsg, " ");
        }
      }

      var _c4 = function _c4(a0, a1) {
        return {
          "below": a0,
          "above": a1
        };
      };

      var _c5 = function _c5(a0, a1, a2) {
        return {
          "top.px": a0,
          "left.px": a1,
          "width.px": a2
        };
      };

      var _c6 = ["optionTemplate"];
      var _c7 = ["selection"];
      var _c8 = ["dropdown"];

      function SelectComponent_label_0_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "label");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx_r0.label, "\n");
        }
      }

      function SelectComponent_div_3_div_1_ng_container_1_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainer"](0);
        }
      }

      function SelectComponent_div_3_div_1_span_2_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "span");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx_r11.optionList.selection[0].label);
        }
      }

      var _c9 = function _c9(a0, a1) {
        return {
          option: a0,
          onDeselectOptionClick: a1
        };
      };

      function SelectComponent_div_3_div_1_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, SelectComponent_div_3_div_1_ng_container_1_Template, 1, 0, "ng-container", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, SelectComponent_div_3_div_1_span_2_Template, 2, 1, "span", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngTemplateOutlet", ctx_r5.optionTemplate)("ngTemplateOutletContext", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction2"](3, _c9, ctx_r5.optionList.selection[0].wrappedOption, ctx_r5.onDeselectOptionClick));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx_r5.optionTemplate);
        }
      }

      function SelectComponent_div_3_div_2_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx_r6.placeholderView, " ");
        }
      }

      function SelectComponent_div_3_div_3_Template(rf, ctx) {
        if (rf & 1) {
          var _r13 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SelectComponent_div_3_div_3_Template_div_click_0_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r13);

            var ctx_r12 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

            return ctx_r12.onClearSelectionClick($event);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " \u2715 ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function SelectComponent_div_3_div_4_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " \u25B2 ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function SelectComponent_div_3_div_5_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " \u25BC ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      }

      function SelectComponent_div_3_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, SelectComponent_div_3_div_1_Template, 3, 6, "div", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, SelectComponent_div_3_div_2_Template, 2, 1, "div", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, SelectComponent_div_3_div_3_Template, 2, 0, "div", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, SelectComponent_div_3_div_4_Template, 2, 0, "div", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](5, SelectComponent_div_3_div_5_Template, 2, 0, "div", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r2.optionList.hasSelected);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx_r2.optionList.hasSelected);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r2.allowClear && ctx_r2.optionList.hasSelected);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r2.isOpen);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx_r2.isOpen);
        }
      }

      function SelectComponent_div_4_div_1_Template(rf, ctx) {
        if (rf & 1) {
          var _r19 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 19);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "span", 20);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SelectComponent_div_4_div_1_Template_span_click_1_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r19);

            var option_r17 = ctx.$implicit;

            var ctx_r18 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

            return ctx_r18.onDeselectOptionClick(option_r17);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, " \u2715 ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var option_r17 = ctx.$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", option_r17.label, " ");
        }
      }

      function SelectComponent_div_4_div_2_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r15 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx_r15.placeholderView, " ");
        }
      }

      var _c10 = function _c10(a0) {
        return {
          "width.px": a0
        };
      };

      function SelectComponent_div_4_input_3_Template(rf, ctx) {
        if (rf & 1) {
          var _r22 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "input", 21, 22);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("input", function SelectComponent_div_4_input_3_Template_input_input_0_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r22);

            var ctx_r21 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

            return ctx_r21.onFilterInput($event.target.value);
          })("keydown", function SelectComponent_div_4_input_3_Template_input_keydown_0_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r22);

            var ctx_r23 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

            return ctx_r23.onMultipleFilterKeydown($event);
          })("focus", function SelectComponent_div_4_input_3_Template_input_focus_0_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r22);

            var ctx_r24 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

            return ctx_r24.onMultipleFilterFocus();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r16 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("placeholder", ctx_r16.placeholderView)("ngStyle", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](2, _c10, ctx_r16.filterInputWidth));
        }
      }

      function SelectComponent_div_4_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 16);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, SelectComponent_div_4_div_1_Template, 4, 1, "div", 17);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, SelectComponent_div_4_div_2_Template, 2, 1, "div", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, SelectComponent_div_4_input_3_Template, 2, 4, "input", 18);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r3.optionList.selection);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx_r3.filterEnabled && !ctx_r3.optionList.hasSelected);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r3.filterEnabled);
        }
      }

      function SelectComponent_select_dropdown_5_Template(rf, ctx) {
        if (rf & 1) {
          var _r27 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "select-dropdown", 23, 24);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("optionClicked", function SelectComponent_select_dropdown_5_Template_select_dropdown_optionClicked_0_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r27);

            var ctx_r26 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r26.onDropdownOptionClicked($event);
          })("optionsListClick", function SelectComponent_select_dropdown_5_Template_select_dropdown_optionsListClick_0_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r27);

            var ctx_r28 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r28.onOptionsListClick();
          })("singleFilterClick", function SelectComponent_select_dropdown_5_Template_select_dropdown_singleFilterClick_0_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r27);

            var ctx_r29 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r29.onSingleFilterClick();
          })("singleFilterFocus", function SelectComponent_select_dropdown_5_Template_select_dropdown_singleFilterFocus_0_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r27);

            var ctx_r30 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r30.onSingleFilterFocus();
          })("singleFilterInput", function SelectComponent_select_dropdown_5_Template_select_dropdown_singleFilterInput_0_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r27);

            var ctx_r31 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r31.onFilterInput($event);
          })("singleFilterKeydown", function SelectComponent_select_dropdown_5_Template_select_dropdown_singleFilterKeydown_0_listener($event) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r27);

            var ctx_r32 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

            return ctx_r32.onSingleFilterKeydown($event);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("multiple", ctx_r4.multiple)("optionList", ctx_r4.optionList)("notFoundMsg", ctx_r4.notFoundMsg)("highlightColor", ctx_r4.highlightColor)("highlightTextColor", ctx_r4.highlightTextColor)("filterEnabled", ctx_r4.filterEnabled)("placeholder", ctx_r4.filterPlaceholder)("isBelow", ctx_r4.isBelow)("width", ctx_r4.width)("top", ctx_r4.top)("left", ctx_r4.left)("optionTemplate", ctx_r4.optionTemplate);
        }
      }

      var _c11 = function _c11(a0, a1, a2, a3, a4) {
        return {
          "open": a0,
          "focus": a1,
          "below": a2,
          "above": a3,
          "disabled": a4
        };
      };

      var Option = /*#__PURE__*/function () {
        /**
         * @param {?} option
         */
        function Option(option) {
          _classCallCheck(this, Option);

          this.wrappedOption = option;
          this.disabled = false;
          this.highlighted = false;
          this.selected = false;
          this.shown = true;
        }
        /**
         * @return {?}
         */


        _createClass(Option, [{
          key: "value",
          get: function get() {
            return this.wrappedOption.value;
          }
          /**
           * @return {?}
           */

        }, {
          key: "label",
          get: function get() {
            return this.wrappedOption.label;
          }
        }]);

        return Option;
      }();
      /**
       * @fileoverview added by tsickle
       * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
       */


      var Diacritics = /*#__PURE__*/function () {
        function Diacritics() {
          _classCallCheck(this, Diacritics);
        }

        _createClass(Diacritics, null, [{
          key: "strip",

          /**
           * @param {?} text
           * @return {?}
           */
          value: function strip(text) {
            var _this4 = this;

            /** @type {?} */
            var match =
            /**
            * @param {?} a
            * @return {?}
            */
            function match(a) {
              return _this4.DIACRITICS[a] || a;
            };

            return text.replace(/[^\u0000-\u007E]/g, match);
          }
        }]);

        return Diacritics;
      }();

      Diacritics.DIACRITICS = {
        "\u24B6": 'A',
        "\uFF21": 'A',
        "\xC0": 'A',
        "\xC1": 'A',
        "\xC2": 'A',
        "\u1EA6": 'A',
        "\u1EA4": 'A',
        "\u1EAA": 'A',
        "\u1EA8": 'A',
        "\xC3": 'A',
        "\u0100": 'A',
        "\u0102": 'A',
        "\u1EB0": 'A',
        "\u1EAE": 'A',
        "\u1EB4": 'A',
        "\u1EB2": 'A',
        "\u0226": 'A',
        "\u01E0": 'A',
        "\xC4": 'A',
        "\u01DE": 'A',
        "\u1EA2": 'A',
        "\xC5": 'A',
        "\u01FA": 'A',
        "\u01CD": 'A',
        "\u0200": 'A',
        "\u0202": 'A',
        "\u1EA0": 'A',
        "\u1EAC": 'A',
        "\u1EB6": 'A',
        "\u1E00": 'A',
        "\u0104": 'A',
        "\u023A": 'A',
        "\u2C6F": 'A',
        "\uA732": 'AA',
        "\xC6": 'AE',
        "\u01FC": 'AE',
        "\u01E2": 'AE',
        "\uA734": 'AO',
        "\uA736": 'AU',
        "\uA738": 'AV',
        "\uA73A": 'AV',
        "\uA73C": 'AY',
        "\u24B7": 'B',
        "\uFF22": 'B',
        "\u1E02": 'B',
        "\u1E04": 'B',
        "\u1E06": 'B',
        "\u0243": 'B',
        "\u0182": 'B',
        "\u0181": 'B',
        "\u24B8": 'C',
        "\uFF23": 'C',
        "\u0106": 'C',
        "\u0108": 'C',
        "\u010A": 'C',
        "\u010C": 'C',
        "\xC7": 'C',
        "\u1E08": 'C',
        "\u0187": 'C',
        "\u023B": 'C',
        "\uA73E": 'C',
        "\u24B9": 'D',
        "\uFF24": 'D',
        "\u1E0A": 'D',
        "\u010E": 'D',
        "\u1E0C": 'D',
        "\u1E10": 'D',
        "\u1E12": 'D',
        "\u1E0E": 'D',
        "\u0110": 'D',
        "\u018B": 'D',
        "\u018A": 'D',
        "\u0189": 'D',
        "\uA779": 'D',
        "\u01F1": 'DZ',
        "\u01C4": 'DZ',
        "\u01F2": 'Dz',
        "\u01C5": 'Dz',
        "\u24BA": 'E',
        "\uFF25": 'E',
        "\xC8": 'E',
        "\xC9": 'E',
        "\xCA": 'E',
        "\u1EC0": 'E',
        "\u1EBE": 'E',
        "\u1EC4": 'E',
        "\u1EC2": 'E',
        "\u1EBC": 'E',
        "\u0112": 'E',
        "\u1E14": 'E',
        "\u1E16": 'E',
        "\u0114": 'E',
        "\u0116": 'E',
        "\xCB": 'E',
        "\u1EBA": 'E',
        "\u011A": 'E',
        "\u0204": 'E',
        "\u0206": 'E',
        "\u1EB8": 'E',
        "\u1EC6": 'E',
        "\u0228": 'E',
        "\u1E1C": 'E',
        "\u0118": 'E',
        "\u1E18": 'E',
        "\u1E1A": 'E',
        "\u0190": 'E',
        "\u018E": 'E',
        "\u24BB": 'F',
        "\uFF26": 'F',
        "\u1E1E": 'F',
        "\u0191": 'F',
        "\uA77B": 'F',
        "\u24BC": 'G',
        "\uFF27": 'G',
        "\u01F4": 'G',
        "\u011C": 'G',
        "\u1E20": 'G',
        "\u011E": 'G',
        "\u0120": 'G',
        "\u01E6": 'G',
        "\u0122": 'G',
        "\u01E4": 'G',
        "\u0193": 'G',
        "\uA7A0": 'G',
        "\uA77D": 'G',
        "\uA77E": 'G',
        "\u24BD": 'H',
        "\uFF28": 'H',
        "\u0124": 'H',
        "\u1E22": 'H',
        "\u1E26": 'H',
        "\u021E": 'H',
        "\u1E24": 'H',
        "\u1E28": 'H',
        "\u1E2A": 'H',
        "\u0126": 'H',
        "\u2C67": 'H',
        "\u2C75": 'H',
        "\uA78D": 'H',
        "\u24BE": 'I',
        "\uFF29": 'I',
        "\xCC": 'I',
        "\xCD": 'I',
        "\xCE": 'I',
        "\u0128": 'I',
        "\u012A": 'I',
        "\u012C": 'I',
        "\u0130": 'I',
        "\xCF": 'I',
        "\u1E2E": 'I',
        "\u1EC8": 'I',
        "\u01CF": 'I',
        "\u0208": 'I',
        "\u020A": 'I',
        "\u1ECA": 'I',
        "\u012E": 'I',
        "\u1E2C": 'I',
        "\u0197": 'I',
        "\u24BF": 'J',
        "\uFF2A": 'J',
        "\u0134": 'J',
        "\u0248": 'J',
        "\u24C0": 'K',
        "\uFF2B": 'K',
        "\u1E30": 'K',
        "\u01E8": 'K',
        "\u1E32": 'K',
        "\u0136": 'K',
        "\u1E34": 'K',
        "\u0198": 'K',
        "\u2C69": 'K',
        "\uA740": 'K',
        "\uA742": 'K',
        "\uA744": 'K',
        "\uA7A2": 'K',
        "\u24C1": 'L',
        "\uFF2C": 'L',
        "\u013F": 'L',
        "\u0139": 'L',
        "\u013D": 'L',
        "\u1E36": 'L',
        "\u1E38": 'L',
        "\u013B": 'L',
        "\u1E3C": 'L',
        "\u1E3A": 'L',
        "\u0141": 'L',
        "\u023D": 'L',
        "\u2C62": 'L',
        "\u2C60": 'L',
        "\uA748": 'L',
        "\uA746": 'L',
        "\uA780": 'L',
        "\u01C7": 'LJ',
        "\u01C8": 'Lj',
        "\u24C2": 'M',
        "\uFF2D": 'M',
        "\u1E3E": 'M',
        "\u1E40": 'M',
        "\u1E42": 'M',
        "\u2C6E": 'M',
        "\u019C": 'M',
        "\u24C3": 'N',
        "\uFF2E": 'N',
        "\u01F8": 'N',
        "\u0143": 'N',
        "\xD1": 'N',
        "\u1E44": 'N',
        "\u0147": 'N',
        "\u1E46": 'N',
        "\u0145": 'N',
        "\u1E4A": 'N',
        "\u1E48": 'N',
        "\u0220": 'N',
        "\u019D": 'N',
        "\uA790": 'N',
        "\uA7A4": 'N',
        "\u01CA": 'NJ',
        "\u01CB": 'Nj',
        "\u24C4": 'O',
        "\uFF2F": 'O',
        "\xD2": 'O',
        "\xD3": 'O',
        "\xD4": 'O',
        "\u1ED2": 'O',
        "\u1ED0": 'O',
        "\u1ED6": 'O',
        "\u1ED4": 'O',
        "\xD5": 'O',
        "\u1E4C": 'O',
        "\u022C": 'O',
        "\u1E4E": 'O',
        "\u014C": 'O',
        "\u1E50": 'O',
        "\u1E52": 'O',
        "\u014E": 'O',
        "\u022E": 'O',
        "\u0230": 'O',
        "\xD6": 'O',
        "\u022A": 'O',
        "\u1ECE": 'O',
        "\u0150": 'O',
        "\u01D1": 'O',
        "\u020C": 'O',
        "\u020E": 'O',
        "\u01A0": 'O',
        "\u1EDC": 'O',
        "\u1EDA": 'O',
        "\u1EE0": 'O',
        "\u1EDE": 'O',
        "\u1EE2": 'O',
        "\u1ECC": 'O',
        "\u1ED8": 'O',
        "\u01EA": 'O',
        "\u01EC": 'O',
        "\xD8": 'O',
        "\u01FE": 'O',
        "\u0186": 'O',
        "\u019F": 'O',
        "\uA74A": 'O',
        "\uA74C": 'O',
        "\u01A2": 'OI',
        "\uA74E": 'OO',
        "\u0222": 'OU',
        "\u24C5": 'P',
        "\uFF30": 'P',
        "\u1E54": 'P',
        "\u1E56": 'P',
        "\u01A4": 'P',
        "\u2C63": 'P',
        "\uA750": 'P',
        "\uA752": 'P',
        "\uA754": 'P',
        "\u24C6": 'Q',
        "\uFF31": 'Q',
        "\uA756": 'Q',
        "\uA758": 'Q',
        "\u024A": 'Q',
        "\u24C7": 'R',
        "\uFF32": 'R',
        "\u0154": 'R',
        "\u1E58": 'R',
        "\u0158": 'R',
        "\u0210": 'R',
        "\u0212": 'R',
        "\u1E5A": 'R',
        "\u1E5C": 'R',
        "\u0156": 'R',
        "\u1E5E": 'R',
        "\u024C": 'R',
        "\u2C64": 'R',
        "\uA75A": 'R',
        "\uA7A6": 'R',
        "\uA782": 'R',
        "\u24C8": 'S',
        "\uFF33": 'S',
        "\u1E9E": 'S',
        "\u015A": 'S',
        "\u1E64": 'S',
        "\u015C": 'S',
        "\u1E60": 'S',
        "\u0160": 'S',
        "\u1E66": 'S',
        "\u1E62": 'S',
        "\u1E68": 'S',
        "\u0218": 'S',
        "\u015E": 'S',
        "\u2C7E": 'S',
        "\uA7A8": 'S',
        "\uA784": 'S',
        "\u24C9": 'T',
        "\uFF34": 'T',
        "\u1E6A": 'T',
        "\u0164": 'T',
        "\u1E6C": 'T',
        "\u021A": 'T',
        "\u0162": 'T',
        "\u1E70": 'T',
        "\u1E6E": 'T',
        "\u0166": 'T',
        "\u01AC": 'T',
        "\u01AE": 'T',
        "\u023E": 'T',
        "\uA786": 'T',
        "\uA728": 'TZ',
        "\u24CA": 'U',
        "\uFF35": 'U',
        "\xD9": 'U',
        "\xDA": 'U',
        "\xDB": 'U',
        "\u0168": 'U',
        "\u1E78": 'U',
        "\u016A": 'U',
        "\u1E7A": 'U',
        "\u016C": 'U',
        "\xDC": 'U',
        "\u01DB": 'U',
        "\u01D7": 'U',
        "\u01D5": 'U',
        "\u01D9": 'U',
        "\u1EE6": 'U',
        "\u016E": 'U',
        "\u0170": 'U',
        "\u01D3": 'U',
        "\u0214": 'U',
        "\u0216": 'U',
        "\u01AF": 'U',
        "\u1EEA": 'U',
        "\u1EE8": 'U',
        "\u1EEE": 'U',
        "\u1EEC": 'U',
        "\u1EF0": 'U',
        "\u1EE4": 'U',
        "\u1E72": 'U',
        "\u0172": 'U',
        "\u1E76": 'U',
        "\u1E74": 'U',
        "\u0244": 'U',
        "\u24CB": 'V',
        "\uFF36": 'V',
        "\u1E7C": 'V',
        "\u1E7E": 'V',
        "\u01B2": 'V',
        "\uA75E": 'V',
        "\u0245": 'V',
        "\uA760": 'VY',
        "\u24CC": 'W',
        "\uFF37": 'W',
        "\u1E80": 'W',
        "\u1E82": 'W',
        "\u0174": 'W',
        "\u1E86": 'W',
        "\u1E84": 'W',
        "\u1E88": 'W',
        "\u2C72": 'W',
        "\u24CD": 'X',
        "\uFF38": 'X',
        "\u1E8A": 'X',
        "\u1E8C": 'X',
        "\u24CE": 'Y',
        "\uFF39": 'Y',
        "\u1EF2": 'Y',
        "\xDD": 'Y',
        "\u0176": 'Y',
        "\u1EF8": 'Y',
        "\u0232": 'Y',
        "\u1E8E": 'Y',
        "\u0178": 'Y',
        "\u1EF6": 'Y',
        "\u1EF4": 'Y',
        "\u01B3": 'Y',
        "\u024E": 'Y',
        "\u1EFE": 'Y',
        "\u24CF": 'Z',
        "\uFF3A": 'Z',
        "\u0179": 'Z',
        "\u1E90": 'Z',
        "\u017B": 'Z',
        "\u017D": 'Z',
        "\u1E92": 'Z',
        "\u1E94": 'Z',
        "\u01B5": 'Z',
        "\u0224": 'Z',
        "\u2C7F": 'Z',
        "\u2C6B": 'Z',
        "\uA762": 'Z',
        "\u24D0": 'a',
        "\uFF41": 'a',
        "\u1E9A": 'a',
        "\xE0": 'a',
        "\xE1": 'a',
        "\xE2": 'a',
        "\u1EA7": 'a',
        "\u1EA5": 'a',
        "\u1EAB": 'a',
        "\u1EA9": 'a',
        "\xE3": 'a',
        "\u0101": 'a',
        "\u0103": 'a',
        "\u1EB1": 'a',
        "\u1EAF": 'a',
        "\u1EB5": 'a',
        "\u1EB3": 'a',
        "\u0227": 'a',
        "\u01E1": 'a',
        "\xE4": 'a',
        "\u01DF": 'a',
        "\u1EA3": 'a',
        "\xE5": 'a',
        "\u01FB": 'a',
        "\u01CE": 'a',
        "\u0201": 'a',
        "\u0203": 'a',
        "\u1EA1": 'a',
        "\u1EAD": 'a',
        "\u1EB7": 'a',
        "\u1E01": 'a',
        "\u0105": 'a',
        "\u2C65": 'a',
        "\u0250": 'a',
        "\uA733": 'aa',
        "\xE6": 'ae',
        "\u01FD": 'ae',
        "\u01E3": 'ae',
        "\uA735": 'ao',
        "\uA737": 'au',
        "\uA739": 'av',
        "\uA73B": 'av',
        "\uA73D": 'ay',
        "\u24D1": 'b',
        "\uFF42": 'b',
        "\u1E03": 'b',
        "\u1E05": 'b',
        "\u1E07": 'b',
        "\u0180": 'b',
        "\u0183": 'b',
        "\u0253": 'b',
        "\u24D2": 'c',
        "\uFF43": 'c',
        "\u0107": 'c',
        "\u0109": 'c',
        "\u010B": 'c',
        "\u010D": 'c',
        "\xE7": 'c',
        "\u1E09": 'c',
        "\u0188": 'c',
        "\u023C": 'c',
        "\uA73F": 'c',
        "\u2184": 'c',
        "\u24D3": 'd',
        "\uFF44": 'd',
        "\u1E0B": 'd',
        "\u010F": 'd',
        "\u1E0D": 'd',
        "\u1E11": 'd',
        "\u1E13": 'd',
        "\u1E0F": 'd',
        "\u0111": 'd',
        "\u018C": 'd',
        "\u0256": 'd',
        "\u0257": 'd',
        "\uA77A": 'd',
        "\u01F3": 'dz',
        "\u01C6": 'dz',
        "\u24D4": 'e',
        "\uFF45": 'e',
        "\xE8": 'e',
        "\xE9": 'e',
        "\xEA": 'e',
        "\u1EC1": 'e',
        "\u1EBF": 'e',
        "\u1EC5": 'e',
        "\u1EC3": 'e',
        "\u1EBD": 'e',
        "\u0113": 'e',
        "\u1E15": 'e',
        "\u1E17": 'e',
        "\u0115": 'e',
        "\u0117": 'e',
        "\xEB": 'e',
        "\u1EBB": 'e',
        "\u011B": 'e',
        "\u0205": 'e',
        "\u0207": 'e',
        "\u1EB9": 'e',
        "\u1EC7": 'e',
        "\u0229": 'e',
        "\u1E1D": 'e',
        "\u0119": 'e',
        "\u1E19": 'e',
        "\u1E1B": 'e',
        "\u0247": 'e',
        "\u025B": 'e',
        "\u01DD": 'e',
        "\u24D5": 'f',
        "\uFF46": 'f',
        "\u1E1F": 'f',
        "\u0192": 'f',
        "\uA77C": 'f',
        "\u24D6": 'g',
        "\uFF47": 'g',
        "\u01F5": 'g',
        "\u011D": 'g',
        "\u1E21": 'g',
        "\u011F": 'g',
        "\u0121": 'g',
        "\u01E7": 'g',
        "\u0123": 'g',
        "\u01E5": 'g',
        "\u0260": 'g',
        "\uA7A1": 'g',
        "\u1D79": 'g',
        "\uA77F": 'g',
        "\u24D7": 'h',
        "\uFF48": 'h',
        "\u0125": 'h',
        "\u1E23": 'h',
        "\u1E27": 'h',
        "\u021F": 'h',
        "\u1E25": 'h',
        "\u1E29": 'h',
        "\u1E2B": 'h',
        "\u1E96": 'h',
        "\u0127": 'h',
        "\u2C68": 'h',
        "\u2C76": 'h',
        "\u0265": 'h',
        "\u0195": 'hv',
        "\u24D8": 'i',
        "\uFF49": 'i',
        "\xEC": 'i',
        "\xED": 'i',
        "\xEE": 'i',
        "\u0129": 'i',
        "\u012B": 'i',
        "\u012D": 'i',
        "\xEF": 'i',
        "\u1E2F": 'i',
        "\u1EC9": 'i',
        "\u01D0": 'i',
        "\u0209": 'i',
        "\u020B": 'i',
        "\u1ECB": 'i',
        "\u012F": 'i',
        "\u1E2D": 'i',
        "\u0268": 'i',
        "\u0131": 'i',
        "\u24D9": 'j',
        "\uFF4A": 'j',
        "\u0135": 'j',
        "\u01F0": 'j',
        "\u0249": 'j',
        "\u24DA": 'k',
        "\uFF4B": 'k',
        "\u1E31": 'k',
        "\u01E9": 'k',
        "\u1E33": 'k',
        "\u0137": 'k',
        "\u1E35": 'k',
        "\u0199": 'k',
        "\u2C6A": 'k',
        "\uA741": 'k',
        "\uA743": 'k',
        "\uA745": 'k',
        "\uA7A3": 'k',
        "\u24DB": 'l',
        "\uFF4C": 'l',
        "\u0140": 'l',
        "\u013A": 'l',
        "\u013E": 'l',
        "\u1E37": 'l',
        "\u1E39": 'l',
        "\u013C": 'l',
        "\u1E3D": 'l',
        "\u1E3B": 'l',
        "\u017F": 'l',
        "\u0142": 'l',
        "\u019A": 'l',
        "\u026B": 'l',
        "\u2C61": 'l',
        "\uA749": 'l',
        "\uA781": 'l',
        "\uA747": 'l',
        "\u01C9": 'lj',
        "\u24DC": 'm',
        "\uFF4D": 'm',
        "\u1E3F": 'm',
        "\u1E41": 'm',
        "\u1E43": 'm',
        "\u0271": 'm',
        "\u026F": 'm',
        "\u24DD": 'n',
        "\uFF4E": 'n',
        "\u01F9": 'n',
        "\u0144": 'n',
        "\xF1": 'n',
        "\u1E45": 'n',
        "\u0148": 'n',
        "\u1E47": 'n',
        "\u0146": 'n',
        "\u1E4B": 'n',
        "\u1E49": 'n',
        "\u019E": 'n',
        "\u0272": 'n',
        "\u0149": 'n',
        "\uA791": 'n',
        "\uA7A5": 'n',
        "\u01CC": 'nj',
        "\u24DE": 'o',
        "\uFF4F": 'o',
        "\xF2": 'o',
        "\xF3": 'o',
        "\xF4": 'o',
        "\u1ED3": 'o',
        "\u1ED1": 'o',
        "\u1ED7": 'o',
        "\u1ED5": 'o',
        "\xF5": 'o',
        "\u1E4D": 'o',
        "\u022D": 'o',
        "\u1E4F": 'o',
        "\u014D": 'o',
        "\u1E51": 'o',
        "\u1E53": 'o',
        "\u014F": 'o',
        "\u022F": 'o',
        "\u0231": 'o',
        "\xF6": 'o',
        "\u022B": 'o',
        "\u1ECF": 'o',
        "\u0151": 'o',
        "\u01D2": 'o',
        "\u020D": 'o',
        "\u020F": 'o',
        "\u01A1": 'o',
        "\u1EDD": 'o',
        "\u1EDB": 'o',
        "\u1EE1": 'o',
        "\u1EDF": 'o',
        "\u1EE3": 'o',
        "\u1ECD": 'o',
        "\u1ED9": 'o',
        "\u01EB": 'o',
        "\u01ED": 'o',
        "\xF8": 'o',
        "\u01FF": 'o',
        "\u0254": 'o',
        "\uA74B": 'o',
        "\uA74D": 'o',
        "\u0275": 'o',
        "\u01A3": 'oi',
        "\u0223": 'ou',
        "\uA74F": 'oo',
        "\u24DF": 'p',
        "\uFF50": 'p',
        "\u1E55": 'p',
        "\u1E57": 'p',
        "\u01A5": 'p',
        "\u1D7D": 'p',
        "\uA751": 'p',
        "\uA753": 'p',
        "\uA755": 'p',
        "\u24E0": 'q',
        "\uFF51": 'q',
        "\u024B": 'q',
        "\uA757": 'q',
        "\uA759": 'q',
        "\u24E1": 'r',
        "\uFF52": 'r',
        "\u0155": 'r',
        "\u1E59": 'r',
        "\u0159": 'r',
        "\u0211": 'r',
        "\u0213": 'r',
        "\u1E5B": 'r',
        "\u1E5D": 'r',
        "\u0157": 'r',
        "\u1E5F": 'r',
        "\u024D": 'r',
        "\u027D": 'r',
        "\uA75B": 'r',
        "\uA7A7": 'r',
        "\uA783": 'r',
        "\u24E2": 's',
        "\uFF53": 's',
        "\xDF": 's',
        "\u015B": 's',
        "\u1E65": 's',
        "\u015D": 's',
        "\u1E61": 's',
        "\u0161": 's',
        "\u1E67": 's',
        "\u1E63": 's',
        "\u1E69": 's',
        "\u0219": 's',
        "\u015F": 's',
        "\u023F": 's',
        "\uA7A9": 's',
        "\uA785": 's',
        "\u1E9B": 's',
        "\u24E3": 't',
        "\uFF54": 't',
        "\u1E6B": 't',
        "\u1E97": 't',
        "\u0165": 't',
        "\u1E6D": 't',
        "\u021B": 't',
        "\u0163": 't',
        "\u1E71": 't',
        "\u1E6F": 't',
        "\u0167": 't',
        "\u01AD": 't',
        "\u0288": 't',
        "\u2C66": 't',
        "\uA787": 't',
        "\uA729": 'tz',
        "\u24E4": 'u',
        "\uFF55": 'u',
        "\xF9": 'u',
        "\xFA": 'u',
        "\xFB": 'u',
        "\u0169": 'u',
        "\u1E79": 'u',
        "\u016B": 'u',
        "\u1E7B": 'u',
        "\u016D": 'u',
        "\xFC": 'u',
        "\u01DC": 'u',
        "\u01D8": 'u',
        "\u01D6": 'u',
        "\u01DA": 'u',
        "\u1EE7": 'u',
        "\u016F": 'u',
        "\u0171": 'u',
        "\u01D4": 'u',
        "\u0215": 'u',
        "\u0217": 'u',
        "\u01B0": 'u',
        "\u1EEB": 'u',
        "\u1EE9": 'u',
        "\u1EEF": 'u',
        "\u1EED": 'u',
        "\u1EF1": 'u',
        "\u1EE5": 'u',
        "\u1E73": 'u',
        "\u0173": 'u',
        "\u1E77": 'u',
        "\u1E75": 'u',
        "\u0289": 'u',
        "\u24E5": 'v',
        "\uFF56": 'v',
        "\u1E7D": 'v',
        "\u1E7F": 'v',
        "\u028B": 'v',
        "\uA75F": 'v',
        "\u028C": 'v',
        "\uA761": 'vy',
        "\u24E6": 'w',
        "\uFF57": 'w',
        "\u1E81": 'w',
        "\u1E83": 'w',
        "\u0175": 'w',
        "\u1E87": 'w',
        "\u1E85": 'w',
        "\u1E98": 'w',
        "\u1E89": 'w',
        "\u2C73": 'w',
        "\u24E7": 'x',
        "\uFF58": 'x',
        "\u1E8B": 'x',
        "\u1E8D": 'x',
        "\u24E8": 'y',
        "\uFF59": 'y',
        "\u1EF3": 'y',
        "\xFD": 'y',
        "\u0177": 'y',
        "\u1EF9": 'y',
        "\u0233": 'y',
        "\u1E8F": 'y',
        "\xFF": 'y',
        "\u1EF7": 'y',
        "\u1E99": 'y',
        "\u1EF5": 'y',
        "\u01B4": 'y',
        "\u024F": 'y',
        "\u1EFF": 'y',
        "\u24E9": 'z',
        "\uFF5A": 'z',
        "\u017A": 'z',
        "\u1E91": 'z',
        "\u017C": 'z',
        "\u017E": 'z',
        "\u1E93": 'z',
        "\u1E95": 'z',
        "\u01B6": 'z',
        "\u0225": 'z',
        "\u0240": 'z',
        "\u2C6C": 'z',
        "\uA763": 'z',
        "\u0386": "\u0391",
        "\u0388": "\u0395",
        "\u0389": "\u0397",
        "\u038A": "\u0399",
        "\u03AA": "\u0399",
        "\u038C": "\u039F",
        "\u038E": "\u03A5",
        "\u03AB": "\u03A5",
        "\u038F": "\u03A9",
        "\u03AC": "\u03B1",
        "\u03AD": "\u03B5",
        "\u03AE": "\u03B7",
        "\u03AF": "\u03B9",
        "\u03CA": "\u03B9",
        "\u0390": "\u03B9",
        "\u03CC": "\u03BF",
        "\u03CD": "\u03C5",
        "\u03CB": "\u03C5",
        "\u03B0": "\u03C5",
        "\u03C9": "\u03C9",
        "\u03C2": "\u03C3"
      };
      /**
       * @fileoverview added by tsickle
       * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
       */

      var OptionList = /*#__PURE__*/function () {
        /**
         * @param {?} options
         */
        function OptionList(options) {
          _classCallCheck(this, OptionList);

          /* Consider using these for performance improvement. */
          // private _selection: Array<Option>;
          // private _filtered: Array<Option>;
          // private _value: Array<string>;
          this._highlightedOption = null;

          if (typeof options === 'undefined' || options === null) {
            options = [];
          }

          this._options = options.map(
          /**
          * @param {?} option
          * @return {?}
          */
          function (option) {
            /** @type {?} */
            var o = new Option(option);

            if (option.disabled) {
              o.disabled = true;
            }

            return o;
          });
          this._hasShown = this._options.length > 0;
          this.highlight();
        }
        /**
         * @return {?}
         */


        _createClass(OptionList, [{
          key: "getOptionsByValue",

          /**
           * @param {?} value
           * @return {?}
           */
          value: function getOptionsByValue(value) {
            return this.options.filter(
            /**
            * @param {?} option
            * @return {?}
            */
            function (option) {
              return option.value === value;
            });
          }
          /**
           * Value. *
           * @return {?}
           */

        }, {
          key: "select",

          /**
           * @param {?} option
           * @param {?} multiple
           * @return {?}
           */
          value: function select(option, multiple) {
            if (!multiple) {
              this.clearSelection();
            }

            option.selected = true;
            this.updateHasSelected();
          }
          /**
           * @param {?} option
           * @return {?}
           */

        }, {
          key: "deselect",
          value: function deselect(option) {
            option.selected = false;
            this.updateHasSelected();
          }
          /**
           * @return {?}
           */

        }, {
          key: "clearSelection",
          value: function clearSelection() {
            this.options.forEach(
            /**
            * @param {?} option
            * @return {?}
            */
            function (option) {
              option.selected = false;
            });
            this._hasSelected = false;
          }
          /**
           * @private
           * @return {?}
           */

        }, {
          key: "updateHasSelected",
          value: function updateHasSelected() {
            this._hasSelected = this.options.some(
            /**
            * @param {?} option
            * @return {?}
            */
            function (option) {
              return option.selected;
            });
          }
          /**
           * Filter. *
           * @return {?}
           */

        }, {
          key: "filter",

          /**
           * @param {?} term
           * @return {?}
           */
          value: function filter(term) {
            /** @type {?} */
            var anyShown = false;

            if (term.trim() === '') {
              this.resetFilter();
              anyShown = this.options.length > 0;
            } else {
              this.options.forEach(
              /**
              * @param {?} option
              * @return {?}
              */
              function (option) {
                /** @type {?} */
                var l = Diacritics.strip(option.label).toUpperCase();
                /** @type {?} */

                var t = Diacritics.strip(term).toUpperCase();
                option.shown = l.indexOf(t) > -1;

                if (option.shown) {
                  anyShown = true;
                }
              });
            }

            this.highlight();
            this._hasShown = anyShown;
            return anyShown;
          }
          /**
           * @private
           * @return {?}
           */

        }, {
          key: "resetFilter",
          value: function resetFilter() {
            this.options.forEach(
            /**
            * @param {?} option
            * @return {?}
            */
            function (option) {
              option.shown = true;
            });
          }
          /**
           * Highlight. *
           * @return {?}
           */

        }, {
          key: "highlight",

          /**
           * @return {?}
           */
          value: function highlight() {
            /** @type {?} */
            var option = this.hasShownSelected() ? this.getFirstShownSelected() : this.getFirstShown();
            this.highlightOption(option);
          }
          /**
           * @param {?} option
           * @return {?}
           */

        }, {
          key: "highlightOption",
          value: function highlightOption(option) {
            this.clearHighlightedOption();

            if (option !== null) {
              option.highlighted = true;
              this._highlightedOption = option;
            }
          }
          /**
           * @return {?}
           */

        }, {
          key: "highlightNextOption",
          value: function highlightNextOption() {
            /** @type {?} */
            var shownEnabledOptions = this.filteredEnabled;
            /** @type {?} */

            var index = this.getHighlightedIndexFromList(shownEnabledOptions);

            if (index > -1 && index < shownEnabledOptions.length - 1) {
              this.highlightOption(shownEnabledOptions[index + 1]);
            }
          }
          /**
           * @return {?}
           */

        }, {
          key: "highlightPreviousOption",
          value: function highlightPreviousOption() {
            /** @type {?} */
            var shownEnabledOptions = this.filteredEnabled;
            /** @type {?} */

            var index = this.getHighlightedIndexFromList(shownEnabledOptions);

            if (index > 0) {
              this.highlightOption(shownEnabledOptions[index - 1]);
            }
          }
          /**
           * @private
           * @return {?}
           */

        }, {
          key: "clearHighlightedOption",
          value: function clearHighlightedOption() {
            if (this.highlightedOption !== null) {
              this.highlightedOption.highlighted = false;
              this._highlightedOption = null;
            }
          }
          /**
           * @private
           * @param {?} options
           * @return {?}
           */

        }, {
          key: "getHighlightedIndexFromList",
          value: function getHighlightedIndexFromList(options) {
            for (var i = 0; i < options.length; i++) {
              if (options[i].highlighted) {
                return i;
              }
            }

            return -1;
          }
          /**
           * @return {?}
           */

        }, {
          key: "getHighlightedIndex",
          value: function getHighlightedIndex() {
            return this.getHighlightedIndexFromList(this.filtered);
          }
          /**
           * Util. *
           * @return {?}
           */

        }, {
          key: "hasShownSelected",
          value: function hasShownSelected() {
            return this.options.some(
            /**
            * @param {?} option
            * @return {?}
            */
            function (option) {
              return option.shown && option.selected;
            });
          }
          /**
           * @private
           * @return {?}
           */

        }, {
          key: "getFirstShown",
          value: function getFirstShown() {
            var _iterator = _createForOfIteratorHelper(this.options),
                _step;

            try {
              for (_iterator.s(); !(_step = _iterator.n()).done;) {
                var option = _step.value;

                if (option.shown) {
                  return option;
                }
              }
            } catch (err) {
              _iterator.e(err);
            } finally {
              _iterator.f();
            }

            return null;
          }
          /**
           * @private
           * @return {?}
           */

        }, {
          key: "getFirstShownSelected",
          value: function getFirstShownSelected() {
            var _iterator2 = _createForOfIteratorHelper(this.options),
                _step2;

            try {
              for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
                var option = _step2.value;

                if (option.shown && option.selected) {
                  return option;
                }
              }
            } catch (err) {
              _iterator2.e(err);
            } finally {
              _iterator2.f();
            }

            return null;
          } // v0 and v1 are assumed not to be undefined or null.

          /**
           * @param {?} v0
           * @param {?} v1
           * @return {?}
           */

        }, {
          key: "hasShown",
          get: function get() {
            return this._hasShown;
          }
          /**
           * @return {?}
           */

        }, {
          key: "hasSelected",
          get: function get() {
            return this._hasSelected;
          }
          /**
           * Options. *
           * @return {?}
           */

        }, {
          key: "options",
          get: function get() {
            return this._options;
          }
        }, {
          key: "value",
          get: function get() {
            return this.selection.map(
            /**
            * @param {?} option
            * @return {?}
            */
            function (option) {
              return option.value;
            });
          }
          /**
           * @param {?} v
           * @return {?}
           */
          ,
          set: function set(v) {
            v = typeof v === 'undefined' || v === null ? [] : v;
            this.options.forEach(
            /**
            * @param {?} option
            * @return {?}
            */
            function (option) {
              option.selected = v.indexOf(option.value) > -1;
            });
            this.updateHasSelected();
          }
          /**
           * Selection. *
           * @return {?}
           */

        }, {
          key: "selection",
          get: function get() {
            return this.options.filter(
            /**
            * @param {?} option
            * @return {?}
            */
            function (option) {
              return option.selected;
            });
          }
        }, {
          key: "filtered",
          get: function get() {
            return this.options.filter(
            /**
            * @param {?} option
            * @return {?}
            */
            function (option) {
              return option.shown;
            });
          }
          /**
           * @return {?}
           */

        }, {
          key: "filteredEnabled",
          get: function get() {
            return this.options.filter(
            /**
            * @param {?} option
            * @return {?}
            */
            function (option) {
              return option.shown && !option.disabled;
            });
          }
        }, {
          key: "highlightedOption",
          get: function get() {
            return this._highlightedOption;
          }
        }], [{
          key: "equalValues",
          value: function equalValues(v0, v1) {
            if (v0.length !== v1.length) {
              return false;
            }
            /** @type {?} */


            var a = v0.slice().sort();
            /** @type {?} */

            var b = v1.slice().sort();
            return a.every(
            /**
            * @param {?} v
            * @param {?} i
            * @return {?}
            */
            function (v, i) {
              return v === b[i];
            });
          }
        }]);

        return OptionList;
      }();
      /**
       * @fileoverview added by tsickle
       * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
       */


      var SelectDropdownComponent = /*#__PURE__*/function () {
        /**
         * @param {?} hostElement
         */
        function SelectDropdownComponent(hostElement) {
          _classCallCheck(this, SelectDropdownComponent);

          this.hostElement = hostElement;
          this.optionClicked = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
          this.optionsListClick = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
          this.singleFilterClick = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
          this.singleFilterFocus = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
          this.singleFilterInput = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
          this.singleFilterKeydown = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
          this.disabledColor = '#fff';
          this.disabledTextColor = '9e9e9e';
        }
        /**
         * Event handlers. *
         * @return {?}
         */


        _createClass(SelectDropdownComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.optionsReset();
          }
          /**
           * @param {?} changes
           * @return {?}
           */

        }, {
          key: "ngOnChanges",
          value: function ngOnChanges(changes) {
            if (changes.hasOwnProperty('optionList')) {
              this.optionsReset();
            }
          }
          /**
           * @return {?}
           */

        }, {
          key: "ngAfterViewInit",
          value: function ngAfterViewInit() {
            this.moveHighlightedIntoView();

            if (!this.multiple && this.filterEnabled) {
              this.filterInput.nativeElement.focus();
            }
          }
          /**
           * @return {?}
           */

        }, {
          key: "onOptionsListClick",
          value: function onOptionsListClick() {
            this.optionsListClick.emit(null);
          }
          /**
           * @return {?}
           */

        }, {
          key: "onSingleFilterClick",
          value: function onSingleFilterClick() {
            this.singleFilterClick.emit(null);
          }
          /**
           * @param {?} event
           * @return {?}
           */

        }, {
          key: "onSingleFilterInput",
          value: function onSingleFilterInput(event) {
            this.singleFilterInput.emit(event.target.value);
          }
          /**
           * @param {?} event
           * @return {?}
           */

        }, {
          key: "onSingleFilterKeydown",
          value: function onSingleFilterKeydown(event) {
            this.singleFilterKeydown.emit(event);
          }
          /**
           * @return {?}
           */

        }, {
          key: "onSingleFilterFocus",
          value: function onSingleFilterFocus() {
            this.singleFilterFocus.emit(null);
          }
          /**
           * @param {?} event
           * @return {?}
           */

        }, {
          key: "onOptionsWheel",
          value: function onOptionsWheel(event) {
            this.handleOptionsWheel(event);
          }
          /**
           * @param {?} option
           * @return {?}
           */

        }, {
          key: "onOptionMouseover",
          value: function onOptionMouseover(option) {
            this.optionList.highlightOption(option);
          }
          /**
           * @param {?} option
           * @return {?}
           */

        }, {
          key: "onOptionClick",
          value: function onOptionClick(option) {
            this.optionClicked.emit(option);
          }
          /**
           * Initialization. *
           * @private
           * @return {?}
           */

        }, {
          key: "optionsReset",
          value: function optionsReset() {
            this.optionList.filter('');
            this.optionList.highlight();
          }
          /**
           * View. *
           * @param {?} option
           * @return {?}
           */

        }, {
          key: "getOptionStyle",
          value: function getOptionStyle(option) {
            if (option.highlighted) {
              /** @type {?} */
              var style = {};

              if (typeof this.highlightColor !== 'undefined') {
                style['background-color'] = this.highlightColor;
              }

              if (typeof this.highlightTextColor !== 'undefined') {
                style['color'] = this.highlightTextColor;
              }

              return style;
            } else {
              return {};
            }
          }
          /**
           * @return {?}
           */

        }, {
          key: "moveHighlightedIntoView",
          value: function moveHighlightedIntoView() {
            /** @type {?} */
            var list = this.optionsList.nativeElement;
            /** @type {?} */

            var listHeight = list.offsetHeight;
            /** @type {?} */

            var itemIndex = this.optionList.getHighlightedIndex();

            if (itemIndex > -1) {
              /** @type {?} */
              var item = list.children[0].children[itemIndex];
              /** @type {?} */

              var itemHeight = item.offsetHeight;
              /** @type {?} */

              var itemTop = itemIndex * itemHeight;
              /** @type {?} */

              var itemBottom = itemTop + itemHeight;
              /** @type {?} */

              var viewTop = list.scrollTop;
              /** @type {?} */

              var viewBottom = viewTop + listHeight;

              if (itemBottom > viewBottom) {
                list.scrollTop = itemBottom - listHeight;
              } else if (itemTop < viewTop) {
                list.scrollTop = itemTop;
              }
            }
          }
          /**
           * @private
           * @param {?} e
           * @return {?}
           */

        }, {
          key: "handleOptionsWheel",
          value: function handleOptionsWheel(e) {
            /** @type {?} */
            var div = this.optionsList.nativeElement;
            /** @type {?} */

            var atTop = div.scrollTop === 0;
            /** @type {?} */

            var atBottom = div.offsetHeight + div.scrollTop === div.scrollHeight;

            if (atTop && e.deltaY < 0) {
              e.preventDefault();
            } else if (atBottom && e.deltaY > 0) {
              e.preventDefault();
            }
          }
        }]);

        return SelectDropdownComponent;
      }();

      SelectDropdownComponent.ɵfac = function SelectDropdownComponent_Factory(t) {
        return new (t || SelectDropdownComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]));
      };

      SelectDropdownComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: SelectDropdownComponent,
        selectors: [["select-dropdown"]],
        viewQuery: function SelectDropdownComponent_Query(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c0, true);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstaticViewQuery"](_c1, true);
          }

          if (rf & 2) {
            var _t;

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.filterInput = _t.first);
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.optionsList = _t.first);
          }
        },
        inputs: {
          filterEnabled: "filterEnabled",
          highlightColor: "highlightColor",
          highlightTextColor: "highlightTextColor",
          left: "left",
          multiple: "multiple",
          notFoundMsg: "notFoundMsg",
          optionList: "optionList",
          isBelow: "isBelow",
          top: "top",
          width: "width",
          placeholder: "placeholder",
          optionTemplate: "optionTemplate"
        },
        outputs: {
          optionClicked: "optionClicked",
          optionsListClick: "optionsListClick",
          singleFilterClick: "singleFilterClick",
          singleFilterFocus: "singleFilterFocus",
          singleFilterInput: "singleFilterInput",
          singleFilterKeydown: "singleFilterKeydown"
        },
        features: [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵNgOnChangesFeature"]],
        decls: 7,
        vars: 12,
        consts: [[3, "ngClass", "ngStyle"], ["class", "filter", 4, "ngIf"], [1, "options", 3, "click"], ["optionsList", ""], [3, "wheel"], [3, "ngClass", "ngStyle", "click", "mouseover", 4, "ngFor", "ngForOf"], ["class", "message", 4, "ngIf"], [1, "filter"], ["autocomplete", "off", 3, "placeholder", "click", "input", "keydown", "focus"], ["filterInput", ""], [3, "ngClass", "ngStyle", "click", "mouseover"], [4, "ngTemplateOutlet", "ngTemplateOutletContext"], [4, "ngIf"], [1, "message"]],
        template: function SelectDropdownComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, SelectDropdownComponent_div_1_Template, 3, 1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2, 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SelectDropdownComponent_Template_div_click_2_listener() {
              return ctx.onOptionsListClick();
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "ul", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("wheel", function SelectDropdownComponent_Template_ul_wheel_4_listener($event) {
              return ctx.onOptionsWheel($event);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](5, SelectDropdownComponent_li_5_Template, 3, 11, "li", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](6, SelectDropdownComponent_li_6_Template, 2, 1, "li", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction2"](5, _c4, ctx.isBelow, !ctx.isBelow))("ngStyle", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction3"](8, _c5, ctx.top, ctx.left, ctx.width));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.multiple && ctx.filterEnabled);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.optionList.filtered);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.optionList.hasShown);
          }
        },
        directives: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["NgClass"], _angular_common__WEBPACK_IMPORTED_MODULE_1__["NgStyle"], _angular_common__WEBPACK_IMPORTED_MODULE_1__["NgIf"], _angular_common__WEBPACK_IMPORTED_MODULE_1__["NgForOf"], _angular_common__WEBPACK_IMPORTED_MODULE_1__["NgTemplateOutlet"]],
        styles: ["select-dropdown,select-dropdown *{box-sizing:border-box}select-dropdown>div{background-color:#fff;border:1px solid #ccc;box-sizing:border-box;position:absolute;z-index:1}select-dropdown>div.above{border-bottom:none}select-dropdown>div.below{border-top:none}select-dropdown>div .filter{padding:3px;width:100%}select-dropdown>div .filter input{border:1px solid #eee;box-sizing:border-box;padding:4px;width:100%}select-dropdown>div .options{max-height:200px;overflow-y:auto}select-dropdown>div .options ul{list-style:none;margin:0;padding:0}select-dropdown>div .options ul li{padding:4px 8px;cursor:pointer;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}select-dropdown .selected{background-color:#e0e0e0}select-dropdown .highlighted,select-dropdown .selected.highlighted{background-color:#2196f3;color:#fff}select-dropdown .disabled{background-color:#fff;color:#9e9e9e;cursor:default;pointer-events:none}"],
        encapsulation: 2
      });
      /** @nocollapse */

      SelectDropdownComponent.ctorParameters = function () {
        return [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]
        }];
      };

      SelectDropdownComponent.propDecorators = {
        filterEnabled: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        highlightColor: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        highlightTextColor: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        left: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        multiple: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        notFoundMsg: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        optionList: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        isBelow: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        top: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        width: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        placeholder: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        optionTemplate: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        optionClicked: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }],
        optionsListClick: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }],
        singleFilterClick: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }],
        singleFilterFocus: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }],
        singleFilterInput: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }],
        singleFilterKeydown: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }],
        filterInput: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"],
          args: ['filterInput', {
            "static": false
          }]
        }],
        optionsList: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"],
          args: ['optionsList', {
            "static": true
          }]
        }]
      };
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](SelectDropdownComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'select-dropdown',
            template: "<div\n    [ngClass]=\"{'below': isBelow, 'above': !isBelow}\"\n    [ngStyle]=\"{'top.px': top, 'left.px': left, 'width.px': width}\">\n\n    <div class=\"filter\"\n        *ngIf=\"!multiple && filterEnabled\">\n        <input\n            #filterInput\n            autocomplete=\"off\"\n            [placeholder]=\"placeholder\"\n            (click)=\"onSingleFilterClick()\"\n            (input)=\"onSingleFilterInput($event)\"\n            (keydown)=\"onSingleFilterKeydown($event)\"\n            (focus)=\"onSingleFilterFocus()\">\n    </div>\n\n    <div class=\"options\"\n        (click)=\"onOptionsListClick()\"\n        #optionsList>\n        <ul\n            (wheel)=\"onOptionsWheel($event)\">\n            <li *ngFor=\"let option of optionList.filtered\"\n                [ngClass]=\"{'highlighted': option.highlighted, 'selected': option.selected, 'disabled': option.disabled}\"\n                [ngStyle]=\"getOptionStyle(option)\"\n                (click)=\"onOptionClick(option)\"\n                (mouseover)=\"onOptionMouseover(option)\">\n                <ng-container *ngTemplateOutlet=\"optionTemplate; context:{option: option.wrappedOption}\"></ng-container>\n                <span *ngIf=\"!optionTemplate\">{{option.label}}</span>\n            </li>\n            <li\n                *ngIf=\"!optionList.hasShown\"\n                class=\"message\">\n                {{notFoundMsg}}\n            </li>\n        </ul>\n    </div>\n</div>\n",
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewEncapsulation"].None,
            styles: ["select-dropdown,select-dropdown *{box-sizing:border-box}select-dropdown>div{background-color:#fff;border:1px solid #ccc;box-sizing:border-box;position:absolute;z-index:1}select-dropdown>div.above{border-bottom:none}select-dropdown>div.below{border-top:none}select-dropdown>div .filter{padding:3px;width:100%}select-dropdown>div .filter input{border:1px solid #eee;box-sizing:border-box;padding:4px;width:100%}select-dropdown>div .options{max-height:200px;overflow-y:auto}select-dropdown>div .options ul{list-style:none;margin:0;padding:0}select-dropdown>div .options ul li{padding:4px 8px;cursor:pointer;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}select-dropdown .selected{background-color:#e0e0e0}select-dropdown .highlighted,select-dropdown .selected.highlighted{background-color:#2196f3;color:#fff}select-dropdown .disabled{background-color:#fff;color:#9e9e9e;cursor:default;pointer-events:none}"]
          }]
        }], function () {
          return [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]
          }];
        }, {
          optionClicked: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
          }],
          optionsListClick: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
          }],
          singleFilterClick: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
          }],
          singleFilterFocus: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
          }],
          singleFilterInput: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
          }],
          singleFilterKeydown: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
          }],
          filterEnabled: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          highlightColor: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          highlightTextColor: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          left: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          multiple: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          notFoundMsg: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          optionList: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          isBelow: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          top: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          width: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          placeholder: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          optionTemplate: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          filterInput: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"],
            args: ['filterInput', {
              "static": false
            }]
          }],
          optionsList: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"],
            args: ['optionsList', {
              "static": true
            }]
          }]
        });
      })();
      /**
       * @fileoverview added by tsickle
       * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
       */

      /** @type {?} */


      var SELECT_VALUE_ACCESSOR = {
        provide: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NG_VALUE_ACCESSOR"],
        useExisting: Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["forwardRef"])(
        /**
        * @return {?}
        */
        function () {
          return SelectComponent;
        }),
        multi: true
      };

      var SelectComponent = /*#__PURE__*/function () {
        /**
         * @param {?} hostElement
         */
        function SelectComponent(hostElement) {
          _classCallCheck(this, SelectComponent);

          this.hostElement = hostElement; // Data input.

          this.options = []; // Functionality settings.

          this.allowClear = false;
          this.disabled = false;
          this.multiple = false;
          this.noFilter = 0; // Text settings.

          this.notFoundMsg = 'No results found';
          this.placeholder = '';
          this.filterPlaceholder = '';
          this.label = ''; // Output events.

          this.opened = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
          this.closed = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
          this.selected = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
          this.deselected = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
          this.focus = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
          this.blur = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
          this.noOptionsFound = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
          this.filterInputChanged = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
          this._value = [];
          this.optionList = new OptionList([]); // View state variables.

          this.hasFocus = false;
          this.isOpen = false;
          this.isBelow = true;
          this.filterEnabled = true;
          this.filterInputWidth = 1;
          this.isDisabled = false;
          this.placeholderView = '';
          this.clearClicked = false;
          this.selectContainerClicked = false;
          this.optionListClicked = false;
          this.optionClicked = false;

          this.onChange =
          /**
          * @param {?} _
          * @return {?}
          */
          function (_) {};

          this.onTouched =
          /**
          * @return {?}
          */
          function () {};
          /**
           * Keys. *
           */


          this.KEYS = {
            BACKSPACE: 8,
            TAB: 9,
            ENTER: 13,
            ESC: 27,
            SPACE: 32,
            UP: 38,
            DOWN: 40
          };
        }
        /**
         * Event handlers. *
         * @return {?}
         */


        _createClass(SelectComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.placeholderView = this.placeholder;
          }
          /**
           * @param {?} changes
           * @return {?}
           */

        }, {
          key: "ngOnChanges",
          value: function ngOnChanges(changes) {
            this.handleInputChanges(changes);
          }
          /**
           * @return {?}
           */

        }, {
          key: "ngAfterViewInit",
          value: function ngAfterViewInit() {
            this.updateState();
          }
          /**
           * @return {?}
           */

        }, {
          key: "onWindowBlur",
          value: function onWindowBlur() {
            this._blur();
          }
          /**
           * @return {?}
           */

        }, {
          key: "onWindowClick",
          value: function onWindowClick() {
            if (!this.selectContainerClicked && (!this.optionListClicked || this.optionListClicked && this.optionClicked)) {
              this.closeDropdown(this.optionClicked);

              if (!this.optionClicked) {
                this._blur();
              }
            }

            this.clearClicked = false;
            this.selectContainerClicked = false;
            this.optionListClicked = false;
            this.optionClicked = false;
          }
          /**
           * @return {?}
           */

        }, {
          key: "onWindowResize",
          value: function onWindowResize() {
            this.updateWidth();
          }
          /**
           * @param {?} event
           * @return {?}
           */

        }, {
          key: "onSelectContainerClick",
          value: function onSelectContainerClick(event) {
            this.selectContainerClicked = true;

            if (!this.clearClicked) {
              this.toggleDropdown();
            }
          }
          /**
           * @return {?}
           */

        }, {
          key: "onSelectContainerFocus",
          value: function onSelectContainerFocus() {
            this._focus();
          }
          /**
           * @param {?} event
           * @return {?}
           */

        }, {
          key: "onSelectContainerKeydown",
          value: function onSelectContainerKeydown(event) {
            this.handleSelectContainerKeydown(event);
          }
          /**
           * @return {?}
           */

        }, {
          key: "onOptionsListClick",
          value: function onOptionsListClick() {
            this.optionListClicked = true;
          }
          /**
           * @param {?} option
           * @return {?}
           */

        }, {
          key: "onDropdownOptionClicked",
          value: function onDropdownOptionClicked(option) {
            this.optionClicked = true;
            this.multiple ? this.toggleSelectOption(option) : this.selectOption(option);
          }
          /**
           * @return {?}
           */

        }, {
          key: "onSingleFilterClick",
          value: function onSingleFilterClick() {
            this.selectContainerClicked = true;
          }
          /**
           * @return {?}
           */

        }, {
          key: "onSingleFilterFocus",
          value: function onSingleFilterFocus() {
            this._focus();
          }
          /**
           * @param {?} term
           * @return {?}
           */

        }, {
          key: "onFilterInput",
          value: function onFilterInput(term) {
            this.filterInputChanged.emit(term);
            this.filter(term);
          }
          /**
           * @param {?} event
           * @return {?}
           */

        }, {
          key: "onSingleFilterKeydown",
          value: function onSingleFilterKeydown(event) {
            this.handleSingleFilterKeydown(event);
          }
          /**
           * @param {?} event
           * @return {?}
           */

        }, {
          key: "onMultipleFilterKeydown",
          value: function onMultipleFilterKeydown(event) {
            this.handleMultipleFilterKeydown(event);
          }
          /**
           * @return {?}
           */

        }, {
          key: "onMultipleFilterFocus",
          value: function onMultipleFilterFocus() {
            this._focus();
          }
          /**
           * @param {?} event
           * @return {?}
           */

        }, {
          key: "onClearSelectionClick",
          value: function onClearSelectionClick(event) {
            this.clearClicked = true;
            this.clearSelection();
            this.closeDropdown(true);
          }
          /**
           * @param {?} option
           * @return {?}
           */

        }, {
          key: "onDeselectOptionClick",
          value: function onDeselectOptionClick(option) {
            this.clearClicked = true;
            this.deselectOption(option);
          }
          /**
           * API. *
           * @return {?}
           */
          // TODO fix issues with global click/key handler that closes the dropdown.

        }, {
          key: "open",
          value: function open() {
            this.openDropdown();
          }
          /**
           * @return {?}
           */

        }, {
          key: "close",
          value: function close() {
            this.closeDropdown(false);
          }
          /**
           * @return {?}
           */

        }, {
          key: "clear",
          value: function clear() {
            this.clearSelection();
          }
          /**
           * @param {?} value
           * @return {?}
           */

        }, {
          key: "select",
          value: function select(value) {
            this.writeValue(value);
          }
          /**
           * ControlValueAccessor interface methods. *
           * @param {?} value
           * @return {?}
           */

        }, {
          key: "writeValue",
          value: function writeValue(value) {
            this.value = value;
          }
          /**
           * @param {?} fn
           * @return {?}
           */

        }, {
          key: "registerOnChange",
          value: function registerOnChange(fn) {
            this.onChange = fn;
          }
          /**
           * @param {?} fn
           * @return {?}
           */

        }, {
          key: "registerOnTouched",
          value: function registerOnTouched(fn) {
            this.onTouched = fn;
          }
          /**
           * @param {?} isDisabled
           * @return {?}
           */

        }, {
          key: "setDisabledState",
          value: function setDisabledState(isDisabled) {
            this.disabled = isDisabled;
          }
          /**
           * Input change handling. *
           * @private
           * @param {?} changes
           * @return {?}
           */

        }, {
          key: "handleInputChanges",
          value: function handleInputChanges(changes) {
            /** @type {?} */
            var optionsChanged = changes.hasOwnProperty('options');
            /** @type {?} */

            var noFilterChanged = changes.hasOwnProperty('noFilter');
            /** @type {?} */

            var placeholderChanged = changes.hasOwnProperty('placeholder');

            if (optionsChanged) {
              this.updateOptionList(changes.options.currentValue);
              this.updateState();
            }

            if (optionsChanged || noFilterChanged) {
              this.updateFilterEnabled();
            }

            if (placeholderChanged) {
              this.updateState();
            }
          }
          /**
           * @private
           * @param {?} options
           * @return {?}
           */

        }, {
          key: "updateOptionList",
          value: function updateOptionList(options) {
            this.optionList = new OptionList(options);
            this.optionList.value = this._value;
          }
          /**
           * @private
           * @return {?}
           */

        }, {
          key: "updateFilterEnabled",
          value: function updateFilterEnabled() {
            this.filterEnabled = this.optionList.options.length >= this.noFilter;
          }
          /**
           * Value. *
           * @return {?}
           */

        }, {
          key: "valueChanged",

          /**
           * @private
           * @return {?}
           */
          value: function valueChanged() {
            this._value = this.optionList.value;
            this.updateState();
            this.onChange(this.value);
          }
          /**
           * @private
           * @return {?}
           */

        }, {
          key: "updateState",
          value: function updateState() {
            var _this5 = this;

            this.placeholderView = this.optionList.hasSelected ? '' : this.placeholder;
            setTimeout(
            /**
            * @return {?}
            */
            function () {
              _this5.updateFilterWidth();
            });
          }
          /**
           * Select. *
           * @private
           * @param {?} option
           * @return {?}
           */

        }, {
          key: "selectOption",
          value: function selectOption(option) {
            if (!option.selected && !option.disabled) {
              this.optionList.select(option, this.multiple);
              this.valueChanged();
              this.selected.emit(option.wrappedOption);
            }
          }
          /**
           * @private
           * @param {?} option
           * @return {?}
           */

        }, {
          key: "deselectOption",
          value: function deselectOption(option) {
            var _this6 = this;

            if (option.selected) {
              this.optionList.deselect(option);
              this.valueChanged();
              this.deselected.emit(option.wrappedOption);
              setTimeout(
              /**
              * @return {?}
              */
              function () {
                if (_this6.multiple) {
                  _this6.updatePosition();

                  _this6.optionList.highlight();

                  if (_this6.isOpen) {
                    _this6.dropdown.moveHighlightedIntoView();
                  }
                }
              });
            }
          }
          /**
           * @private
           * @return {?}
           */

        }, {
          key: "clearSelection",
          value: function clearSelection() {
            /** @type {?} */
            var selection = this.optionList.selection;

            if (selection.length > 0) {
              this.optionList.clearSelection();
              this.valueChanged();

              if (selection.length === 1) {
                this.deselected.emit(selection[0].wrappedOption);
              } else {
                this.deselected.emit(selection.map(
                /**
                * @param {?} option
                * @return {?}
                */
                function (option) {
                  return option.wrappedOption;
                }));
              }
            }
          }
          /**
           * @private
           * @param {?} option
           * @return {?}
           */

        }, {
          key: "toggleSelectOption",
          value: function toggleSelectOption(option) {
            option.selected ? this.deselectOption(option) : this.selectOption(option);
          }
          /**
           * @private
           * @return {?}
           */

        }, {
          key: "selectHighlightedOption",
          value: function selectHighlightedOption() {
            /** @type {?} */
            var option = this.optionList.highlightedOption;

            if (option !== null) {
              this.selectOption(option);
              this.closeDropdown(true);
            }
          }
          /**
           * @private
           * @return {?}
           */

        }, {
          key: "deselectLast",
          value: function deselectLast() {
            /** @type {?} */
            var sel = this.optionList.selection;

            if (sel.length > 0) {
              /** @type {?} */
              var option = sel[sel.length - 1];
              this.deselectOption(option);
              this.setMultipleFilterInput(option.label + ' ');
            }
          }
          /**
           * Dropdown. *
           * @private
           * @return {?}
           */

        }, {
          key: "toggleDropdown",
          value: function toggleDropdown() {
            if (!this.isDisabled) {
              this.isOpen ? this.closeDropdown(true) : this.openDropdown();
            }
          }
          /**
           * @private
           * @return {?}
           */

        }, {
          key: "openDropdown",
          value: function openDropdown() {
            var _this7 = this;

            if (!this.isOpen) {
              this.isOpen = true;
              this.updateWidth();
              setTimeout(
              /**
              * @return {?}
              */
              function () {
                _this7.updatePosition();

                if (_this7.multiple && _this7.filterEnabled) {
                  _this7.filterInput.nativeElement.focus();
                }

                _this7.opened.emit(null);
              });
            }
          }
          /**
           * @private
           * @param {?} focus
           * @return {?}
           */

        }, {
          key: "closeDropdown",
          value: function closeDropdown(focus) {
            if (this.isOpen) {
              this.clearFilterInput();
              this.updateFilterWidth();
              this.isOpen = false;

              if (focus) {
                this._focusSelectContainer();
              }

              this.closed.emit(null);
            }
          }
          /**
           * Filter. *
           * @private
           * @param {?} term
           * @return {?}
           */

        }, {
          key: "filter",
          value: function filter(term) {
            var _this8 = this;

            if (this.multiple) {
              if (!this.isOpen) {
                this.openDropdown();
              }

              this.updateFilterWidth();
            }

            setTimeout(
            /**
            * @return {?}
            */
            function () {
              /** @type {?} */
              var hasShown = _this8.optionList.filter(term);

              if (!hasShown) {
                _this8.noOptionsFound.emit(term);
              }
            });
          }
          /**
           * @private
           * @return {?}
           */

        }, {
          key: "clearFilterInput",
          value: function clearFilterInput() {
            if (this.multiple && this.filterEnabled) {
              this.filterInput.nativeElement.value = '';
            }
          }
          /**
           * @private
           * @param {?} value
           * @return {?}
           */

        }, {
          key: "setMultipleFilterInput",
          value: function setMultipleFilterInput(value) {
            if (this.filterEnabled) {
              this.filterInput.nativeElement.value = value;
            }
          }
          /**
           * @private
           * @param {?} event
           * @return {?}
           */

        }, {
          key: "handleSelectContainerKeydown",
          value: function handleSelectContainerKeydown(event) {
            var _this9 = this;

            /** @type {?} */
            var key = event.which;

            if (this.isOpen) {
              if (key === this.KEYS.ESC || key === this.KEYS.UP && event.altKey) {
                this.closeDropdown(true);
              } else if (key === this.KEYS.TAB) {
                this.closeDropdown(event.shiftKey);

                this._blur();
              } else if (key === this.KEYS.ENTER) {
                this.selectHighlightedOption();
              } else if (key === this.KEYS.UP) {
                this.optionList.highlightPreviousOption();
                this.dropdown.moveHighlightedIntoView();

                if (!this.filterEnabled) {
                  event.preventDefault();
                }
              } else if (key === this.KEYS.DOWN) {
                this.optionList.highlightNextOption();
                this.dropdown.moveHighlightedIntoView();

                if (!this.filterEnabled) {
                  event.preventDefault();
                }
              }
            } else {
              // DEPRICATED --> SPACE
              if (key === this.KEYS.ENTER || key === this.KEYS.SPACE || key === this.KEYS.DOWN && event.altKey) {
                /* FIREFOX HACK:
                 *
                 * The setTimeout is added to prevent the enter keydown event
                 * to be triggered for the filter input field, which causes
                 * the dropdown to be closed again.
                 */
                setTimeout(
                /**
                * @return {?}
                */
                function () {
                  _this9.openDropdown();
                });
              } else if (key === this.KEYS.TAB) {
                this._blur();
              }
            }
          }
          /**
           * @private
           * @param {?} event
           * @return {?}
           */

        }, {
          key: "handleMultipleFilterKeydown",
          value: function handleMultipleFilterKeydown(event) {
            /** @type {?} */
            var key = event.which;

            if (key === this.KEYS.BACKSPACE) {
              if (this.optionList.hasSelected && this.filterEnabled && this.filterInput.nativeElement.value === '') {
                this.deselectLast();
              }
            }
          }
          /**
           * @private
           * @param {?} event
           * @return {?}
           */

        }, {
          key: "handleSingleFilterKeydown",
          value: function handleSingleFilterKeydown(event) {
            /** @type {?} */
            var key = event.which;

            if (key === this.KEYS.ESC || key === this.KEYS.TAB || key === this.KEYS.UP || key === this.KEYS.DOWN || key === this.KEYS.ENTER) {
              this.handleSelectContainerKeydown(event);
            }
          }
          /**
           * View. *
           * @return {?}
           */

        }, {
          key: "_blur",
          value: function _blur() {
            if (this.hasFocus) {
              this.hasFocus = false;
              this.onTouched();
              this.blur.emit(null);
            }
          }
          /**
           * @return {?}
           */

        }, {
          key: "_focus",
          value: function _focus() {
            if (!this.hasFocus) {
              this.hasFocus = true;
              this.focus.emit(null);
            }
          }
          /**
           * @return {?}
           */

        }, {
          key: "_focusSelectContainer",
          value: function _focusSelectContainer() {
            this.selectionSpan.nativeElement.focus();
          }
          /**
           * @private
           * @return {?}
           */

        }, {
          key: "updateWidth",
          value: function updateWidth() {
            this.width = this.selectionSpan.nativeElement.getBoundingClientRect().width;
          }
          /**
           * @private
           * @return {?}
           */

        }, {
          key: "updatePosition",
          value: function updatePosition() {
            if (typeof this.dropdown !== 'undefined') {
              /** @type {?} */
              var hostRect = this.hostElement.nativeElement.getBoundingClientRect();
              /** @type {?} */

              var spanRect = this.selectionSpan.nativeElement.getBoundingClientRect();
              /** @type {?} */

              var dropRect = this.dropdown.hostElement.nativeElement.firstElementChild.getBoundingClientRect();
              /** @type {?} */

              var windowHeight = window.innerHeight;
              /** @type {?} */

              var top = spanRect.top - hostRect.top;
              /** @type {?} */

              var bottom = hostRect.bottom + dropRect.height;
              this.isBelow = bottom < windowHeight;
              this.left = spanRect.left - hostRect.left;
              this.top = this.isBelow ? top + spanRect.height : top - dropRect.height;
            }
          }
          /**
           * @private
           * @return {?}
           */

        }, {
          key: "updateFilterWidth",
          value: function updateFilterWidth() {
            if (typeof this.filterInput !== 'undefined') {
              /** @type {?} */
              var value = this.filterInput.nativeElement.value;
              this.filterInputWidth = value.length === 0 ? 1 + this.placeholderView.length * 10 : 1 + value.length * 10;
            }
          }
        }, {
          key: "value",
          get: function get() {
            return this.multiple ? this._value : this._value[0];
          }
          /**
           * @param {?} v
           * @return {?}
           */
          ,
          set: function set(v) {
            if (typeof v === 'undefined' || v === null || v === '') {
              v = [];
            } else if (typeof v === 'string') {
              v = [v];
            } else if (!Array.isArray(v)) {
              throw new TypeError('Value must be a string or an array.');
            }

            this.optionList.value = v;
            this._value = v;
            this.updateState();
          }
        }]);

        return SelectComponent;
      }();

      SelectComponent.ɵfac = function SelectComponent_Factory(t) {
        return new (t || SelectComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]));
      };

      SelectComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: SelectComponent,
        selectors: [["ng-select"]],
        contentQueries: function SelectComponent_ContentQueries(rf, ctx, dirIndex) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵcontentQuery"](dirIndex, _c6, true);
          }

          if (rf & 2) {
            var _t;

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.optionTemplate = _t.first);
          }
        },
        viewQuery: function SelectComponent_Query(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstaticViewQuery"](_c7, true);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c8, true);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c0, true);
          }

          if (rf & 2) {
            var _t;

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.selectionSpan = _t.first);
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.dropdown = _t.first);
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.filterInput = _t.first);
          }
        },
        hostBindings: function SelectComponent_HostBindings(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("blur", function SelectComponent_blur_HostBindingHandler() {
              return ctx.onWindowBlur();
            }, false, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵresolveWindow"])("click", function SelectComponent_click_HostBindingHandler() {
              return ctx.onWindowClick();
            }, false, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵresolveWindow"])("resize", function SelectComponent_resize_HostBindingHandler() {
              return ctx.onWindowResize();
            }, false, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵresolveWindow"]);
          }
        },
        inputs: {
          options: "options",
          allowClear: "allowClear",
          disabled: "disabled",
          multiple: "multiple",
          noFilter: "noFilter",
          notFoundMsg: "notFoundMsg",
          placeholder: "placeholder",
          filterPlaceholder: "filterPlaceholder",
          label: "label",
          highlightColor: "highlightColor",
          highlightTextColor: "highlightTextColor"
        },
        outputs: {
          opened: "opened",
          closed: "closed",
          selected: "selected",
          deselected: "deselected",
          focus: "focus",
          blur: "blur",
          noOptionsFound: "noOptionsFound",
          filterInputChanged: "filterInputChanged"
        },
        features: [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵProvidersFeature"]([SELECT_VALUE_ACCESSOR]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵNgOnChangesFeature"]],
        decls: 6,
        vars: 12,
        consts: [[4, "ngIf"], [3, "ngClass", "click", "focus", "keydown"], ["selection", ""], ["class", "single", 4, "ngIf"], ["class", "multiple", 4, "ngIf"], [3, "multiple", "optionList", "notFoundMsg", "highlightColor", "highlightTextColor", "filterEnabled", "placeholder", "isBelow", "width", "top", "left", "optionTemplate", "optionClicked", "optionsListClick", "singleFilterClick", "singleFilterFocus", "singleFilterInput", "singleFilterKeydown", 4, "ngIf"], [1, "single"], ["class", "value", 4, "ngIf"], ["class", "placeholder", 4, "ngIf"], ["class", "clear", 3, "click", 4, "ngIf"], ["class", "toggle", 4, "ngIf"], [1, "value"], [4, "ngTemplateOutlet", "ngTemplateOutletContext"], [1, "placeholder"], [1, "clear", 3, "click"], [1, "toggle"], [1, "multiple"], ["class", "option", 4, "ngFor", "ngForOf"], ["autocomplete", "off", "tabindex", "-1", 3, "placeholder", "ngStyle", "input", "keydown", "focus", 4, "ngIf"], [1, "option"], [1, "deselect-option", 3, "click"], ["autocomplete", "off", "tabindex", "-1", 3, "placeholder", "ngStyle", "input", "keydown", "focus"], ["filterInput", ""], [3, "multiple", "optionList", "notFoundMsg", "highlightColor", "highlightTextColor", "filterEnabled", "placeholder", "isBelow", "width", "top", "left", "optionTemplate", "optionClicked", "optionsListClick", "singleFilterClick", "singleFilterFocus", "singleFilterInput", "singleFilterKeydown"], ["dropdown", ""]],
        template: function SelectComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](0, SelectComponent_label_0_Template, 2, 1, "label", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1, 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function SelectComponent_Template_div_click_1_listener($event) {
              return ctx.onSelectContainerClick($event);
            })("focus", function SelectComponent_Template_div_focus_1_listener() {
              return ctx.onSelectContainerFocus();
            })("keydown", function SelectComponent_Template_div_keydown_1_listener($event) {
              return ctx.onSelectContainerKeydown($event);
            });

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, SelectComponent_div_3_Template, 6, 5, "div", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, SelectComponent_div_4_Template, 4, 3, "div", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](5, SelectComponent_select_dropdown_5_Template, 2, 12, "select-dropdown", 5);
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.label !== "");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction5"](6, _c11, ctx.isOpen, ctx.hasFocus, ctx.isBelow, !ctx.isBelow, ctx.disabled));

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵattribute"]("tabindex", ctx.disabled ? null : 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.multiple);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.multiple);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.isOpen);
          }
        },
        directives: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["NgIf"], _angular_common__WEBPACK_IMPORTED_MODULE_1__["NgClass"], _angular_common__WEBPACK_IMPORTED_MODULE_1__["NgTemplateOutlet"], _angular_common__WEBPACK_IMPORTED_MODULE_1__["NgForOf"], _angular_common__WEBPACK_IMPORTED_MODULE_1__["NgStyle"], SelectDropdownComponent],
        styles: ["ng-select{display:inline-block;margin:0;position:relative;vertical-align:middle;width:100%}ng-select *{box-sizing:border-box}ng-select>div{border:1px solid #ddd;box-sizing:border-box;cursor:pointer;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;width:100%}ng-select>div.disabled{background-color:#eee;color:#aaa;cursor:default;pointer-events:none}ng-select>div>div.single{display:flex;height:30px;width:100%}ng-select>div>div.single>div.placeholder,ng-select>div>div.single>div.value{flex:1;line-height:30px;overflow:hidden;padding:0 10px;white-space:nowrap}ng-select>div>div.single>div.placeholder{color:#757575}ng-select>div>div.single>div.clear,ng-select>div>div.single>div.toggle{color:#aaa;line-height:30px;text-align:center;width:30px}ng-select>div>div.single>div.clear:hover,ng-select>div>div.single>div.toggle:hover{background-color:#ececec}ng-select>div>div.single>div.clear{font-size:18px}ng-select>div>div.single>div.toggle{font-size:14px}ng-select>div>div.multiple{display:flex;flex-flow:row wrap;height:100%;min-height:30px;padding:0 10px;width:100%}ng-select>div>div.multiple>div.option{background-color:#eee;border:1px solid #aaa;border-radius:4px;color:#333;cursor:default;display:inline-block;flex-shrink:0;font-size:14px;line-height:22px;margin:3px 5px 3px 0;padding:0 4px}ng-select>div>div.multiple>div.option span.deselect-option{color:#aaa;cursor:pointer;font-size:14px;height:20px;line-height:20px}ng-select>div>div.multiple>div.option span.deselect-option:hover{color:#555}ng-select>div>div.multiple input{background-color:transparent;border:none;cursor:pointer;height:30px;line-height:30px;padding:0}ng-select>div>div.multiple input:focus{outline:0}ng-select label{color:rgba(0,0,0,.38);display:block;font-size:13px;padding:4px 0}"],
        encapsulation: 2
      });
      /** @nocollapse */

      SelectComponent.ctorParameters = function () {
        return [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]
        }];
      };

      SelectComponent.propDecorators = {
        options: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        allowClear: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        disabled: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        multiple: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        noFilter: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        highlightColor: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        highlightTextColor: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        notFoundMsg: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        placeholder: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        filterPlaceholder: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        label: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        opened: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }],
        closed: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }],
        selected: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }],
        deselected: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }],
        focus: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }],
        blur: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }],
        noOptionsFound: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }],
        filterInputChanged: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }],
        selectionSpan: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"],
          args: ['selection', {
            "static": true
          }]
        }],
        dropdown: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"],
          args: ['dropdown', {
            "static": false
          }]
        }],
        filterInput: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"],
          args: ['filterInput', {
            "static": false
          }]
        }],
        optionTemplate: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ContentChild"],
          args: ['optionTemplate', {
            "static": false
          }]
        }],
        onWindowBlur: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
          args: ['window:blur']
        }],
        onWindowClick: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
          args: ['window:click']
        }],
        onWindowResize: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
          args: ['window:resize']
        }]
      };
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](SelectComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
          args: [{
            selector: 'ng-select',
            template: "<label\n    *ngIf=\"label !== ''\">\n    {{label}}\n</label>\n<div\n    #selection\n    [attr.tabindex]=\"disabled ? null : 0\"\n    [ngClass]=\"{'open': isOpen, 'focus': hasFocus, 'below': isBelow, 'above': !isBelow, 'disabled': disabled}\"\n    (click)=\"onSelectContainerClick($event)\"\n    (focus)=\"onSelectContainerFocus()\"\n    (keydown)=\"onSelectContainerKeydown($event)\">\n\n    <div class=\"single\"\n        *ngIf=\"!multiple\">\n        <div class=\"value\"\n            *ngIf=\"optionList.hasSelected\">\n            <ng-container *ngTemplateOutlet=\"optionTemplate; context:{option: optionList.selection[0].wrappedOption, onDeselectOptionClick: onDeselectOptionClick}\"></ng-container>\n            <span *ngIf=\"!optionTemplate\">{{optionList.selection[0].label}}</span>\n        </div>\n        <div class=\"placeholder\"\n            *ngIf=\"!optionList.hasSelected\">\n            {{placeholderView}}\n        </div>\n        <div class=\"clear\"\n            *ngIf=\"allowClear && optionList.hasSelected\"\n            (click)=\"onClearSelectionClick($event)\">\n            &#x2715;\n        </div>\n        <div class=\"toggle\"\n            *ngIf=\"isOpen\">\n            &#x25B2;\n        </div>\n        <div class=\"toggle\"\n            *ngIf=\"!isOpen\">\n            &#x25BC;\n        </div>\n    </div>\n\n    <div class=\"multiple\"\n        *ngIf=\"multiple\">\n        <div class=\"option\"\n            *ngFor=\"let option of optionList.selection\">\n            <span class=\"deselect-option\"\n                (click)=onDeselectOptionClick(option)>\n                &#x2715;\n            </span>\n            {{option.label}}\n        </div>\n        <div class=\"placeholder\"\n            *ngIf=\"!filterEnabled && !optionList.hasSelected\">\n            {{placeholderView}}\n        </div>\n        <input\n            *ngIf=\"filterEnabled\"\n            #filterInput\n            autocomplete=\"off\"\n            tabindex=\"-1\"\n            [placeholder]=\"placeholderView\"\n            [ngStyle]=\"{'width.px': filterInputWidth}\"\n            (input)=\"onFilterInput($event.target.value)\"\n            (keydown)=\"onMultipleFilterKeydown($event)\"\n            (focus)=\"onMultipleFilterFocus()\"/>\n    </div>\n\n</div>\n<select-dropdown\n    *ngIf=\"isOpen\"\n    #dropdown\n    [multiple]=\"multiple\"\n    [optionList]=\"optionList\"\n    [notFoundMsg]=\"notFoundMsg\"\n    [highlightColor]=\"highlightColor\"\n    [highlightTextColor]=\"highlightTextColor\"\n    [filterEnabled]=\"filterEnabled\"\n    [placeholder]=\"filterPlaceholder\"\n    [isBelow]=\"isBelow\"\n    [width]=\"width\"\n    [top]=\"top\"\n    [left]=\"left\"\n    [optionTemplate]=\"optionTemplate\"\n    (optionClicked)=\"onDropdownOptionClicked($event)\"\n    (optionsListClick)=\"onOptionsListClick()\"\n    (singleFilterClick)=\"onSingleFilterClick()\"\n    (singleFilterFocus)=\"onSingleFilterFocus()\"\n    (singleFilterInput)=\"onFilterInput($event)\"\n    (singleFilterKeydown)=\"onSingleFilterKeydown($event)\">\n</select-dropdown>\n",
            providers: [SELECT_VALUE_ACCESSOR],
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewEncapsulation"].None,
            styles: ["ng-select{display:inline-block;margin:0;position:relative;vertical-align:middle;width:100%}ng-select *{box-sizing:border-box}ng-select>div{border:1px solid #ddd;box-sizing:border-box;cursor:pointer;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;width:100%}ng-select>div.disabled{background-color:#eee;color:#aaa;cursor:default;pointer-events:none}ng-select>div>div.single{display:flex;height:30px;width:100%}ng-select>div>div.single>div.placeholder,ng-select>div>div.single>div.value{flex:1;line-height:30px;overflow:hidden;padding:0 10px;white-space:nowrap}ng-select>div>div.single>div.placeholder{color:#757575}ng-select>div>div.single>div.clear,ng-select>div>div.single>div.toggle{color:#aaa;line-height:30px;text-align:center;width:30px}ng-select>div>div.single>div.clear:hover,ng-select>div>div.single>div.toggle:hover{background-color:#ececec}ng-select>div>div.single>div.clear{font-size:18px}ng-select>div>div.single>div.toggle{font-size:14px}ng-select>div>div.multiple{display:flex;flex-flow:row wrap;height:100%;min-height:30px;padding:0 10px;width:100%}ng-select>div>div.multiple>div.option{background-color:#eee;border:1px solid #aaa;border-radius:4px;color:#333;cursor:default;display:inline-block;flex-shrink:0;font-size:14px;line-height:22px;margin:3px 5px 3px 0;padding:0 4px}ng-select>div>div.multiple>div.option span.deselect-option{color:#aaa;cursor:pointer;font-size:14px;height:20px;line-height:20px}ng-select>div>div.multiple>div.option span.deselect-option:hover{color:#555}ng-select>div>div.multiple input{background-color:transparent;border:none;cursor:pointer;height:30px;line-height:30px;padding:0}ng-select>div>div.multiple input:focus{outline:0}ng-select label{color:rgba(0,0,0,.38);display:block;font-size:13px;padding:4px 0}"]
          }]
        }], function () {
          return [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]
          }];
        }, {
          options: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          allowClear: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          disabled: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          multiple: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          noFilter: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          notFoundMsg: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          placeholder: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          filterPlaceholder: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          label: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          opened: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
          }],
          closed: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
          }],
          selected: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
          }],
          deselected: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
          }],
          focus: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
          }],
          blur: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
          }],
          noOptionsFound: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
          }],
          filterInputChanged: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
          }],

          /**
           * @return {?}
           */
          onWindowBlur: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
            args: ['window:blur']
          }],

          /**
           * @return {?}
           */
          onWindowClick: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
            args: ['window:click']
          }],

          /**
           * @return {?}
           */
          onWindowResize: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"],
            args: ['window:resize']
          }],
          highlightColor: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          highlightTextColor: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
          }],
          selectionSpan: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"],
            args: ['selection', {
              "static": true
            }]
          }],
          dropdown: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"],
            args: ['dropdown', {
              "static": false
            }]
          }],
          filterInput: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"],
            args: ['filterInput', {
              "static": false
            }]
          }],
          optionTemplate: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ContentChild"],
            args: ['optionTemplate', {
              "static": false
            }]
          }]
        });
      })();
      /**
       * @fileoverview added by tsickle
       * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
       */

      /**
       * @fileoverview added by tsickle
       * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
       */


      var SelectModule = function SelectModule() {
        _classCallCheck(this, SelectModule);
      };

      SelectModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
        type: SelectModule
      });
      SelectModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
        factory: function SelectModule_Factory(t) {
          return new (t || SelectModule)();
        },
        imports: [[_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"]]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](SelectModule, {
          declarations: function declarations() {
            return [SelectComponent, SelectDropdownComponent];
          },
          imports: function imports() {
            return [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"]];
          },
          exports: function exports() {
            return [SelectComponent];
          }
        });
      })();
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](SelectModule, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
          args: [{
            declarations: [SelectComponent, SelectDropdownComponent],
            exports: [SelectComponent],
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"]]
          }]
        }], null, null);
      })();
      /**
       * @fileoverview added by tsickle
       * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
       */

      /**
       * @fileoverview added by tsickle
       * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
       */
      //# sourceMappingURL=ng-select.js.map

      /***/

    },

    /***/
    "mkVx":
    /*!*************************************************************************************!*\
      !*** ./node_modules/angular-archwizard/__ivy_ngcc__/fesm2015/angular-archwizard.js ***!
      \*************************************************************************************/

    /*! exports provided: ArchwizardModule, BaseNavigationMode, CompletedStepDirective, ConfigurableNavigationMode, EnableBackLinksDirective, GoToStepDirective, MovingDirection, NavigationModeDirective, NextStepDirective, OptionalStepDirective, PreviousStepDirective, ResetWizardDirective, SelectedStepDirective, WizardCompletionStep, WizardCompletionStepComponent, WizardCompletionStepDirective, WizardComponent, WizardNavigationBarComponent, WizardStep, WizardStepComponent, WizardStepDirective, WizardStepSymbolDirective, WizardStepTitleDirective, isStepId, isStepIndex, isStepOffset */

    /***/
    function mkVx(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ArchwizardModule", function () {
        return ArchwizardModule;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "BaseNavigationMode", function () {
        return BaseNavigationMode;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CompletedStepDirective", function () {
        return CompletedStepDirective;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ConfigurableNavigationMode", function () {
        return ConfigurableNavigationMode;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "EnableBackLinksDirective", function () {
        return EnableBackLinksDirective;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "GoToStepDirective", function () {
        return GoToStepDirective;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "MovingDirection", function () {
        return MovingDirection;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "NavigationModeDirective", function () {
        return NavigationModeDirective;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "NextStepDirective", function () {
        return NextStepDirective;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "OptionalStepDirective", function () {
        return OptionalStepDirective;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PreviousStepDirective", function () {
        return PreviousStepDirective;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ResetWizardDirective", function () {
        return ResetWizardDirective;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SelectedStepDirective", function () {
        return SelectedStepDirective;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "WizardCompletionStep", function () {
        return WizardCompletionStep;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "WizardCompletionStepComponent", function () {
        return WizardCompletionStepComponent;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "WizardCompletionStepDirective", function () {
        return WizardCompletionStepDirective;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "WizardComponent", function () {
        return WizardComponent;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "WizardNavigationBarComponent", function () {
        return WizardNavigationBarComponent;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "WizardStep", function () {
        return WizardStep;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "WizardStepComponent", function () {
        return WizardStepComponent;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "WizardStepDirective", function () {
        return WizardStepDirective;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "WizardStepSymbolDirective", function () {
        return WizardStepSymbolDirective;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "WizardStepTitleDirective", function () {
        return WizardStepTitleDirective;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "isStepId", function () {
        return isStepId;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "isStepIndex", function () {
        return isStepIndex;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "isStepOffset", function () {
        return isStepOffset;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "dpFU");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /**
       * The `awWizardStepSymbol` directive can be used as an alternative to the `navigationSymbol` input of a [[WizardStep]]
       * to define the step symbol inside the navigation bar.  This way step symbol may contain arbitrary content.
       *
       * ### Syntax
       *
       * ```html
       * <ng-template awWizardStepSymbol>
       *     ...
       * </ng-template>
       * ```
       */


      var _c0 = ["*"];

      var _c1 = function _c1(a0, a1, a2, a3, a4, a5, a6) {
        return {
          "vertical": a0,
          "horizontal": a1,
          "small": a2,
          "large-filled": a3,
          "large-filled-symbols": a4,
          "large-empty": a5,
          "large-empty-symbols": a6
        };
      };

      function WizardComponent_aw_wizard_navigation_bar_0_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "aw-wizard-navigation-bar", 2);
        }

        if (rf & 2) {
          var ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("direction", ctx_r0.navBarDirection)("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction7"](2, _c1, ctx_r0.navBarLocation == "left", ctx_r0.navBarLocation == "top", ctx_r0.navBarLayout == "small", ctx_r0.navBarLayout == "large-filled", ctx_r0.navBarLayout == "large-filled-symbols", ctx_r0.navBarLayout == "large-empty", ctx_r0.navBarLayout == "large-empty-symbols"));
        }
      }

      function WizardComponent_aw_wizard_navigation_bar_3_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "aw-wizard-navigation-bar", 2);
        }

        if (rf & 2) {
          var ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("direction", ctx_r1.navBarDirection)("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction7"](2, _c1, ctx_r1.navBarLocation == "right", ctx_r1.navBarLocation == "bottom", ctx_r1.navBarLayout == "small", ctx_r1.navBarLayout == "large-filled", ctx_r1.navBarLayout == "large-filled-symbols", ctx_r1.navBarLayout == "large-empty", ctx_r1.navBarLayout == "large-empty-symbols"));
        }
      }

      var _c2 = function _c2(a1, a2) {
        return {
          "wizard-steps": true,
          "vertical": a1,
          "horizontal": a2
        };
      };

      var _c3 = function _c3(a0) {
        return {
          wizardStep: a0
        };
      };

      function WizardNavigationBarComponent_li_1_ng_container_3_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainer"](0, 7);
        }

        if (rf & 2) {
          var step_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngTemplateOutlet", step_r1.stepTitleTemplate.templateRef)("ngTemplateOutletContext", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](2, _c3, step_r1));
        }
      }

      function WizardNavigationBarComponent_li_1_ng_container_4_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainerStart"](0);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainerEnd"]();
        }

        if (rf & 2) {
          var step_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](step_r1.stepTitle);
        }
      }

      function WizardNavigationBarComponent_li_1_ng_container_6_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainer"](0, 7);
        }

        if (rf & 2) {
          var step_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngTemplateOutlet", step_r1.stepSymbolTemplate.templateRef)("ngTemplateOutletContext", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](2, _c3, step_r1));
        }
      }

      function WizardNavigationBarComponent_li_1_ng_container_7_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainerStart"](0);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainerEnd"]();
        }

        if (rf & 2) {
          var step_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]().$implicit;

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](step_r1.navigationSymbol.symbol);
        }
      }

      var _c4 = function _c4(a0, a1, a2, a3, a4, a5) {
        return {
          "current": a0,
          "editing": a1,
          "done": a2,
          "optional": a3,
          "completed": a4,
          "navigable": a5
        };
      };

      var _c5 = function _c5(a0) {
        return {
          "font-family": a0
        };
      };

      function WizardNavigationBarComponent_li_1_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "li", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "a", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "div", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, WizardNavigationBarComponent_li_1_ng_container_3_Template, 1, 4, "ng-container", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](4, WizardNavigationBarComponent_li_1_ng_container_4_Template, 2, 1, "ng-container", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "div", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](6, WizardNavigationBarComponent_li_1_ng_container_6_Template, 1, 4, "ng-container", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](7, WizardNavigationBarComponent_li_1_ng_container_7_Template, 2, 1, "ng-container", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var step_r1 = ctx.$implicit;

          var ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction6"](8, _c4, ctx_r0.isCurrent(step_r1), ctx_r0.isEditing(step_r1), ctx_r0.isDone(step_r1), ctx_r0.isOptional(step_r1), ctx_r0.isCompleted(step_r1), ctx_r0.isNavigable(step_r1)));

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵattribute"]("id", step_r1.stepId);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("awGoToStep", step_r1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", step_r1.stepTitleTemplate);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", !step_r1.stepTitleTemplate);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngStyle", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](15, _c5, step_r1.stepSymbolTemplate ? "" : step_r1.navigationSymbol.fontFamily));

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", step_r1.stepSymbolTemplate);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", !step_r1.stepSymbolTemplate);
        }
      }

      var WizardStepSymbolDirective =
      /**
       * Constructor
       *
       * @param templateRef A reference to the content of the `ng-template` that contains this [[WizardStepSymbolDirective]]
       */
      function WizardStepSymbolDirective(templateRef) {
        _classCallCheck(this, WizardStepSymbolDirective);

        this.templateRef = templateRef;
      };

      WizardStepSymbolDirective.ɵfac = function WizardStepSymbolDirective_Factory(t) {
        return new (t || WizardStepSymbolDirective)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["TemplateRef"]));
      };

      WizardStepSymbolDirective.ɵdir = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineDirective"]({
        type: WizardStepSymbolDirective,
        selectors: [["ng-template", "awStepSymbol", ""], ["ng-template", "awWizardStepSymbol", ""]]
      });

      WizardStepSymbolDirective.ctorParameters = function () {
        return [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["TemplateRef"]
        }];
      };

      WizardStepSymbolDirective = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["TemplateRef"]])], WizardStepSymbolDirective);
      /**
       * The `awWizardStepTitle` directive can be used as an alternative to the `stepTitle` input of a [[WizardStep]]
       * to define the content of a step title inside the navigation bar.
       * This step title can be freely created and can contain more than only plain text
       *
       * ### Syntax
       *
       * ```html
       * <ng-template awWizardStepTitle>
       *     ...
       * </ng-template>
       * ```
       *
       * @author Marc Arndt
       */

      var WizardStepTitleDirective =
      /**
       * Constructor
       *
       * @param templateRef A reference to the content of the `ng-template` that contains this [[WizardStepTitleDirective]]
       */
      function WizardStepTitleDirective(templateRef) {
        _classCallCheck(this, WizardStepTitleDirective);

        this.templateRef = templateRef;
      };

      WizardStepTitleDirective.ɵfac = function WizardStepTitleDirective_Factory(t) {
        return new (t || WizardStepTitleDirective)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_1__["TemplateRef"]));
      };

      WizardStepTitleDirective.ɵdir = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineDirective"]({
        type: WizardStepTitleDirective,
        selectors: [["ng-template", "awStepTitle", ""], ["ng-template", "awWizardStepTitle", ""]]
      });

      WizardStepTitleDirective.ctorParameters = function () {
        return [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["TemplateRef"]
        }];
      };

      WizardStepTitleDirective = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["TemplateRef"]])], WizardStepTitleDirective);
      var WizardStep_1;
      /**
       * Basic functionality every type of wizard step needs to provide
       *
       * @author Marc Arndt
       */

      var WizardStep = WizardStep_1 = /*#__PURE__*/function () {
        function WizardStep() {
          _classCallCheck(this, WizardStep);

          /**
           * A symbol property, which contains an optional symbol for the step inside the navigation bar.
           * Takes effect when `stepSymbolTemplate` is not defined or null.
           */
          this.navigationSymbol = {
            symbol: ''
          };
          /**
           * A boolean describing if the wizard step is currently selected
           */

          this.selected = false;
          /**
           * A boolean describing if the wizard step has been completed
           */

          this.completed = false;
          /**
           * A boolean describing if the wizard step is shown as completed when the wizard is presented to the user
           *
           * Users will typically use `CompletedStepDirective` to set this flag
           */

          this.initiallyCompleted = false;
          /**
           * A boolean describing if the wizard step is being edited after being competed
           *
           * This flag can only be true when `selected` is true.
           */

          this.editing = false;
          /**
           * A boolean describing, if the wizard step should be selected by default, i.e. after the wizard has been initialized as the initial step
           */

          this.defaultSelected = false;
          /**
           * A boolean describing if the wizard step is an optional step
           */

          this.optional = false;
          /**
           * A function or boolean deciding, if this step can be entered
           */

          this.canEnter = true;
          /**
           * A function or boolean deciding, if this step can be exited
           */

          this.canExit = true;
          /**
           * This [[EventEmitter]] is called when the step is entered.
           * The bound method should be used to do initialization work.
           */

          this.stepEnter = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
          /**
           * This [[EventEmitter]] is called when the step is exited.
           * The bound method can be used to do cleanup work.
           */

          this.stepExit = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        }
        /**
         * Returns true if this wizard step should be visible to the user.
         * If the step should be visible to the user false is returned, otherwise true
         */


        _createClass(WizardStep, [{
          key: "enter",

          /**
           * A function called when the step is entered
           *
           * @param direction The direction in which the step is entered
           */
          value: function enter(direction) {
            this.stepEnter.emit(direction);
          }
          /**
           * A function called when the step is exited
           *
           * @param direction The direction in which the step is exited
           */

        }, {
          key: "exit",
          value: function exit(direction) {
            this.stepExit.emit(direction);
          }
          /**
           * This method returns true, if this wizard step can be entered from the given direction.
           * Because this method depends on the value `canEnter`, it will throw an error, if `canEnter` is neither a boolean
           * nor a function.
           *
           * @param direction The direction in which this step should be entered
           * @returns A [[Promise]] containing `true`, if the step can be entered in the given direction, false otherwise
           * @throws An `Error` is thrown if `anEnter` is neither a function nor a boolean
           */

        }, {
          key: "canEnterStep",
          value: function canEnterStep(direction) {
            return WizardStep_1.canTransitionStep(this.canEnter, direction);
          }
          /**
           * This method returns true, if this wizard step can be exited into given direction.
           * Because this method depends on the value `canExit`, it will throw an error, if `canExit` is neither a boolean
           * nor a function.
           *
           * @param direction The direction in which this step should be left
           * @returns A [[Promise]] containing `true`, if the step can be exited in the given direction, false otherwise
           * @throws An `Error` is thrown if `canExit` is neither a function nor a boolean
           */

        }, {
          key: "canExitStep",
          value: function canExitStep(direction) {
            return WizardStep_1.canTransitionStep(this.canExit, direction);
          }
        }, {
          key: "hidden",
          get: function get() {
            return !this.selected;
          }
          /**
           * This method returns true, if this wizard step can be transitioned with a given direction.
           * Transitioned in this case means either entered or exited, depending on the given `condition` parameter.
           *
           * @param condition A condition variable, deciding if the step can be transitioned
           * @param direction The direction in which this step should be transitioned
           * @returns A [[Promise]] containing `true`, if this step can transitioned in the given direction
           * @throws An `Error` is thrown if `condition` is neither a function nor a boolean
           */

        }], [{
          key: "canTransitionStep",
          value: function canTransitionStep(condition, direction) {
            if (typeof condition === typeof true) {
              return Promise.resolve(condition);
            } else if (condition instanceof Function) {
              return Promise.resolve(condition(direction));
            } else {
              return Promise.reject(new Error("Input value '".concat(condition, "' is neither a boolean nor a function")));
            }
          }
        }]);

        return WizardStep;
      }();

      WizardStep.ɵfac = function WizardStep_Factory(t) {
        return new (t || WizardStep)();
      };

      WizardStep.ɵdir = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineDirective"]({
        type: WizardStep,
        contentQueries: function WizardStep_ContentQueries(rf, ctx, dirIndex) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵcontentQuery"](dirIndex, WizardStepTitleDirective, true);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵcontentQuery"](dirIndex, WizardStepSymbolDirective, true);
          }

          if (rf & 2) {
            var _t;

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵloadQuery"]()) && (ctx.stepTitleTemplate = _t.first);
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵloadQuery"]()) && (ctx.stepSymbolTemplate = _t.first);
          }
        },
        hostVars: 1,
        hostBindings: function WizardStep_HostBindings(rf, ctx) {
          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵhostProperty"]("hidden", ctx.hidden);
          }
        },
        inputs: {
          navigationSymbol: "navigationSymbol",
          canEnter: "canEnter",
          canExit: "canExit",
          stepId: "stepId",
          stepTitle: "stepTitle"
        },
        outputs: {
          stepEnter: "stepEnter",
          stepExit: "stepExit"
        }
      });
      Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ContentChild"])(WizardStepTitleDirective), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", WizardStepTitleDirective)], WizardStep.prototype, "stepTitleTemplate", void 0);
      Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ContentChild"])(WizardStepSymbolDirective), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", WizardStepSymbolDirective)], WizardStep.prototype, "stepSymbolTemplate", void 0);
      Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", String)], WizardStep.prototype, "stepId", void 0);
      Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", String)], WizardStep.prototype, "stepTitle", void 0);
      Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Object)], WizardStep.prototype, "navigationSymbol", void 0);
      Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Object)], WizardStep.prototype, "canEnter", void 0);
      Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Object)], WizardStep.prototype, "canExit", void 0);
      Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])], WizardStep.prototype, "stepEnter", void 0);
      Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])], WizardStep.prototype, "stepExit", void 0);
      Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostBinding"])('hidden'), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Boolean), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])], WizardStep.prototype, "hidden", null);
      /**
       * Basic functionality every wizard completion step needs to provide
       *
       * @author Marc Arndt
       */

      var WizardCompletionStep = /*#__PURE__*/function (_WizardStep) {
        _inherits(WizardCompletionStep, _WizardStep);

        var _super = _createSuper(WizardCompletionStep);

        function WizardCompletionStep() {
          var _this10;

          _classCallCheck(this, WizardCompletionStep);

          _this10 = _super.apply(this, arguments);
          /**
           * @inheritDoc
           */

          _this10.stepExit = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
          /**
           * @inheritDoc
           */

          _this10.canExit = false;
          return _this10;
        }
        /**
         * @inheritDoc
         */


        _createClass(WizardCompletionStep, [{
          key: "enter",
          value: function enter(direction) {
            this.completed = true;
            this.stepEnter.emit(direction);
          }
          /**
           * @inheritDoc
           */

        }, {
          key: "exit",
          value: function exit(direction) {
            // set this completion step as incomplete (unless it happens to be initiallyCompleted)
            this.completed = this.initiallyCompleted;
            this.stepExit.emit(direction);
          }
        }]);

        return WizardCompletionStep;
      }(WizardStep);

      WizardCompletionStep.ɵfac = function WizardCompletionStep_Factory(t) {
        return ɵWizardCompletionStep_BaseFactory(t || WizardCompletionStep);
      };

      WizardCompletionStep.ɵdir = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineDirective"]({
        type: WizardCompletionStep,
        features: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵInheritDefinitionFeature"]]
      });
      var WizardCompletionStepComponent_1;
      /**
       * The `aw-wizard-completion-step` component can be used to define a completion/success step at the end of your wizard
       * After a `aw-wizard-completion-step` has been entered, it has the characteristic that the user is blocked from
       * leaving it again to a previous step.
       * In addition entering a `aw-wizard-completion-step` automatically sets the `aw-wizard` and all steps inside the `aw-wizard`
       * as completed.
       *
       * ### Syntax
       *
       * ```html
       * <aw-wizard-completion-step [stepTitle]="title of the wizard step"
       *    [navigationSymbol]="{ symbol: 'navigation symbol', fontFamily: 'navigation symbol font family' }"
       *    (stepEnter)="event emitter to be called when the wizard step is entered"
       *    (stepExit)="event emitter to be called when the wizard step is exited">
       *    ...
       * </aw-wizard-completion-step>
       * ```
       *
       * ### Example
       *
       * ```html
       * <aw-wizard-completion-step stepTitle="Step 1" [navigationSymbol]="{ symbol: '1' }">
       *    ...
       * </aw-wizard-completion-step>
       * ```
       *
       * With a navigation symbol from the `font-awesome` font:
       *
       * ```html
       * <aw-wizard-completion-step stepTitle="Step 1" [navigationSymbol]="{ symbol: '&#xf1ba;', fontFamily: 'FontAwesome' }">
       *    ...
       * </aw-wizard-completion-step>
       * ```
       *
       * @author Marc Arndt
       */

      var WizardCompletionStepComponent = WizardCompletionStepComponent_1 = /*#__PURE__*/function (_WizardCompletionStep) {
        _inherits(WizardCompletionStepComponent, _WizardCompletionStep);

        var _super2 = _createSuper(WizardCompletionStepComponent);

        function WizardCompletionStepComponent() {
          _classCallCheck(this, WizardCompletionStepComponent);

          return _super2.apply(this, arguments);
        }

        return WizardCompletionStepComponent;
      }(WizardCompletionStep);

      WizardCompletionStepComponent.ɵfac = function WizardCompletionStepComponent_Factory(t) {
        return ɵWizardCompletionStepComponent_BaseFactory(t || WizardCompletionStepComponent);
      };

      WizardCompletionStepComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({
        type: WizardCompletionStepComponent,
        selectors: [["aw-wizard-completion-step"]],
        features: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵProvidersFeature"]([{
          provide: WizardStep,
          useExisting: Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["forwardRef"])(function () {
            return WizardCompletionStepComponent_1;
          })
        }, {
          provide: WizardCompletionStep,
          useExisting: Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["forwardRef"])(function () {
            return WizardCompletionStepComponent_1;
          })
        }]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵInheritDefinitionFeature"]],
        ngContentSelectors: _c0,
        decls: 1,
        vars: 0,
        template: function WizardCompletionStepComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵprojectionDef"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵprojection"](0);
          }
        },
        encapsulation: 2
      });
      /**
       * The direction in which a step transition was made
       *
       * @author Marc Arndt
       */

      /**
       * This enum contains the different possible moving directions in which a wizard can be traversed
       *
       * @author Marc Arndt
       */

      var MovingDirection;

      (function (MovingDirection) {
        /**
         * A forward step transition
         */
        MovingDirection[MovingDirection["Forwards"] = 0] = "Forwards";
        /**
         * A backward step transition
         */

        MovingDirection[MovingDirection["Backwards"] = 1] = "Backwards";
        /**
         * No step transition was done
         */

        MovingDirection[MovingDirection["Stay"] = 2] = "Stay";
      })(MovingDirection || (MovingDirection = {}));
      /**
       * Base implementation of [[NavigationMode]]
       *
       * Note: Built-in [[NavigationMode]] classes should be stateless, allowing the library user to easily create
       * an instance of a particular [[NavigationMode]] class and pass it to `<aw-wizard [navigationMode]="...">`.
       *
       * @author Marc Arndt
       */


      var BaseNavigationMode = /*#__PURE__*/function () {
        function BaseNavigationMode() {
          _classCallCheck(this, BaseNavigationMode);
        }

        _createClass(BaseNavigationMode, [{
          key: "canGoToStep",

          /**
           * Checks, whether a wizard step, as defined by the given destination index, can be transitioned to.
           *
           * This method controls navigation by [[goToStep]], [[goToPreviousStep]], and [[goToNextStep]] directives.
           * Navigation by navigation bar is governed by [[isNavigable]].
           *
           * In this implementation, a destination wizard step can be entered if:
           * - it exists
           * - the current step can be exited in the direction of the destination step
           * - the destination step can be entered in the direction from the current step
           *
           * Subclasses can impose additional restrictions, see [[canTransitionToStep]].
           *
           * @param wizard The wizard component to operate on
           * @param destinationIndex The index of the destination step
           * @returns A [[Promise]] containing `true`, if the destination step can be transitioned to and `false` otherwise
           */
          value: function canGoToStep(wizard, destinationIndex) {
            var _this11 = this;

            var hasStep = wizard.hasStep(destinationIndex);
            var movingDirection = wizard.getMovingDirection(destinationIndex);

            var canExitCurrentStep = function canExitCurrentStep(previous) {
              return previous && wizard.currentStep.canExitStep(movingDirection);
            };

            var canEnterDestinationStep = function canEnterDestinationStep(previous) {
              return previous && wizard.getStepAtIndex(destinationIndex).canEnterStep(movingDirection);
            };

            var canTransitionToStep = function canTransitionToStep(previous) {
              return previous && _this11.canTransitionToStep(wizard, destinationIndex);
            };

            return Promise.resolve(hasStep).then(canTransitionToStep) // Apply user-defined checks at the end.  They can involve user interaction
            // which is better to be avoided if navigation mode does not actually allow the transition
            // (`canTransitionToStep` returns `false`).
            .then(canExitCurrentStep).then(canEnterDestinationStep);
          }
          /**
           * Imposes additional restrictions for `canGoToStep` in current navigation mode.
           *
           * The base implementation allows transition iff the given step is navigable from the navigation bar (see `isNavigable`).
           * However, in some navigation modes `canTransitionToStep` can be more relaxed to allow navigation to certain steps
           * by previous/next buttons, but not using the navigation bar.
           *
           * @param wizard The wizard component to operate on
           * @param destinationIndex The index of the destination step
           * @returns `true`, if the destination step can be transitioned to and `false` otherwise
           */

        }, {
          key: "canTransitionToStep",
          value: function canTransitionToStep(wizard, destinationIndex) {
            return this.isNavigable(wizard, destinationIndex);
          }
          /**
           * Tries to transition to the wizard step, as denoted by the given destination index.
           *
           * When entering the destination step, the following actions are done:
           * - the old current step is set as completed
           * - the old current step is set as unselected
           * - the old current step is exited
           * - the destination step is set as selected
           * - the destination step is entered
           *
           * When the destination step couldn't be entered, the following actions are done:
           * - the current step is exited and entered in the direction `MovingDirection.Stay`
           *
           * @param wizard The wizard component to operate on
           * @param destinationIndex The index of the destination wizard step, which should be entered
           * @param preFinalize An event emitter, to be called before the step has been transitioned
           * @param postFinalize An event emitter, to be called after the step has been transitioned
           */

        }, {
          key: "goToStep",
          value: function goToStep(wizard, destinationIndex, preFinalize, postFinalize) {
            var _this12 = this;

            this.canGoToStep(wizard, destinationIndex).then(function (navigationAllowed) {
              if (navigationAllowed) {
                // the current step can be exited in the given direction
                var movingDirection = wizard.getMovingDirection(destinationIndex);
                /* istanbul ignore if */

                if (preFinalize) {
                  preFinalize.emit();
                } // leave current step


                wizard.currentStep.completed = true;
                wizard.currentStep.exit(movingDirection);
                wizard.currentStep.editing = false;
                wizard.currentStep.selected = false;

                _this12.transition(wizard, destinationIndex); // remember if the next step is already completed before entering it to properly set `editing` flag


                var wasCompleted = wizard.completed || wizard.currentStep.completed; // go to next step

                wizard.currentStep.enter(movingDirection);
                wizard.currentStep.selected = true;

                if (wasCompleted) {
                  wizard.currentStep.editing = true;
                }
                /* istanbul ignore if */


                if (postFinalize) {
                  postFinalize.emit();
                }
              } else {
                // if the current step can't be left, reenter the current step
                wizard.currentStep.exit(MovingDirection.Stay);
                wizard.currentStep.enter(MovingDirection.Stay);
              }
            });
          }
          /**
           * Transitions the wizard to the given step index.
           *
           * Can perform additional actions in particular navigation mode implementations.
           *
           * @param wizard The wizard component to operate on
           * @param destinationIndex The index of the destination wizard step
           */

        }, {
          key: "transition",
          value: function transition(wizard, destinationIndex) {
            wizard.currentStepIndex = destinationIndex;
          }
          /**
           * Resets the state of this wizard.
           *
           * A reset transitions the wizard automatically to the first step and sets all steps as incomplete.
           * In addition the whole wizard is set as incomplete.
           *
           * @param wizard The wizard component to operate on
           */

        }, {
          key: "reset",
          value: function reset(wizard) {
            this.ensureCanReset(wizard); // reset the step internal state

            wizard.wizardSteps.forEach(function (step) {
              step.completed = step.initiallyCompleted;
              step.selected = false;
              step.editing = false;
            }); // set the first step as the current step

            wizard.currentStepIndex = wizard.defaultStepIndex;
            wizard.currentStep.selected = true;
            wizard.currentStep.enter(MovingDirection.Forwards);
          }
          /**
           * Checks if wizard configuration allows to perform reset.
           *
           * A check failure is indicated by throwing an `Error` with the message discribing the discovered misconfiguration issue.
           *
           * Can include additional checks in particular navigation mode implementations.
           *
           * @param wizard The wizard component to operate on
           * @throws An `Error` is thrown, if a micconfiguration issue is discovered.
           */

        }, {
          key: "ensureCanReset",
          value: function ensureCanReset(wizard) {
            // the wizard doesn't contain a step with the default step index
            if (!wizard.hasStep(wizard.defaultStepIndex)) {
              throw new Error("The wizard doesn't contain a step with index ".concat(wizard.defaultStepIndex));
            }
          }
        }]);

        return BaseNavigationMode;
      }();
      /**
       * The default navigation mode used by [[WizardComponent]] and [[NavigationModeDirective]].
       *
       * It is parameterized with two navigation policies passed to constructor:
       *
       * - [[navigateBackward]] policy controls whether wizard steps before the current step are navigable:
       *
       *   - `"deny"` -- the steps are not navigable
       *   - `"allow"` -- the steps are navigable
       *   - If the corresponding constructor argument is omitted or is `null` or `undefined`,
       *     then the default value is applied which is `"deny"`
       *
       * - [[navigateForward]] policy controls whether wizard steps after the current step are navigable:
       *
       *   - `"deny"` -- the steps are not navigable
       *   - `"allow"` -- the steps are navigable
       *   - `"visited"` -- a step is navigable iff it was already visited before
       *   - If the corresponding constructor argument is omitted or is `null` or `undefined`,
       *     then the default value is applied which is `"allow"`
       */


      var ConfigurableNavigationMode = /*#__PURE__*/function (_BaseNavigationMode) {
        _inherits(ConfigurableNavigationMode, _BaseNavigationMode);

        var _super3 = _createSuper(ConfigurableNavigationMode);

        /**
         * Constructor
         *
         * @param navigateBackward Controls whether wizard steps before the current step are navigable
         * @param navigateForward Controls whether wizard steps before the current step are navigable
         */
        function ConfigurableNavigationMode() {
          var _this13;

          var navigateBackward = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
          var navigateForward = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

          _classCallCheck(this, ConfigurableNavigationMode);

          _this13 = _super3.call(this);
          _this13.navigateBackward = navigateBackward;
          _this13.navigateForward = navigateForward;
          _this13.navigateBackward = _this13.navigateBackward || 'allow';
          _this13.navigateForward = _this13.navigateForward || 'deny';
          return _this13;
        }
        /**
         * @inheritDoc
         */


        _createClass(ConfigurableNavigationMode, [{
          key: "canTransitionToStep",
          value: function canTransitionToStep(wizard, destinationIndex) {
            // if the destination step can be navigated to using the navigation bar,
            // it should be accessible with [goToStep] as well
            if (this.isNavigable(wizard, destinationIndex)) {
              return true;
            } // navigation with [goToStep] is permitted if all previous steps
            // to the destination step have been completed or are optional


            return wizard.wizardSteps.filter(function (step, index) {
              return index < destinationIndex && index !== wizard.currentStepIndex;
            }).every(function (step) {
              return step.completed || step.optional;
            });
          }
          /**
           * @inheritDoc
           */

        }, {
          key: "transition",
          value: function transition(wizard, destinationIndex) {
            if (this.navigateForward === 'deny') {
              // set all steps after the destination step to incomplete
              wizard.wizardSteps.filter(function (step, index) {
                return wizard.currentStepIndex > destinationIndex && index > destinationIndex;
              }).forEach(function (step) {
                return step.completed = false;
              });
            }

            _get(_getPrototypeOf(ConfigurableNavigationMode.prototype), "transition", this).call(this, wizard, destinationIndex);
          }
          /**
           * @inheritDoc
           */

        }, {
          key: "isNavigable",
          value: function isNavigable(wizard, destinationIndex) {
            // Check if the destination step can be navigated to
            var destinationStep = wizard.getStepAtIndex(destinationIndex);

            if (destinationStep instanceof WizardCompletionStep) {
              // A completion step can only be entered, if all previous steps have been completed, are optional, or selected
              var previousStepsCompleted = wizard.wizardSteps.filter(function (step, index) {
                return index < destinationIndex;
              }).every(function (step) {
                return step.completed || step.optional || step.selected;
              });

              if (!previousStepsCompleted) {
                return false;
              }
            } // Apply navigation pocicies


            if (destinationIndex < wizard.currentStepIndex) {
              // If the destination step is before current, apply the `navigateBackward` policy
              switch (this.navigateBackward) {
                case 'allow':
                  return true;

                case 'deny':
                  return false;

                default:
                  throw new Error("Invalid value for navigateBackward: ".concat(this.navigateBackward));
              }
            } else if (destinationIndex > wizard.currentStepIndex) {
              // If the destination step is after current, apply the `navigateForward` policy
              switch (this.navigateForward) {
                case 'allow':
                  return true;

                case 'deny':
                  return false;

                case 'visited':
                  return destinationStep.completed;

                default:
                  throw new Error("Invalid value for navigateForward: ".concat(this.navigateForward));
              }
            } else {
              // Re-entering the current step is not allowed
              return false;
            }
          }
          /**
           * @inheritDoc
           */

        }, {
          key: "ensureCanReset",
          value: function ensureCanReset(wizard) {
            _get(_getPrototypeOf(ConfigurableNavigationMode.prototype), "ensureCanReset", this).call(this, wizard); // the default step is a completion step and the wizard contains more than one step


            var defaultWizardStep = wizard.getStepAtIndex(wizard.defaultStepIndex);
            var defaultCompletionStep = defaultWizardStep instanceof WizardCompletionStep;

            if (defaultCompletionStep && wizard.wizardSteps.length !== 1) {
              throw new Error("The default step index ".concat(wizard.defaultStepIndex, " references a completion step"));
            }
          }
        }]);

        return ConfigurableNavigationMode;
      }(BaseNavigationMode);
      /**
       * The `aw-wizard` component defines the root component of a wizard.
       * Through the setting of input parameters for the `aw-wizard` component it's possible to change the location and size
       * of its navigation bar.
       *
       * ### Syntax
       * ```html
       * <aw-wizard [navBarLocation]="location of navigation bar" [navBarLayout]="layout of navigation bar">
       *     ...
       * </aw-wizard>
       * ```
       *
       * ### Example
       *
       * Without completion step:
       *
       * ```html
       * <aw-wizard navBarLocation="top" navBarLayout="small">
       *     <aw-wizard-step>...</aw-wizard-step>
       *     <aw-wizard-step>...</aw-wizard-step>
       * </aw-wizard>
       * ```
       *
       * With completion step:
       *
       * ```html
       * <aw-wizard navBarLocation="top" navBarLayout="small">
       *     <aw-wizard-step>...</aw-wizard-step>
       *     <aw-wizard-step>...</aw-wizard-step>
       *     <aw-wizard-completion-step>...</aw-wizard-completion-step>
       * </aw-wizard>
       * ```
       *
       * @author Marc Arndt
       */


      var WizardComponent = /*#__PURE__*/function () {
        /**
         * Constructor
         */
        function WizardComponent() {
          _classCallCheck(this, WizardComponent);

          /**
           * The location of the navigation bar inside the wizard.
           * This location can be either top, bottom, left or right
           */
          this.navBarLocation = 'top';
          /**
           * The layout of the navigation bar inside the wizard.
           * The layout can be either small, large-filled, large-empty or large-symbols
           */

          this.navBarLayout = 'small';
          /**
           * The direction in which the steps inside the navigation bar should be shown.
           * The direction can be either `left-to-right` or `right-to-left`
           */

          this.navBarDirection = 'left-to-right';
          this._defaultStepIndex = 0;
          /**
           * True, if the navigation bar shouldn't be used for navigating
           */

          this.disableNavigationBar = false;
          /**
           * The navigation mode used to navigate inside the wizard
           *
           * For outside access, use the [[navigation]] getter.
           */

          this._navigation = new ConfigurableNavigationMode();
          /**
           * An array representation of all wizard steps belonging to this model
           *
           * For outside access, use the [[wizardSteps]] getter.
           */

          this._wizardSteps = [];
          /**
           * The index of the currently visible and selected step inside the wizardSteps QueryList.
           * If this wizard contains no steps, currentStepIndex is -1
           *
           * Note: Do not modify this field directly.  Instead, use navigation methods:
           * [[goToStep]], [[goToPreviousStep]], [[goToNextStep]].
           */

          this.currentStepIndex = -1;
        }
        /**
         * The initially selected step, represented by its index
         * Beware: This initial default is only used if no wizard step has been enhanced with the `selected` directive
         */


        _createClass(WizardComponent, [{
          key: "ngAfterContentInit",

          /**
           * Initialization work
           */
          value: function ngAfterContentInit() {
            var _this14 = this;

            // add a subscriber to the wizard steps QueryList to listen to changes in the DOM
            this.wizardStepsQueryList.changes.subscribe(function (changedWizardSteps) {
              _this14.updateWizardSteps(changedWizardSteps.toArray());
            }); // initialize the model

            this.updateWizardSteps(this.wizardStepsQueryList.toArray()); // finally reset the whole wizard component

            setTimeout(function () {
              return _this14.reset();
            });
          }
          /**
           * The WizardStep object belonging to the currently visible and selected step.
           * The currentStep is always the currently selected wizard step.
           * The currentStep can be either completed, if it was visited earlier,
           * or not completed, if it is visited for the first time or its state is currently out of date.
           *
           * If this wizard contains no steps, currentStep is null
           */

        }, {
          key: "updateWizardSteps",

          /**
           * Updates the wizard steps to the new array
           *
           * @param wizardSteps The updated wizard steps
           */
          value: function updateWizardSteps(wizardSteps) {
            // the wizard is currently not in the initialization phase
            if (this.wizardSteps.length > 0 && this.currentStepIndex > -1) {
              this.currentStepIndex = wizardSteps.indexOf(this.wizardSteps[this.currentStepIndex]);
            }

            this._wizardSteps = wizardSteps;
          }
          /**
           * The navigation mode used to navigate inside the wizard
           */

        }, {
          key: "hasStep",

          /**
           * Checks if a given index `stepIndex` is inside the range of possible wizard steps inside this wizard
           *
           * @param stepIndex The to be checked index of a step inside this wizard
           * @returns True if the given `stepIndex` is contained inside this wizard, false otherwise
           */
          value: function hasStep(stepIndex) {
            return this.wizardSteps.length > 0 && 0 <= stepIndex && stepIndex < this.wizardSteps.length;
          }
          /**
           * Checks if this wizard has a previous step, compared to the current step
           *
           * @returns True if this wizard has a previous step before the current step
           */

        }, {
          key: "hasPreviousStep",
          value: function hasPreviousStep() {
            return this.hasStep(this.currentStepIndex - 1);
          }
          /**
           * Checks if this wizard has a next step, compared to the current step
           *
           * @returns True if this wizard has a next step after the current step
           */

        }, {
          key: "hasNextStep",
          value: function hasNextStep() {
            return this.hasStep(this.currentStepIndex + 1);
          }
          /**
           * Checks if this wizard is currently inside its last step
           *
           * @returns True if the wizard is currently inside its last step
           */

        }, {
          key: "isLastStep",
          value: function isLastStep() {
            return this.wizardSteps.length > 0 && this.currentStepIndex === this.wizardSteps.length - 1;
          }
          /**
           * Finds the [[WizardStep]] at the given index `stepIndex`.
           * If no [[WizardStep]] exists at the given index an Error is thrown
           *
           * @param stepIndex The given index
           * @returns The found [[WizardStep]] at the given index `stepIndex`
           * @throws An `Error` is thrown, if the given index `stepIndex` doesn't exist
           */

        }, {
          key: "getStepAtIndex",
          value: function getStepAtIndex(stepIndex) {
            if (!this.hasStep(stepIndex)) {
              throw new Error("Expected a known step, but got stepIndex: ".concat(stepIndex, "."));
            }

            return this.wizardSteps[stepIndex];
          }
          /**
           * Finds the index of the step with the given `stepId`.
           * If no step with the given `stepId` exists, `-1` is returned
           *
           * @param stepId The given step id
           * @returns The found index of a step with the given step id, or `-1` if no step with the given id is included in the wizard
           */

        }, {
          key: "getIndexOfStepWithId",
          value: function getIndexOfStepWithId(stepId) {
            return this.wizardSteps.findIndex(function (step) {
              return step.stepId === stepId;
            });
          }
          /**
           * Finds the index of the given [[WizardStep]] `step`.
           * If the given [[WizardStep]] is not contained inside this wizard, `-1` is returned
           *
           * @param step The given [[WizardStep]]
           * @returns The found index of `step` or `-1` if the step is not included in the wizard
           */

        }, {
          key: "getIndexOfStep",
          value: function getIndexOfStep(step) {
            return this.wizardSteps.indexOf(step);
          }
          /**
           * Calculates the correct [[MovingDirection]] value for a given `destinationStep` compared to the `currentStepIndex`.
           *
           * @param destinationStep The given destination step
           * @returns The calculated [[MovingDirection]]
           */

        }, {
          key: "getMovingDirection",
          value: function getMovingDirection(destinationStep) {
            var movingDirection;

            if (destinationStep > this.currentStepIndex) {
              movingDirection = MovingDirection.Forwards;
            } else if (destinationStep < this.currentStepIndex) {
              movingDirection = MovingDirection.Backwards;
            } else {
              movingDirection = MovingDirection.Stay;
            }

            return movingDirection;
          }
          /**
           * Checks, whether a wizard step, as defined by the given destination index, can be transitioned to.
           *
           * This method controls navigation by [[goToStep]], [[goToPreviousStep]], and [[goToNextStep]] directives.
           * Navigation by navigation bar is governed by [[isNavigable]].
           *
           * @param destinationIndex The index of the destination step
           * @returns A [[Promise]] containing `true`, if the destination step can be transitioned to and false otherwise
           */

        }, {
          key: "canGoToStep",
          value: function canGoToStep(destinationIndex) {
            return this.navigation.canGoToStep(this, destinationIndex);
          }
          /**
           * Tries to transition to the wizard step, as denoted by the given destination index.
           *
           * Note: You do not have to call [[canGoToStep]] before calling [[goToStep]].
           * The [[canGoToStep]] method will be called automatically.
           *
           * @param destinationIndex The index of the destination wizard step, which should be entered
           * @param preFinalize An event emitter, to be called before the step has been transitioned
           * @param postFinalize An event emitter, to be called after the step has been transitioned
           */

        }, {
          key: "goToStep",
          value: function goToStep(destinationIndex, preFinalize, postFinalize) {
            return this.navigation.goToStep(this, destinationIndex, preFinalize, postFinalize);
          }
          /**
           * Tries to transition the wizard to the previous step
           *
           * @param preFinalize An event emitter, to be called before the step has been transitioned
           * @param postFinalize An event emitter, to be called after the step has been transitioned
           */

        }, {
          key: "goToPreviousStep",
          value: function goToPreviousStep(preFinalize, postFinalize) {
            return this.navigation.goToStep(this, this.currentStepIndex - 1, preFinalize, postFinalize);
          }
          /**
           * Tries to transition the wizard to the next step
           *
           * @param preFinalize An event emitter, to be called before the step has been transitioned
           * @param postFinalize An event emitter, to be called after the step has been transitioned
           */

        }, {
          key: "goToNextStep",
          value: function goToNextStep(preFinalize, postFinalize) {
            return this.navigation.goToStep(this, this.currentStepIndex + 1, preFinalize, postFinalize);
          }
          /**
           * Checks, whether the wizard step, located at the given index, can be navigated to using the navigation bar.
           *
           * @param destinationIndex The index of the destination step
           * @returns True if the step can be navigated to, false otherwise
           */

        }, {
          key: "isNavigable",
          value: function isNavigable(destinationIndex) {
            return this.navigation.isNavigable(this, destinationIndex);
          }
          /**
           * Resets the state of this wizard.
           */

        }, {
          key: "reset",
          value: function reset() {
            this.navigation.reset(this);
          }
        }, {
          key: "defaultStepIndex",
          get: function get() {
            // This value can be either:
            // - the index of a wizard step with a `selected` directive, or
            // - the default step index, set in the [[WizardComponent]]
            var foundDefaultStep = this.wizardSteps.find(function (step) {
              return step.defaultSelected;
            });

            if (foundDefaultStep) {
              return this.getIndexOfStep(foundDefaultStep);
            } else {
              return this._defaultStepIndex;
            }
          },
          set: function set(defaultStepIndex) {
            this._defaultStepIndex = defaultStepIndex;
          }
          /**
           * Returns true if this wizard uses a horizontal orientation.
           * The wizard uses a horizontal orientation, iff the navigation bar is shown at the top or bottom of this wizard
           *
           * @returns True if this wizard uses a horizontal orientation
           */

        }, {
          key: "horizontalOrientation",
          get: function get() {
            return this.navBarLocation === 'top' || this.navBarLocation === 'bottom';
          }
          /**
           * Returns true if this wizard uses a vertical orientation.
           * The wizard uses a vertical orientation, iff the navigation bar is shown at the left or right of this wizard
           *
           * @returns True if this wizard uses a vertical orientation
           */

        }, {
          key: "verticalOrientation",
          get: function get() {
            return this.navBarLocation === 'left' || this.navBarLocation === 'right';
          }
        }, {
          key: "currentStep",
          get: function get() {
            if (this.hasStep(this.currentStepIndex)) {
              return this.wizardSteps[this.currentStepIndex];
            } else {
              return null;
            }
          }
          /**
           * The completeness of the wizard.
           * If the wizard has been completed, i.e. all steps are either completed or optional, this value is true, otherwise it is false
           */

        }, {
          key: "completed",
          get: function get() {
            return this.wizardSteps.every(function (step) {
              return step.completed || step.optional;
            });
          }
          /**
           * An array representation of all wizard steps belonging to this model
           */

        }, {
          key: "wizardSteps",
          get: function get() {
            return this._wizardSteps;
          }
        }, {
          key: "navigation",
          get: function get() {
            return this._navigation;
          }
          /**
           * Updates the navigation mode for this wizard component
           *
           * @param navigation The updated navigation mode
           */
          ,
          set: function set(navigation) {
            this._navigation = navigation;
          }
        }]);

        return WizardComponent;
      }();

      WizardComponent.ɵfac = function WizardComponent_Factory(t) {
        return new (t || WizardComponent)();
      };

      WizardComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({
        type: WizardComponent,
        selectors: [["aw-wizard"]],
        contentQueries: function WizardComponent_ContentQueries(rf, ctx, dirIndex) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵcontentQuery"](dirIndex, WizardStep, true);
          }

          if (rf & 2) {
            var _t;

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵloadQuery"]()) && (ctx.wizardStepsQueryList = _t);
          }
        },
        hostVars: 4,
        hostBindings: function WizardComponent_HostBindings(rf, ctx) {
          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassProp"]("horizontal", ctx.horizontalOrientation)("vertical", ctx.verticalOrientation);
          }
        },
        inputs: {
          navBarLocation: "navBarLocation",
          navBarLayout: "navBarLayout",
          navBarDirection: "navBarDirection",
          disableNavigationBar: "disableNavigationBar",
          defaultStepIndex: "defaultStepIndex"
        },
        ngContentSelectors: _c0,
        decls: 4,
        vars: 6,
        consts: [[3, "direction", "ngClass", 4, "ngIf"], [3, "ngClass"], [3, "direction", "ngClass"]],
        template: function WizardComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵprojectionDef"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](0, WizardComponent_aw_wizard_navigation_bar_0_Template, 1, 10, "aw-wizard-navigation-bar", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵprojection"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, WizardComponent_aw_wizard_navigation_bar_3_Template, 1, 10, "aw-wizard-navigation-bar", 0);
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.navBarLocation == "top" || ctx.navBarLocation == "left");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction2"](3, _c2, ctx.navBarLocation == "left" || ctx.navBarLocation == "right", ctx.navBarLocation == "top" || ctx.navBarLocation == "bottom"));

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.navBarLocation == "bottom" || ctx.navBarLocation == "right");
          }
        },
        directives: function directives() {
          return [_angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"], _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgClass"], WizardNavigationBarComponent];
        },
        encapsulation: 2
      });
      Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ContentChildren"])(WizardStep, {
        descendants: true
      }), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["QueryList"])], WizardComponent.prototype, "wizardStepsQueryList", void 0);
      Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Object)], WizardComponent.prototype, "navBarLocation", void 0);
      Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Object)], WizardComponent.prototype, "navBarLayout", void 0);
      Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Object)], WizardComponent.prototype, "navBarDirection", void 0);
      Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Number), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [Number])], WizardComponent.prototype, "defaultStepIndex", null);
      Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Object)], WizardComponent.prototype, "disableNavigationBar", void 0);
      Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostBinding"])('class.horizontal'), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Boolean), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])], WizardComponent.prototype, "horizontalOrientation", null);
      Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostBinding"])('class.vertical'), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Boolean), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])], WizardComponent.prototype, "verticalOrientation", null);
      WizardComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [])], WizardComponent);
      /**
       * The `aw-wizard-navigation-bar` component contains the navigation bar inside a [[WizardComponent]].
       * To correctly display the navigation bar, it's required to set the right css classes for the navigation bar,
       * otherwise it will look like a normal `ul` component.
       *
       * ### Syntax
       *
       * ```html
       * <aw-wizard-navigation-bar></aw-wizard-navigation-bar>
       * ```
       *
       * @author Marc Arndt
       */

      var WizardNavigationBarComponent = /*#__PURE__*/function () {
        /**
         * Constructor
         *
         * @param wizard The state the wizard currently resides in
         */
        function WizardNavigationBarComponent(wizard) {
          _classCallCheck(this, WizardNavigationBarComponent);

          this.wizard = wizard;
          /**
           * The direction in which the wizard steps should be shown in the navigation bar.
           * This value can be either `left-to-right` or `right-to-left`
           */

          this.direction = 'left-to-right';
        }
        /**
         * Returns all [[WizardStep]]s contained in the wizard
         *
         * @returns An array containing all [[WizardStep]]s
         */


        _createClass(WizardNavigationBarComponent, [{
          key: "isCurrent",

          /**
           * Checks, whether a [[WizardStep]] can be marked as `current` in the navigation bar
           *
           * @param wizardStep The wizard step to be checked
           * @returns True if the step can be marked as `current`
           */
          value: function isCurrent(wizardStep) {
            return wizardStep.selected;
          }
          /**
           * Checks, whether a [[WizardStep]] can be marked as `editing` in the navigation bar
           *
           * @param wizardStep The wizard step to be checked
           * @returns True if the step can be marked as `editing`
           */

        }, {
          key: "isEditing",
          value: function isEditing(wizardStep) {
            return wizardStep.editing;
          }
          /**
           * Checks, whether a [[WizardStep]] can be marked as `done` in the navigation bar
           *
           * @param wizardStep The wizard step to be checked
           * @returns True if the step can be marked as `done`
           */

        }, {
          key: "isDone",
          value: function isDone(wizardStep) {
            return wizardStep.completed;
          }
          /**
           * Checks, whether a [[WizardStep]] can be marked as `optional` in the navigation bar
           *
           * @param wizardStep The wizard step to be checked
           * @returns True if the step can be marked as `optional`
           */

        }, {
          key: "isOptional",
          value: function isOptional(wizardStep) {
            return wizardStep.optional;
          }
          /**
           * Checks, whether a [[WizardStep]] can be marked as `completed` in the navigation bar.
           *
           * The `completed` class is only applied to completion steps.
           *
           * @param wizardStep The wizard step to be checked
           * @returns True if the step can be marked as `completed`
           */

        }, {
          key: "isCompleted",
          value: function isCompleted(wizardStep) {
            return wizardStep instanceof WizardCompletionStep && this.wizard.completed;
          }
          /**
           * Checks, whether a [[WizardStep]] can be marked as `navigable` in the navigation bar.
           * A wizard step can be navigated to if:
           * - the step is currently not selected
           * - the navigation bar isn't disabled
           * - the navigation mode allows navigation to the step
           *
           * @param wizardStep The wizard step to be checked
           * @returns True if the step can be marked as navigable
           */

        }, {
          key: "isNavigable",
          value: function isNavigable(wizardStep) {
            return !wizardStep.selected && !this.wizard.disableNavigationBar && this.wizard.isNavigable(this.wizard.getIndexOfStep(wizardStep));
          }
        }, {
          key: "wizardSteps",
          get: function get() {
            switch (this.direction) {
              case 'right-to-left':
                return this.wizard.wizardSteps.slice().reverse();

              case 'left-to-right':
              default:
                return this.wizard.wizardSteps;
            }
          }
          /**
           * Returns the number of wizard steps, that need to be displaced in the navigation bar
           *
           * @returns The number of wizard steps to be displayed
           */

        }, {
          key: "numberOfWizardSteps",
          get: function get() {
            return this.wizard.wizardSteps.length;
          }
        }]);

        return WizardNavigationBarComponent;
      }();

      WizardNavigationBarComponent.ɵfac = function WizardNavigationBarComponent_Factory(t) {
        return new (t || WizardNavigationBarComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](WizardComponent));
      };

      WizardNavigationBarComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({
        type: WizardNavigationBarComponent,
        selectors: [["aw-wizard-navigation-bar"]],
        inputs: {
          direction: "direction"
        },
        decls: 2,
        vars: 4,
        consts: [[3, "ngClass", 4, "ngFor", "ngForOf"], [3, "ngClass"], [3, "awGoToStep"], [1, "label"], [3, "ngTemplateOutlet", "ngTemplateOutletContext", 4, "ngIf"], [4, "ngIf"], [1, "step-indicator", 3, "ngStyle"], [3, "ngTemplateOutlet", "ngTemplateOutletContext"]],
        template: function WizardNavigationBarComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "ul");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, WizardNavigationBarComponent_li_1_Template, 8, 17, "li", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵclassMapInterpolate1"]("steps-indicator steps-", ctx.numberOfWizardSteps, "");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx.wizardSteps);
          }
        },
        directives: function directives() {
          return [_angular_common__WEBPACK_IMPORTED_MODULE_2__["NgForOf"], _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgClass"], GoToStepDirective, _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgIf"], _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgStyle"], _angular_common__WEBPACK_IMPORTED_MODULE_2__["NgTemplateOutlet"]];
        },
        encapsulation: 2
      });

      WizardNavigationBarComponent.ctorParameters = function () {
        return [{
          type: WizardComponent
        }];
      };

      Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Object)], WizardNavigationBarComponent.prototype, "direction", void 0);
      WizardNavigationBarComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [WizardComponent])], WizardNavigationBarComponent);
      var WizardStepComponent_1;
      /**
       * The `aw-wizard-step` component is used to define a normal step inside a wizard.
       *
       * ### Syntax
       *
       * With `stepTitle` and `navigationSymbol` inputs:
       *
       * ```html
       * <aw-wizard-step [stepTitle]="step title" [navigationSymbol]="{ symbol: 'symbol', fontFamily: 'font-family' }"
       *    [canExit]="deciding function" (stepEnter)="enter function" (stepExit)="exit function">
       *    ...
       * </aw-wizard-step>
       * ```
       *
       * With `awWizardStepTitle` and `awWizardStepSymbol` directives:
       *
       * ```html
       * <aw-wizard-step"
       *    [canExit]="deciding function" (stepEnter)="enter function" (stepExit)="exit function">
       *    <ng-template awWizardStepTitle>
       *        step title
       *    </ng-template>
       *    <ng-template awWizardStepSymbol>
       *        symbol
       *    </ng-template>
       *    ...
       * </aw-wizard-step>
       * ```
       *
       * ### Example
       *
       * With `stepTitle` and `navigationSymbol` inputs:
       *
       * ```html
       * <aw-wizard-step stepTitle="Address information" [navigationSymbol]="{ symbol: '&#xf1ba;', fontFamily: 'FontAwesome' }">
       *    ...
       * </aw-wizard-step>
       * ```
       *
       * With `awWizardStepTitle` and `awWizardStepSymbol` directives:
       *
       * ```html
       * <aw-wizard-step>
       *    <ng-template awWizardStepTitle>
       *        Address information
       *    </ng-template>
       *    <ng-template awWizardStepSymbol>
       *        <i class="fa fa-taxi"></i>
       *    </ng-template>
       * </aw-wizard-step>
       * ```
       *
       * @author Marc Arndt
       */

      var WizardStepComponent = WizardStepComponent_1 = /*#__PURE__*/function (_WizardStep2) {
        _inherits(WizardStepComponent, _WizardStep2);

        var _super4 = _createSuper(WizardStepComponent);

        function WizardStepComponent() {
          _classCallCheck(this, WizardStepComponent);

          return _super4.apply(this, arguments);
        }

        return WizardStepComponent;
      }(WizardStep);

      WizardStepComponent.ɵfac = function WizardStepComponent_Factory(t) {
        return ɵWizardStepComponent_BaseFactory(t || WizardStepComponent);
      };

      WizardStepComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({
        type: WizardStepComponent,
        selectors: [["aw-wizard-step"]],
        features: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵProvidersFeature"]([{
          provide: WizardStep,
          useExisting: Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["forwardRef"])(function () {
            return WizardStepComponent_1;
          })
        }]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵInheritDefinitionFeature"]],
        ngContentSelectors: _c0,
        decls: 1,
        vars: 0,
        template: function WizardStepComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵprojectionDef"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵprojection"](0);
          }
        },
        encapsulation: 2
      });
      /**
       * The `awEnableBackLinks` directive can be used to allow the user to leave a [[WizardCompletionStep]] after is has been entered.
       *
       * ### Syntax
       *
       * ```html
       * <aw-wizard-completion-step awEnableBackLinks (stepExit)="exit function">
       *     ...
       * </aw-wizard-completion-step>
       * ```
       *
       * ### Example
       *
       * ```html
       * <aw-wizard-completion-step stepTitle="Final step" awEnableBackLinks>
       *     ...
       * </aw-wizard-completion-step>
       * ```
       *
       * @author Marc Arndt
       */

      var EnableBackLinksDirective = /*#__PURE__*/function () {
        /**
         * Constructor
         *
         * @param completionStep The wizard completion step, which should be exitable
         */
        function EnableBackLinksDirective(completionStep) {
          _classCallCheck(this, EnableBackLinksDirective);

          this.completionStep = completionStep;
          /**
           * This EventEmitter is called when the step is exited.
           * The bound method can be used to do cleanup work.
           */

          this.stepExit = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        }
        /**
         * Initialization work
         */


        _createClass(EnableBackLinksDirective, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.completionStep.canExit = true;
            this.completionStep.stepExit = this.stepExit;
          }
        }]);

        return EnableBackLinksDirective;
      }();

      EnableBackLinksDirective.ɵfac = function EnableBackLinksDirective_Factory(t) {
        return new (t || EnableBackLinksDirective)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](WizardCompletionStep, 1));
      };

      EnableBackLinksDirective.ɵdir = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineDirective"]({
        type: EnableBackLinksDirective,
        selectors: [["", "awEnableBackLinks", ""]],
        outputs: {
          stepExit: "stepExit"
        }
      });

      EnableBackLinksDirective.ctorParameters = function () {
        return [{
          type: WizardCompletionStep,
          decorators: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Host"]
          }]
        }];
      };

      Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Object)], EnableBackLinksDirective.prototype, "stepExit", void 0);
      EnableBackLinksDirective = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__param"])(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Host"])()), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [WizardCompletionStep])], EnableBackLinksDirective);
      /**
       * Checks whether the given `value` implements the interface [[StepId]].
       *
       * @param value The value to be checked
       * @returns True if the given value implements [[StepId]] and false otherwise
       */

      function isStepId(value) {
        return value.hasOwnProperty('stepId') && !(value instanceof WizardStep);
      }
      /**
       * Checks whether the given `value` implements the interface [[StepIndex]].
       *
       * @param value The value to be checked
       * @returns True if the given value implements [[StepIndex]] and false otherwise
       */


      function isStepIndex(value) {
        return value.hasOwnProperty('stepIndex');
      }
      /**
       * Checks whether the given `value` implements the interface [[StepOffset]].
       *
       * @param value The value to be checked
       * @returns True if the given value implements [[StepOffset]] and false otherwise
       */


      function isStepOffset(value) {
        return value.hasOwnProperty('stepOffset');
      }
      /**
       * The `awGoToStep` directive can be used to navigate to a given step.
       * This step can be defined in one of multiple formats
       *
       * ### Syntax
       *
       * With absolute step index:
       *
       * ```html
       * <button [awGoToStep]="{ stepIndex: absolute step index }" (finalize)="finalize method">...</button>
       * ```
       *
       * With unique step id:
       *
       * ```html
       * <button [awGoToStep]="{ stepId: 'step id of destination step' }" (finalize)="finalize method">...</button>
       * ```
       *
       * With a wizard step object:
       *
       * ```html
       * <button [awGoToStep]="wizard step object" (finalize)="finalize method">...</button>
       * ```
       *
       * With an offset to the defining step:
       *
       * ```html
       * <button [awGoToStep]="{ stepOffset: offset }" (finalize)="finalize method">...</button>
       * ```
       *
       * @author Marc Arndt
       */


      var GoToStepDirective = /*#__PURE__*/function () {
        /**
         * Constructor
         *
         * @param wizard The wizard component
         * @param wizardStep The wizard step, which contains this [[GoToStepDirective]]
         */
        function GoToStepDirective(wizard, wizardStep) {
          _classCallCheck(this, GoToStepDirective);

          this.wizard = wizard;
          this.wizardStep = wizardStep;
          /**
           * This [[EventEmitter]] is called directly before the current step is exited during a transition through a component with this directive.
           */

          this.preFinalize = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
          /**
           * This [[EventEmitter]] is called directly after the current step is exited during a transition through a component with this directive.
           */

          this.postFinalize = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        }
        /**
         * A convenience field for `preFinalize`
         */


        _createClass(GoToStepDirective, [{
          key: "onClick",

          /**
           * Listener method for `click` events on the component with this directive.
           * After this method is called the wizard will try to transition to the `destinationStep`
           */
          value: function onClick() {
            this.wizard.goToStep(this.destinationStep, this.preFinalize, this.postFinalize);
          }
        }, {
          key: "finalize",
          get: function get() {
            return this.preFinalize;
          }
          /**
           * A convenience name for `preFinalize`
           *
           * @param emitter The [[EventEmitter]] to be set
           */
          ,
          set: function set(emitter) {
            /* istanbul ignore next */
            this.preFinalize = emitter;
          }
          /**
           * Returns the destination step of this directive as an absolute step index inside the wizard
           *
           * @returns The index of the destination step
           * @throws If `targetStep` is of an unknown type an `Error` is thrown
           */

        }, {
          key: "destinationStep",
          get: function get() {
            var destinationStep;

            if (isStepIndex(this.targetStep)) {
              destinationStep = this.targetStep.stepIndex;
            } else if (isStepId(this.targetStep)) {
              destinationStep = this.wizard.getIndexOfStepWithId(this.targetStep.stepId);
            } else if (isStepOffset(this.targetStep) && this.wizardStep !== null) {
              destinationStep = this.wizard.getIndexOfStep(this.wizardStep) + this.targetStep.stepOffset;
            } else if (this.targetStep instanceof WizardStep) {
              destinationStep = this.wizard.getIndexOfStep(this.targetStep);
            } else {
              throw new Error("Input 'targetStep' is neither a WizardStep, StepOffset, StepIndex or StepId");
            }

            return destinationStep;
          }
        }]);

        return GoToStepDirective;
      }();

      GoToStepDirective.ɵfac = function GoToStepDirective_Factory(t) {
        return new (t || GoToStepDirective)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](WizardComponent), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](WizardStep, 8));
      };

      GoToStepDirective.ɵdir = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineDirective"]({
        type: GoToStepDirective,
        selectors: [["", "awGoToStep", ""]],
        hostBindings: function GoToStepDirective_HostBindings(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function GoToStepDirective_click_HostBindingHandler() {
              return ctx.onClick();
            });
          }
        },
        inputs: {
          targetStep: ["awGoToStep", "targetStep"]
        },
        outputs: {
          preFinalize: "preFinalize",
          postFinalize: "postFinalize",
          finalize: "finalize"
        }
      });

      GoToStepDirective.ctorParameters = function () {
        return [{
          type: WizardComponent
        }, {
          type: WizardStep,
          decorators: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Optional"]
          }]
        }];
      };

      Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])], GoToStepDirective.prototype, "preFinalize", void 0);
      Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])], GoToStepDirective.prototype, "postFinalize", void 0);
      Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('awGoToStep'), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Object)], GoToStepDirective.prototype, "targetStep", void 0);
      Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]])], GoToStepDirective.prototype, "finalize", null);
      Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('click'), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Function), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", []), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:returntype", void 0)], GoToStepDirective.prototype, "onClick", null);
      GoToStepDirective = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__param"])(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Optional"])()), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [WizardComponent, WizardStep])], GoToStepDirective);
      /**
       * The `awNextStep` directive can be used to navigate to the next step.
       *
       * ### Syntax
       *
       * ```html
       * <button awNextStep (finalize)="finalize method">...</button>
       * ```
       *
       * @author Marc Arndt
       */

      var NextStepDirective = /*#__PURE__*/function () {
        /**
         * Constructor
         *
         * @param wizard The state of the wizard
         */
        function NextStepDirective(wizard) {
          _classCallCheck(this, NextStepDirective);

          this.wizard = wizard;
          /**
           * This [[EventEmitter]] is called directly before the current step is exited during a transition through a component with this directive.
           */

          this.preFinalize = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
          /**
           * This [[EventEmitter]] is called directly after the current step is exited during a transition through a component with this directive.
           */

          this.postFinalize = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        }
        /**
         * A convenience field for `preFinalize`
         */


        _createClass(NextStepDirective, [{
          key: "onClick",

          /**
           * Listener method for `click` events on the component with this directive.
           * After this method is called the wizard will try to transition to the next step
           */
          value: function onClick() {
            this.wizard.goToNextStep(this.preFinalize, this.postFinalize);
          }
        }, {
          key: "finalize",
          get: function get() {
            return this.preFinalize;
          }
          /**
           * A convenience name for `preFinalize`
           *
           * @param emitter The [[EventEmitter]] to be set
           */
          ,
          set: function set(emitter) {
            /* istanbul ignore next */
            this.preFinalize = emitter;
          }
        }]);

        return NextStepDirective;
      }();

      NextStepDirective.ɵfac = function NextStepDirective_Factory(t) {
        return new (t || NextStepDirective)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](WizardComponent));
      };

      NextStepDirective.ɵdir = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineDirective"]({
        type: NextStepDirective,
        selectors: [["", "awNextStep", ""]],
        hostBindings: function NextStepDirective_HostBindings(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function NextStepDirective_click_HostBindingHandler() {
              return ctx.onClick();
            });
          }
        },
        outputs: {
          preFinalize: "preFinalize",
          postFinalize: "postFinalize",
          finalize: "finalize"
        }
      });

      NextStepDirective.ctorParameters = function () {
        return [{
          type: WizardComponent
        }];
      };

      Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])], NextStepDirective.prototype, "preFinalize", void 0);
      Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])], NextStepDirective.prototype, "postFinalize", void 0);
      Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]])], NextStepDirective.prototype, "finalize", null);
      Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('click'), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Function), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", []), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:returntype", void 0)], NextStepDirective.prototype, "onClick", null);
      NextStepDirective = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [WizardComponent])], NextStepDirective);
      /**
       * The `awOptionalStep` directive can be used to define an optional `wizard-step`.
       * An optional wizard step is a [[WizardStep]] that doesn't need to be completed to transition to later wizard steps.
       *
       * ### Syntax
       *
       * ```html
       * <aw-wizard-step awOptionalStep>
       *     ...
       * </aw-wizard-step>
       * ```
       *
       * ### Example
       *
       * ```html
       * <aw-wizard-step stepTitle="Second step" awOptionalStep>
       *     ...
       * </aw-wizard-step>
       * ```
       *
       * @author Marc Arndt
       */

      var OptionalStepDirective = /*#__PURE__*/function () {
        /**
         * Constructor
         *
         * @param wizardStep The wizard step, which contains this [[OptionalStepDirective]]
         */
        function OptionalStepDirective(wizardStep) {
          _classCallCheck(this, OptionalStepDirective);

          this.wizardStep = wizardStep; // tslint:disable-next-line:no-input-rename

          this.optional = true;
        }
        /**
         * Initialization work
         */


        _createClass(OptionalStepDirective, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            // The input receives '' when specified in the template without a value.  In this case, apply the default value (`true`).
            this.wizardStep.optional = this.optional || this.optional === '';
          }
        }]);

        return OptionalStepDirective;
      }();

      OptionalStepDirective.ɵfac = function OptionalStepDirective_Factory(t) {
        return new (t || OptionalStepDirective)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](WizardStep, 1));
      };

      OptionalStepDirective.ɵdir = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineDirective"]({
        type: OptionalStepDirective,
        selectors: [["", "awOptionalStep", ""]],
        inputs: {
          optional: ["awOptionalStep", "optional"]
        }
      });

      OptionalStepDirective.ctorParameters = function () {
        return [{
          type: WizardStep,
          decorators: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Host"]
          }]
        }];
      };

      Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('awOptionalStep'), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Object)], OptionalStepDirective.prototype, "optional", void 0);
      OptionalStepDirective = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__param"])(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Host"])()), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [WizardStep])], OptionalStepDirective);
      /**
       * The `awPreviousStep` directive can be used to navigate to the previous step.
       * Compared to the [[NextStepDirective]] it's important to note, that this directive doesn't contain a `finalize` output method.
       *
       * ### Syntax
       *
       * ```html
       * <button awPreviousStep>...</button>
       * ```
       *
       * @author Marc Arndt
       */

      var PreviousStepDirective = /*#__PURE__*/function () {
        /**
         * Constructor
         *
         * @param wizard The state of the wizard
         */
        function PreviousStepDirective(wizard) {
          _classCallCheck(this, PreviousStepDirective);

          this.wizard = wizard;
          /**
           * This [[EventEmitter]] is called directly before the current step is exited during a transition through a component with this directive.
           */

          this.preFinalize = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
          /**
           * This [[EventEmitter]] is called directly after the current step is exited during a transition through a component with this directive.
           */

          this.postFinalize = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        }
        /**
         * A convenience field for `preFinalize`
         */


        _createClass(PreviousStepDirective, [{
          key: "onClick",

          /**
           * Listener method for `click` events on the component with this directive.
           * After this method is called the wizard will try to transition to the previous step
           */
          value: function onClick() {
            this.wizard.goToPreviousStep(this.preFinalize, this.postFinalize);
          }
        }, {
          key: "finalize",
          get: function get() {
            return this.preFinalize;
          }
          /**
           * A convenience field for `preFinalize`
           *
           * @param emitter The [[EventEmitter]] to be set
           */
          ,
          set: function set(emitter) {
            /* istanbul ignore next */
            this.preFinalize = emitter;
          }
        }]);

        return PreviousStepDirective;
      }();

      PreviousStepDirective.ɵfac = function PreviousStepDirective_Factory(t) {
        return new (t || PreviousStepDirective)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](WizardComponent));
      };

      PreviousStepDirective.ɵdir = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineDirective"]({
        type: PreviousStepDirective,
        selectors: [["", "awPreviousStep", ""]],
        hostBindings: function PreviousStepDirective_HostBindings(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function PreviousStepDirective_click_HostBindingHandler() {
              return ctx.onClick();
            });
          }
        },
        outputs: {
          preFinalize: "preFinalize",
          postFinalize: "postFinalize",
          finalize: "finalize"
        }
      });

      PreviousStepDirective.ctorParameters = function () {
        return [{
          type: WizardComponent
        }];
      };

      Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])], PreviousStepDirective.prototype, "preFinalize", void 0);
      Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])], PreviousStepDirective.prototype, "postFinalize", void 0);
      Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]])], PreviousStepDirective.prototype, "finalize", null);
      Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('click'), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Function), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", []), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:returntype", void 0)], PreviousStepDirective.prototype, "onClick", null);
      PreviousStepDirective = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [WizardComponent])], PreviousStepDirective);
      /**
       * The `awResetWizard` directive can be used to reset the wizard to its initial state.
       * This directive accepts an output, which can be used to specify some custom cleanup work during the reset process.
       *
       * ### Syntax
       *
       * ```html
       * <button awResetWizard (finalize)="custom reset task">...</button>
       * ```
       *
       * @author Marc Arndt
       */

      var ResetWizardDirective = /*#__PURE__*/function () {
        /**
         * Constructor
         *
         * @param wizard The wizard component
         */
        function ResetWizardDirective(wizard) {
          _classCallCheck(this, ResetWizardDirective);

          this.wizard = wizard;
          /**
           * An [[EventEmitter]] containing some tasks to be done, directly before the wizard is being reset
           */

          this.finalize = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        }
        /**
         * Resets the wizard
         */


        _createClass(ResetWizardDirective, [{
          key: "onClick",
          value: function onClick() {
            // do some optional cleanup work
            this.finalize.emit(); // reset the wizard to its initial state

            this.wizard.reset();
          }
        }]);

        return ResetWizardDirective;
      }();

      ResetWizardDirective.ɵfac = function ResetWizardDirective_Factory(t) {
        return new (t || ResetWizardDirective)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](WizardComponent));
      };

      ResetWizardDirective.ɵdir = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineDirective"]({
        type: ResetWizardDirective,
        selectors: [["", "awResetWizard", ""]],
        hostBindings: function ResetWizardDirective_HostBindings(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function ResetWizardDirective_click_HostBindingHandler() {
              return ctx.onClick();
            });
          }
        },
        outputs: {
          finalize: "finalize"
        }
      });

      ResetWizardDirective.ctorParameters = function () {
        return [{
          type: WizardComponent
        }];
      };

      Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])], ResetWizardDirective.prototype, "finalize", void 0);
      Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])('click'), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Function), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", []), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:returntype", void 0)], ResetWizardDirective.prototype, "onClick", null);
      ResetWizardDirective = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [WizardComponent])], ResetWizardDirective);
      /**
       * The `awSelectedStep` directive can be used on a [[WizardStep]] to set it as selected after the wizard initialisation or a reset.
       *
       * ### Syntax
       *
       * ```html
       * <aw-wizard-step stepTitle="Step title" awSelectedStep>
       *     ...
       * </aw-wizard-step>
       * ```
       *
       * @author Marc Arndt
       */

      var SelectedStepDirective = /*#__PURE__*/function () {
        /**
         * Constructor
         *
         * @param wizardStep The wizard step, which should be selected by default
         */
        function SelectedStepDirective(wizardStep) {
          _classCallCheck(this, SelectedStepDirective);

          this.wizardStep = wizardStep;
        }
        /**
         * Initialization work
         */


        _createClass(SelectedStepDirective, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.wizardStep.defaultSelected = true;
          }
        }]);

        return SelectedStepDirective;
      }();

      SelectedStepDirective.ɵfac = function SelectedStepDirective_Factory(t) {
        return new (t || SelectedStepDirective)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](WizardStep, 1));
      };

      SelectedStepDirective.ɵdir = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineDirective"]({
        type: SelectedStepDirective,
        selectors: [["", "awSelectedStep", ""]]
      });

      SelectedStepDirective.ctorParameters = function () {
        return [{
          type: WizardStep,
          decorators: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Host"]
          }]
        }];
      };

      SelectedStepDirective = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__param"])(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Host"])()), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [WizardStep])], SelectedStepDirective);
      var WizardCompletionStepDirective_1;
      /**
       * The `awWizardCompletionStep` directive can be used to define a completion/success step at the end of your wizard
       * After a [[WizardCompletionStep]] has been entered, it has the characteristic that the user is blocked from
       * leaving it again to a previous step.
       * In addition entering a [[WizardCompletionStep]] automatically sets the `wizard`, and all steps inside the `wizard`,
       * as completed.
       *
       * ### Syntax
       *
       * ```html
       * <div awWizardCompletionStep [stepTitle]="title of the wizard step"
       *    [navigationSymbol]="{ symbol: 'navigation symbol', fontFamily: 'font-family' }"
       *    (stepEnter)="event emitter to be called when the wizard step is entered"
       *    (stepExit)="event emitter to be called when the wizard step is exited">
       *    ...
       * </div>
       * ```
       *
       * ### Example
       *
       * ```html
       * <div awWizardCompletionStep stepTitle="Step 1" [navigationSymbol]="{ symbol: '1' }">
       *    ...
       * </div>
       * ```
       *
       * With a navigation symbol from the `font-awesome` font:
       *
       * ```html
       * <div awWizardCompletionStep stepTitle="Step 1" [navigationSymbol]="{ symbol: '&#xf1ba;', fontFamily: 'FontAwesome' }">
       *    ...
       * </div>
       * ```
       *
       * @author Marc Arndt
       */

      var WizardCompletionStepDirective = WizardCompletionStepDirective_1 = /*#__PURE__*/function (_WizardCompletionStep2) {
        _inherits(WizardCompletionStepDirective, _WizardCompletionStep2);

        var _super5 = _createSuper(WizardCompletionStepDirective);

        function WizardCompletionStepDirective() {
          _classCallCheck(this, WizardCompletionStepDirective);

          return _super5.apply(this, arguments);
        }

        return WizardCompletionStepDirective;
      }(WizardCompletionStep);

      WizardCompletionStepDirective.ɵfac = function WizardCompletionStepDirective_Factory(t) {
        return ɵWizardCompletionStepDirective_BaseFactory(t || WizardCompletionStepDirective);
      };

      WizardCompletionStepDirective.ɵdir = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineDirective"]({
        type: WizardCompletionStepDirective,
        selectors: [["", "awWizardCompletionStep", ""]],
        features: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵProvidersFeature"]([{
          provide: WizardStep,
          useExisting: Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["forwardRef"])(function () {
            return WizardCompletionStepDirective_1;
          })
        }, {
          provide: WizardCompletionStep,
          useExisting: Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["forwardRef"])(function () {
            return WizardCompletionStepDirective_1;
          })
        }]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵInheritDefinitionFeature"]]
      });
      var WizardStepDirective_1;
      /**
       * The `awWizardStep` directive can be used to define a normal step inside a wizard.
       *
       * ### Syntax
       *
       * With `stepTitle` and `navigationSymbol` inputs:
       *
       * ```html
       * <div awWizardStep [stepTitle]="step title" [navigationSymbol]="{ symbol: 'symbol', fontFamily: 'font-family' }"
       *    [canExit]="deciding function" (stepEnter)="enter function" (stepExit)="exit function">
       *    ...
       * </div>
       * ```
       *
       * With `awWizardStepTitle` and `awWizardStepSymbol` directives:
       *
       * ```html
       * <div awWizardStep [canExit]="deciding function" (stepEnter)="enter function" (stepExit)="exit function">
       *    <ng-template awWizardStepTitle>
       *        step title
       *    </ng-template>
       *    <ng-template awWizardStepSymbol>
       *        symbol
       *    </ng-template>
       *    ...
       * </div>
       * ```
       *
       * ### Example
       *
       * With `stepTitle` and `navigationSymbol` inputs:
       *
       * ```html
       * <div awWizardStep stepTitle="Address information" [navigationSymbol]="{ symbol: '&#xf1ba;', fontFamily: 'FontAwesome' }">
       *    ...
       * </div>
       * ```
       *
       * With `awWizardStepTitle` and `awWizardStepSymbol` directives:
       *
       * ```html
       * <div awWizardStep>
       *    <ng-template awWizardStepTitle>
       *        Address information
       *    </ng-template>
       *    <ng-template awWizardStepSymbol>
       *        <i class="fa fa-taxi"></i>
       *    </ng-template>
       * </div>
       * ```
       *
       * @author Marc Arndt
       */

      var WizardStepDirective = WizardStepDirective_1 = /*#__PURE__*/function (_WizardStep3) {
        _inherits(WizardStepDirective, _WizardStep3);

        var _super6 = _createSuper(WizardStepDirective);

        function WizardStepDirective() {
          _classCallCheck(this, WizardStepDirective);

          return _super6.apply(this, arguments);
        }

        return WizardStepDirective;
      }(WizardStep);

      WizardStepDirective.ɵfac = function WizardStepDirective_Factory(t) {
        return ɵWizardStepDirective_BaseFactory(t || WizardStepDirective);
      };

      WizardStepDirective.ɵdir = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineDirective"]({
        type: WizardStepDirective,
        selectors: [["", "awWizardStep", ""]],
        features: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵProvidersFeature"]([{
          provide: WizardStep,
          useExisting: Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["forwardRef"])(function () {
            return WizardStepDirective_1;
          })
        }]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵInheritDefinitionFeature"]]
      });
      /**
       * The [[awNavigationMode]] directive can be used to customize wizard'd navigation mode.
       *
       * There are several usage options:
       *
       * ### Option 1. Customize the default navigation mode with [[navigateBackward]] and/or [[navigateForward]] inputs.
       *
       * ```html
       * <aw-wizard [awNavigationMode] navigateBackward="deny" navigateForward="allow">...</aw-wizard>
       * ```
       *
       * ### Option 2. Pass in a custom navigation mode
       *
       * ```typescript
       * import { BaseNavigationMode } from 'angular-archwizard'
       *
       * class CustomNavigationMode extends BaseNavigationMode {
       *
       *   // ...
       * }
       * ```
       *
       * ```typescript
       * @Component({
       *   // ...
       * })
       * class MyComponent {
       *
       *   navigationMode = new CustomNavigationMode();
       * }
       * ```
       *
       * ```html
       * <aw-wizard [awNavigationMode]="navigationMode">...</aw-wizard>
       * ```
       *
       * ### Additional Notes
       *
       * - Specifying a custom navigation mode takes priority over [[navigateBackward]] and [[navigateForward]] inputs
       *
       * - Omitting the [[awNavigationMode]] directive or, equally, specifying just [[awNavigationMode]] without
       *   any inputs or parameters causes the wizard to use the default "strict" navigation mode equivalent to
       *
       * ```html
       * <aw-wizard [awNavigationMode] navigateBackward="deny" navigateForward="allow">...</aw-wizard>
       * ````
       */

      var NavigationModeDirective = /*#__PURE__*/function () {
        function NavigationModeDirective(wizard) {
          _classCallCheck(this, NavigationModeDirective);

          this.wizard = wizard;
        }

        _createClass(NavigationModeDirective, [{
          key: "ngOnChanges",
          value: function ngOnChanges(changes) {
            this.wizard.navigation = this.getNavigationMode();
          }
        }, {
          key: "getNavigationMode",
          value: function getNavigationMode() {
            if (this.awNavigationMode) {
              return this.awNavigationMode;
            }

            return new ConfigurableNavigationMode(this.navigateBackward, this.navigateForward);
          }
        }]);

        return NavigationModeDirective;
      }();

      NavigationModeDirective.ɵfac = function NavigationModeDirective_Factory(t) {
        return new (t || NavigationModeDirective)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](WizardComponent));
      };

      NavigationModeDirective.ɵdir = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineDirective"]({
        type: NavigationModeDirective,
        selectors: [["", "awNavigationMode", ""]],
        inputs: {
          awNavigationMode: "awNavigationMode",
          navigateBackward: "navigateBackward",
          navigateForward: "navigateForward"
        },
        features: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵNgOnChangesFeature"]]
      });

      NavigationModeDirective.ctorParameters = function () {
        return [{
          type: WizardComponent
        }];
      };

      Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Object)], NavigationModeDirective.prototype, "awNavigationMode", void 0);
      Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", String)], NavigationModeDirective.prototype, "navigateBackward", void 0);
      Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", String)], NavigationModeDirective.prototype, "navigateForward", void 0);
      NavigationModeDirective = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [WizardComponent])], NavigationModeDirective);
      /**
       * The `awCompletedStep` directive can be used to make a wizard step initially completed.
       *
       * Initially completed steps are shown as completed when the wizard is presented to the user.
       *
       * A typical use case is to make a step initially completed if it is automatically filled with some derived/predefined information.
       *
       * ### Syntax
       *
       * ```html
       * <aw-wizard-step awCompletedStep>
       *     ...
       * </aw-wizard-step>
       * ```
       *
       * An optional boolean condition can be specified:
       *
       * ```html
       * <aw-wizard-step [awCompletedStep]="shouldBeCompleted">
       *     ...
       * </aw-wizard-step>
       * ```
       *
       * ### Example
       *
       * ```html
       * <aw-wizard-step stepTitle="First step" [awCompletedStep]="firstStepPrefilled">
       *     ...
       * </aw-wizard-step>
       * ```
       */

      var CompletedStepDirective = /*#__PURE__*/function () {
        /**
         * Constructor
         *
         * @param wizardStep The wizard step, which contains this [[CompletedStepDirective]]
         */
        function CompletedStepDirective(wizardStep) {
          _classCallCheck(this, CompletedStepDirective);

          this.wizardStep = wizardStep; // tslint:disable-next-line:no-input-rename

          this.initiallyCompleted = true;
        }
        /**
         * Initialization work
         */


        _createClass(CompletedStepDirective, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            // The input receives '' when specified in the template without a value.  In this case, apply the default value (`true`).
            this.wizardStep.initiallyCompleted = this.initiallyCompleted || this.initiallyCompleted === '';
          }
        }]);

        return CompletedStepDirective;
      }();

      CompletedStepDirective.ɵfac = function CompletedStepDirective_Factory(t) {
        return new (t || CompletedStepDirective)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](WizardStep, 1));
      };

      CompletedStepDirective.ɵdir = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineDirective"]({
        type: CompletedStepDirective,
        selectors: [["", "awCompletedStep", ""]],
        inputs: {
          initiallyCompleted: ["awCompletedStep", "initiallyCompleted"]
        }
      });

      CompletedStepDirective.ctorParameters = function () {
        return [{
          type: WizardStep,
          decorators: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Host"]
          }]
        }];
      };

      Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])('awCompletedStep'), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:type", Object)], CompletedStepDirective.prototype, "initiallyCompleted", void 0);
      CompletedStepDirective = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__param"])(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Host"])()), Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"])("design:paramtypes", [WizardStep])], CompletedStepDirective);
      var ArchwizardModule_1;
      /**
       * The module defining all the content inside `angular-archwizard`
       *
       * @author Marc Arndt
       */

      var ArchwizardModule = ArchwizardModule_1 = /*#__PURE__*/function () {
        function ArchwizardModule() {
          _classCallCheck(this, ArchwizardModule);
        }

        _createClass(ArchwizardModule, null, [{
          key: "forRoot",

          /* istanbul ignore next */
          value: function forRoot() {
            return {
              ngModule: ArchwizardModule_1,
              providers: [// Nothing here yet
              ]
            };
          }
        }]);

        return ArchwizardModule;
      }();

      ArchwizardModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineNgModule"]({
        type: ArchwizardModule
      });
      ArchwizardModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjector"]({
        factory: function ArchwizardModule_Factory(t) {
          return new (t || ArchwizardModule)();
        },
        imports: [[_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"]]]
      });
      /*@__PURE__*/

      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](WizardStepSymbolDirective, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"],
          args: [{
            selector: 'ng-template[awStepSymbol], ng-template[awWizardStepSymbol]'
          }]
        }], function () {
          return [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["TemplateRef"]
          }];
        }, null);
      })();
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](WizardStepTitleDirective, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"],
          args: [{
            selector: 'ng-template[awStepTitle], ng-template[awWizardStepTitle]'
          }]
        }], function () {
          return [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["TemplateRef"]
          }];
        }, null);
      })();
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](WizardStep, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"]
        }], function () {
          return [];
        }, {
          navigationSymbol: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }],
          canEnter: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }],
          canExit: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }],
          stepEnter: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
          }],
          stepExit: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
          }],
          hidden: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["HostBinding"],
            args: ['hidden']
          }],
          stepTitleTemplate: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ContentChild"],
            args: [WizardStepTitleDirective]
          }],
          stepSymbolTemplate: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ContentChild"],
            args: [WizardStepSymbolDirective]
          }],
          stepId: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }],
          stepTitle: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }]
        });
      })();

      var ɵWizardCompletionStep_BaseFactory = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetInheritedFactory"](WizardCompletionStep);
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](WizardCompletionStep, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"]
        }], null, null);
      })();

      var ɵWizardCompletionStepComponent_BaseFactory = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetInheritedFactory"](WizardCompletionStepComponent);
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](WizardCompletionStepComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"],
          args: [{
            selector: 'aw-wizard-completion-step',
            template: "<ng-content></ng-content>\n",
            providers: [{
              provide: WizardStep,
              useExisting: Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["forwardRef"])(function () {
                return WizardCompletionStepComponent_1;
              })
            }, {
              provide: WizardCompletionStep,
              useExisting: Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["forwardRef"])(function () {
                return WizardCompletionStepComponent_1;
              })
            }]
          }]
        }], null, null);
      })();
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](WizardComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"],
          args: [{
            selector: 'aw-wizard',
            template: "<aw-wizard-navigation-bar\n  [direction]=\"navBarDirection\"\n  *ngIf=\"navBarLocation == 'top' || navBarLocation == 'left'\"\n  [ngClass]=\"{\n    'vertical': navBarLocation == 'left',\n    'horizontal': navBarLocation == 'top',\n    'small': navBarLayout == 'small',\n    'large-filled': navBarLayout == 'large-filled',\n    'large-filled-symbols': navBarLayout == 'large-filled-symbols',\n    'large-empty': navBarLayout == 'large-empty',\n    'large-empty-symbols': navBarLayout == 'large-empty-symbols'\n  }\">\n</aw-wizard-navigation-bar>\n\n<div [ngClass]=\"{\n  'wizard-steps': true,\n  'vertical': navBarLocation == 'left' || navBarLocation == 'right',\n  'horizontal': navBarLocation == 'top' || navBarLocation == 'bottom'\n}\">\n  <ng-content></ng-content>\n</div>\n\n<aw-wizard-navigation-bar\n  [direction]=\"navBarDirection\"\n  *ngIf=\"navBarLocation == 'bottom' || navBarLocation == 'right'\"\n  [ngClass]=\"{\n    'vertical': navBarLocation == 'right',\n    'horizontal': navBarLocation == 'bottom',\n    'small': navBarLayout == 'small',\n    'large-filled': navBarLayout == 'large-filled',\n    'large-filled-symbols': navBarLayout == 'large-filled-symbols',\n    'large-empty': navBarLayout == 'large-empty',\n    'large-empty-symbols': navBarLayout == 'large-empty-symbols'\n  }\">\n</aw-wizard-navigation-bar>\n"
          }]
        }], function () {
          return [];
        }, {
          navBarLocation: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }],
          navBarLayout: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }],
          navBarDirection: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }],
          disableNavigationBar: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }],
          defaultStepIndex: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }],
          horizontalOrientation: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["HostBinding"],
            args: ['class.horizontal']
          }],
          verticalOrientation: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["HostBinding"],
            args: ['class.vertical']
          }],
          wizardStepsQueryList: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ContentChildren"],
            args: [WizardStep, {
              descendants: true
            }]
          }]
        });
      })();
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](WizardNavigationBarComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"],
          args: [{
            selector: 'aw-wizard-navigation-bar',
            template: "<ul class=\"steps-indicator steps-{{numberOfWizardSteps}}\">\n  <li [attr.id]=\"step.stepId\" *ngFor=\"let step of wizardSteps\" [ngClass]=\"{\n        'current': isCurrent(step),\n        'editing': isEditing(step),\n        'done': isDone(step),\n        'optional': isOptional(step),\n        'completed': isCompleted(step),\n        'navigable': isNavigable(step)\n  }\">\n    <a [awGoToStep]=\"step\">\n      <div class=\"label\">\n        <ng-container *ngIf=\"step.stepTitleTemplate\" [ngTemplateOutlet]=\"step.stepTitleTemplate.templateRef\"\n          [ngTemplateOutletContext]=\"{wizardStep: step}\"></ng-container>\n        <ng-container *ngIf=\"!step.stepTitleTemplate\">{{step.stepTitle}}</ng-container>\n      </div>\n      <div class=\"step-indicator\"\n        [ngStyle]=\"{ 'font-family': step.stepSymbolTemplate ? '' : step.navigationSymbol.fontFamily }\">\n        <ng-container *ngIf=\"step.stepSymbolTemplate\" [ngTemplateOutlet]=\"step.stepSymbolTemplate.templateRef\"\n          [ngTemplateOutletContext]=\"{wizardStep: step}\"></ng-container>\n        <ng-container *ngIf=\"!step.stepSymbolTemplate\">{{step.navigationSymbol.symbol}}</ng-container>\n      </div>\n    </a>\n  </li>\n</ul>\n"
          }]
        }], function () {
          return [{
            type: WizardComponent
          }];
        }, {
          direction: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }]
        });
      })();

      var ɵWizardStepComponent_BaseFactory = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetInheritedFactory"](WizardStepComponent);
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](WizardStepComponent, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"],
          args: [{
            selector: 'aw-wizard-step',
            template: "<ng-content></ng-content>\n",
            providers: [{
              provide: WizardStep,
              useExisting: Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["forwardRef"])(function () {
                return WizardStepComponent_1;
              })
            }]
          }]
        }], null, null);
      })();
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](EnableBackLinksDirective, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"],
          args: [{
            selector: '[awEnableBackLinks]'
          }]
        }], function () {
          return [{
            type: WizardCompletionStep,
            decorators: [{
              type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Host"]
            }]
          }];
        }, {
          stepExit: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
          }]
        });
      })();
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](GoToStepDirective, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"],
          args: [{
            selector: '[awGoToStep]'
          }]
        }], function () {
          return [{
            type: WizardComponent
          }, {
            type: WizardStep,
            decorators: [{
              type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Optional"]
            }]
          }];
        }, {
          preFinalize: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
          }],
          postFinalize: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
          }],
          finalize: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
          }],

          /**
           * Listener method for `click` events on the component with this directive.
           * After this method is called the wizard will try to transition to the `destinationStep`
           */
          onClick: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"],
            args: ['click']
          }],
          targetStep: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"],
            args: ['awGoToStep']
          }]
        });
      })();
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](NextStepDirective, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"],
          args: [{
            selector: '[awNextStep]'
          }]
        }], function () {
          return [{
            type: WizardComponent
          }];
        }, {
          preFinalize: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
          }],
          postFinalize: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
          }],
          finalize: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
          }],

          /**
           * Listener method for `click` events on the component with this directive.
           * After this method is called the wizard will try to transition to the next step
           */
          onClick: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"],
            args: ['click']
          }]
        });
      })();
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](OptionalStepDirective, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"],
          args: [{
            selector: '[awOptionalStep]'
          }]
        }], function () {
          return [{
            type: WizardStep,
            decorators: [{
              type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Host"]
            }]
          }];
        }, {
          optional: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"],
            args: ['awOptionalStep']
          }]
        });
      })();
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](PreviousStepDirective, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"],
          args: [{
            selector: '[awPreviousStep]'
          }]
        }], function () {
          return [{
            type: WizardComponent
          }];
        }, {
          preFinalize: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
          }],
          postFinalize: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
          }],
          finalize: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
          }],

          /**
           * Listener method for `click` events on the component with this directive.
           * After this method is called the wizard will try to transition to the previous step
           */
          onClick: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"],
            args: ['click']
          }]
        });
      })();
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](ResetWizardDirective, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"],
          args: [{
            selector: '[awResetWizard]'
          }]
        }], function () {
          return [{
            type: WizardComponent
          }];
        }, {
          finalize: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"]
          }],

          /**
           * Resets the wizard
           */
          onClick: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"],
            args: ['click']
          }]
        });
      })();
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](SelectedStepDirective, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"],
          args: [{
            selector: '[awSelectedStep]'
          }]
        }], function () {
          return [{
            type: WizardStep,
            decorators: [{
              type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Host"]
            }]
          }];
        }, null);
      })();

      var ɵWizardCompletionStepDirective_BaseFactory = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetInheritedFactory"](WizardCompletionStepDirective);
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](WizardCompletionStepDirective, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"],
          args: [{
            selector: '[awWizardCompletionStep]',
            providers: [{
              provide: WizardStep,
              useExisting: Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["forwardRef"])(function () {
                return WizardCompletionStepDirective_1;
              })
            }, {
              provide: WizardCompletionStep,
              useExisting: Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["forwardRef"])(function () {
                return WizardCompletionStepDirective_1;
              })
            }]
          }]
        }], null, null);
      })();

      var ɵWizardStepDirective_BaseFactory = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetInheritedFactory"](WizardStepDirective);
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](WizardStepDirective, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"],
          args: [{
            selector: '[awWizardStep]',
            providers: [{
              provide: WizardStep,
              useExisting: Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["forwardRef"])(function () {
                return WizardStepDirective_1;
              })
            }]
          }]
        }], null, null);
      })();
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](NavigationModeDirective, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"],
          args: [{
            selector: '[awNavigationMode]'
          }]
        }], function () {
          return [{
            type: WizardComponent
          }];
        }, {
          awNavigationMode: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }],
          navigateBackward: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }],
          navigateForward: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"]
          }]
        });
      })();
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](CompletedStepDirective, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Directive"],
          args: [{
            selector: '[awCompletedStep]'
          }]
        }], function () {
          return [{
            type: WizardStep,
            decorators: [{
              type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Host"]
            }]
          }];
        }, {
          initiallyCompleted: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"],
            args: ['awCompletedStep']
          }]
        });
      })();

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsetNgModuleScope"](ArchwizardModule, {
          declarations: function declarations() {
            return [WizardComponent, WizardStepComponent, WizardNavigationBarComponent, WizardCompletionStepComponent, GoToStepDirective, NextStepDirective, PreviousStepDirective, OptionalStepDirective, WizardStepSymbolDirective, WizardStepTitleDirective, EnableBackLinksDirective, WizardStepDirective, WizardCompletionStepDirective, SelectedStepDirective, ResetWizardDirective, NavigationModeDirective, CompletedStepDirective];
          },
          imports: function imports() {
            return [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"]];
          },
          exports: function exports() {
            return [WizardComponent, WizardStepComponent, WizardNavigationBarComponent, WizardCompletionStepComponent, GoToStepDirective, NextStepDirective, PreviousStepDirective, OptionalStepDirective, WizardStepSymbolDirective, WizardStepTitleDirective, EnableBackLinksDirective, WizardStepDirective, WizardCompletionStepDirective, SelectedStepDirective, ResetWizardDirective, NavigationModeDirective, CompletedStepDirective];
          }
        });
      })();
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](ArchwizardModule, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"],
          args: [{
            declarations: [WizardComponent, WizardStepComponent, WizardNavigationBarComponent, WizardCompletionStepComponent, GoToStepDirective, NextStepDirective, PreviousStepDirective, OptionalStepDirective, WizardStepSymbolDirective, WizardStepTitleDirective, EnableBackLinksDirective, WizardStepDirective, WizardCompletionStepDirective, SelectedStepDirective, ResetWizardDirective, NavigationModeDirective, CompletedStepDirective],
            imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"]],
            exports: [WizardComponent, WizardStepComponent, WizardNavigationBarComponent, WizardCompletionStepComponent, GoToStepDirective, NextStepDirective, PreviousStepDirective, OptionalStepDirective, WizardStepSymbolDirective, WizardStepTitleDirective, EnableBackLinksDirective, WizardStepDirective, WizardCompletionStepDirective, SelectedStepDirective, ResetWizardDirective, NavigationModeDirective, CompletedStepDirective]
          }]
        }], null, null);
      })(); // export the components

      /**
       * Generated bundle index. Do not edit.
       */
      //# sourceMappingURL=angular-archwizard.js.map

      /***/

    },

    /***/
    "zdKD":
    /*!****************************************************************************************!*\
      !*** ./src/app/pages/auth/auth-investor-signup/auth-investor-signup-routing.module.ts ***!
      \****************************************************************************************/

    /*! exports provided: AuthInvestorSignupRoutingModule */

    /***/
    function zdKD(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AuthInvestorSignupRoutingModule", function () {
        return AuthInvestorSignupRoutingModule;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/router */
      "tyNb");
      /* harmony import */


      var _auth_investor_signup_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./auth-investor-signup.component */
      "SEHK");

      var routes = [{
        path: '',
        component: _auth_investor_signup_component__WEBPACK_IMPORTED_MODULE_2__["AuthInvestorSignupComponent"]
      }];

      var AuthInvestorSignupRoutingModule = function AuthInvestorSignupRoutingModule() {
        _classCallCheck(this, AuthInvestorSignupRoutingModule);
      };

      AuthInvestorSignupRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
        type: AuthInvestorSignupRoutingModule
      });
      AuthInvestorSignupRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
        factory: function AuthInvestorSignupRoutingModule_Factory(t) {
          return new (t || AuthInvestorSignupRoutingModule)();
        },
        imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](AuthInvestorSignupRoutingModule, {
          imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
          exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        });
      })();
      /*@__PURE__*/


      (function () {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AuthInvestorSignupRoutingModule, [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
          args: [{
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
          }]
        }], null, null);
      })();
      /***/

    }
  }]);
})();
//# sourceMappingURL=pages-auth-auth-module-es5.js.map